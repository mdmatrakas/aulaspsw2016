package br.angloamericano.util;

public class ListaDupEncadeada {
	private class No{
		public No proximo;
		public No anterior;
		public Object dado;
		public No(Object obj){
			proximo = null;
			anterior=null;
			dado=obj;
		}
		No(Object obj, No prox, No ant)
		{
			proximo = prox;
			anterior = ant;
			dado = obj;
		}
	}
	No inicio;
	No fim;
	int tamanho;
	
	public ListaDupEncadeada() {
		inicio = null;
		fim = null;
		tamanho = 0;
	}
	
	public int getTamanho(){
		return tamanho;
	}

  public int insere(Object obj, int pos){
  	No no;
  	
  	if(obj == null)
  		return tamanho;
  	
  	if(inicio == null)//primeiro elemento - lista vazia
  	{
  		no = new No(obj);
  		inicio = fim = no;
  	}
  	else // j� existem elementos na lista
  	{
  		if(pos <= 1)//inserir no inicio da lista
  		{
  			no = new No(obj, inicio, null);
  			inicio.anterior = no;
  			inicio = no;
  		}
  		else if(pos >= tamanho)//inserir no final da lista
  		{
  			no = new No(obj, null, fim);
  			fim.proximo = no;
  			fim = no;
  		}
  		else// inserir no meio da lista
  		{
  			No aux = inicio;
  			while(pos > 0)
  			{
  				aux = aux.proximo;
  				pos--;
  			}
  			// inserir na posi��o de aux
  			no = new No(obj, aux, aux.anterior);
  			aux.anterior.proximo = no;
  			aux.anterior = no;
  		}
  	}
  	tamanho++;
  	return tamanho;
  }
	
	public Object remove(int pos)
	{
		Object obj;
		if(inicio == null)//se a lista est� vazia
			return null;
		if(inicio == fim)//se existe apenas um elemento na lista
		{
			obj = inicio.dado;
			inicio = fim = null;
			tamanho--;
			return obj;
		}
		if(pos <= 1)//remover o primeiro elemento da lista
		{
			obj = inicio.dado;
			inicio = inicio.proximo;
		}
		else if(pos >= tamanho)//remover o �ltimo elemento da lista
		{
			obj = fim.dado;
			fim = fim.anterior;
		}
		else //remover um elemento no meio da lista
		{
			No aux = inicio;
			while(pos > 0){
				aux = aux.proximo;
				pos--;
			}
			// remover o elemento aux
			obj = aux.dado;
			aux.anterior.proximo = aux.proximo;
			aux.proximo.anterior = aux.anterior;
		}
		tamanho--;
		return obj;
	}
	
	public Object consulta(int pos)
	{
		if(inicio == null)//se a lista est� vazia
			return null;
		if(inicio == fim)//se existe apenas um elemento na lista
			return inicio.dado;
		if(pos==0)//consulta o primeiro elemento da lista
			return inicio.dado;
		else if(pos >= tamanho-1)//consulta o �ltimo elemento da lista
			return fim.dado;
		else //consulta um elemento no meio da lista
		{
			No aux = inicio;
			while(pos > 0){
				aux = aux.proximo;
				pos--;
			}
			// consulta o elemento aux
			return aux.dado;
		}	
	}
}
