package br.angloamericano.exLista;

import br.angloamericano.util.ListaDupEncadeada;

public class teste_Lista {
	public static void main(String[] args) {
		ListaDupEncadeada lista = new ListaDupEncadeada();
		
		String a="str teste 01";
		String b="str teste 02";
		String c="str teste 03";
		String d="str teste 04";
		String e="str teste 05";
		String f="str teste 06";

		lista.insere(f, 0);
		lista.insere(e, 0);
		lista.insere(d, 0);
		lista.insere(c, 0);
		lista.insere(b, 0);
		lista.insere(a, 0);
		System.out.println("Tamanho: " +lista.getTamanho());
		for(int i =0; i<lista.getTamanho(); i++)
			System.out.println(lista.consulta(i));
		System.out.println("");
		for(int i =0; i<lista.getTamanho(); i++)
			System.out.println(lista.consulta(lista.getTamanho()-i-1));
		System.out.println("");
		
		
		System.out.println(lista.remove(0));
		System.out.println("Tamanho: " +lista.getTamanho());
		System.out.println(lista.remove(0));
		System.out.println("Tamanho: " +lista.getTamanho());
		System.out.println(lista.remove(0));
		System.out.println("Tamanho: " +lista.getTamanho());
		System.out.println(lista.remove(0));
		System.out.println("Tamanho: " +lista.getTamanho());
		System.out.println(lista.remove(0));
		System.out.println("Tamanho: " +lista.getTamanho());
		System.out.println(lista.remove(0));
		System.out.println("Tamanho: " +lista.getTamanho());
	}

}
