package a06_02_EmpregadoHierarquia;

/**
 * Classe EmpregadoComissionadoComSalario herda de EmpregadoComissionado e acessa os dados privados
 * de EmpregadoComissionado via os m�todos de acesso publicos de EmpregadoComissionado.
 * 
 * @author Matrakas
 */
public class EmpregadoComissionadoComSalario extends EmpregadoComissionado
{
	/**
	 * Ponto flutuante com o valor do sal�rio de base do empregado.
	 */
	private double	salarioBase;

	/**
	 * Construtor de seis argumentos
	 * 
	 * @param primeiro
	 *            String com o primeiro nome do empregado.
	 * @param ultimo
	 *            String com o �ltimo nome do empregado.
	 * @param cpf
	 *            String com o n�mero do CPF do empregado.
	 * @param vendas
	 *            Ponto flutuante com o valor de vendas realizadas pelo empregado.
	 * @param taxa
	 *            Ponto flutuante com o valor da taxa de comiss�o do empregado.
	 * @param salario
	 *            Ponto flutuante com o valor do sal�rio base do empregado.
	 */
	public EmpregadoComissionadoComSalario(String primeiro, String ultimo, String cpf,
			double vendas, double taxa, double salario)
	{
		super(primeiro, ultimo, cpf, vendas, taxa);
		setSalarioBase(salario); // valida e armazena sal�rio-base
	}

	/**
	 * Valida e configura o sal�rio-base do empregado.
	 * 
	 * @param salario
	 */
	public void setSalarioBase(double salario)
	{
		salarioBase = (salario < 0.0) ? 0.0 : salario;
	}

	/**
	 * M�todo de acesso para o sal�rio-base.
	 * 
	 * @return Ponto flutuante com o valor do sal�rio.
	 */
	public double getSalarioBase()
	{
		return salarioBase;
	}

	/**
	 * Calcula o valor a ser pago ao empregado, sal�rio mais comiss�o. Sobrescreve o m�todo
	 * proventos em EmpregadoComissionado.
	 */
	public double proventos()
	{
		return getSalarioBase() + super.proventos();
	}

	/**
	 * Implementa��o do m�todo toString, que devolve a representa��o String de
	 * EmpregadoComissionadoComSalario
	 */
	public String toString()
	{
		return String.format("%s %s\n%s: %.2f", "Comissionado", super.toString(), "Sal�rio",
				getSalarioBase());
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
