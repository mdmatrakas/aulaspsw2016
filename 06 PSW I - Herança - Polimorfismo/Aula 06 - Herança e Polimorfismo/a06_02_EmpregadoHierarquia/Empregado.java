package a06_02_EmpregadoHierarquia;

/**
 * Superclasse abstrata Empregado.
 * 
 * @author Matrakas
 * 
 */
public abstract class Empregado
{
	/**
	 * Primeiro nome do empregado
	 */
	private String	primeiroNome;
	/**
	 * Sobrenome do empregado
	 */
	private String	ultimoNome;
	/**
	 * N�mero do CPF do empregado
	 */
	private String	numeroCPF;

	/**
	 * Construtor com tr�s argumentos
	 * 
	 * @param primeiro
	 *            String com o primeiro nome do empregado.
	 * @param ultimo
	 *            String com o �ltimo nome do empregado.
	 * @param cpf
	 *            String com o n�mero do CPF do empregado.
	 */
	public Empregado(String primeiro, String ultimo, String cpf)
	{
		primeiroNome = primeiro;
		ultimoNome = ultimo;
		numeroCPF = cpf;
	}

	/**
	 * M�todo de acesso para configurar o primeiro nome do empregado.
	 * 
	 * @param primeiro
	 *            String com o primeiro nome do empregado.
	 */
	public void setPrimeiroNome(String primeiro)
	{
		primeiroNome = primeiro;
	}

	/**
	 * M�todo de acesso para receber o primeiro nome do empregado.
	 * 
	 * @return String com o primeiro nome do empregado.
	 */
	public String getPrimeiroNome()
	{
		return primeiroNome;
	}

	/**
	 * M�todo de acesso para configurar o �ltimo nome do empregado.
	 * 
	 * @param ultimo
	 *            String com o �ltimo nome do empregado.
	 */
	public void setUltimoNome(String ultimo)
	{
		ultimoNome = ultimo;
	}

	/**
	 * M�todo de acesso para configurar o �ltimo nome do empregado.
	 * 
	 * @return String com o �ltimo nome do empregado.
	 */
	public String getUltimoNome()
	{
		return ultimoNome;
	}

	/**
	 * M�todo de acesso para receber o CPF do empregado.
	 * 
	 * @param cpf
	 *            String com o CPF do empregado.
	 */
	public void setNumeroCPF(String cpf)
	{
		numeroCPF = cpf;
	}

	/**
	 * M�todo de acesso para configurar o CPF do empregado.
	 * 
	 * @return String com o CPF do empregado.
	 */
	public String getNumeroCPF()
	{
		return numeroCPF;
	}

	/**
	 * Implementa��o do m�todo toString, que devolve a representa��o String do objeto
	 * EmpregadoComissionado
	 */
	public String toString()
	{
		return String.format("%s %s\n%s: %s", getPrimeiroNome(), getUltimoNome(),
				"Cadstro de Pessoa F�sica (CPF)", getNumeroCPF());
	}

	/**
	 * M�todo abstrato sobrescrito pelas subclasses, para calcular o total dos proventos do
	 * empregado.
	 * 
	 * @return Ponto flutuante com o valor total a ser pago ao empregado.
	 */
	public abstract double proventos(); // nenhuma implementa��o aqui
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
