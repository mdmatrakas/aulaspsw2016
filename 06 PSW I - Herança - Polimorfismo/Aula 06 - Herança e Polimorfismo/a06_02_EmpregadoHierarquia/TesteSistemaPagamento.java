package a06_02_EmpregadoHierarquia;

/**
 * Programa de teste da hierarquia Empregado.
 * 
 * @author Matrakas
 */
public class TesteSistemaPagamento
{
	public static void main(String args[])
	{
		// cria objetos de subclasse
		EmpregadoAssalariado empregadoAssalariado = new EmpregadoAssalariado("Jo�o", "Silva",
				"111.111.111-11", 800.00);
		EmpregadoHorista empregadoHorista = new EmpregadoHorista("Karina", "Vilela", "222.222.222-22",
				16.75, 40);
		EmpregadoComissionado empregadoComissionado = new EmpregadoComissionado("Susan", "Siqueira",
				"333.333.333-33", 10000, .06);
		EmpregadoComissionadoComSalario empregadoComissionadoSalario = new EmpregadoComissionadoComSalario(
				"Braulio", "Gomes", "444.444.444-44", 5000, .04, 300);

		System.out.println("Empregados processados individualmente:\n");

		System.out.printf("%s\n%s: $%,.2f\n\n", empregadoAssalariado, "recebeu",
				empregadoAssalariado.proventos());
		System.out.printf("%s\n%s: $%,.2f\n\n", empregadoHorista, "recebeu",
				empregadoHorista.proventos());
		System.out.printf("%s\n%s: $%,.2f\n\n", empregadoComissionado, "recebeu",
				empregadoComissionado.proventos());
		System.out.printf("%s\n%s: $%,.2f\n\n", empregadoComissionadoSalario, "recebeu",
				empregadoComissionadoSalario.proventos());

		// cria um vetor de Empregados de quatro elementos
		Empregado empregados[] = new Empregado[4];

		// inicializa o vetor com empregados
		empregados[0] = empregadoAssalariado;
		empregados[1] = empregadoHorista;
		empregados[2] = empregadoComissionado;
		empregados[3] = empregadoComissionadoSalario;

		System.out.println("Empregados processados com polimorfismo:\n");

		// processa genericamente cada elemento no empregados
		for (Empregado empregadoCorrente : empregados)
		{
			System.out.println(empregadoCorrente); // invoca toString

			// determina se elemento � um EmpregadoComissionadoComSalario
			if (empregadoCorrente instanceof EmpregadoComissionadoComSalario)
			{
				// downcast da refer�ncia de Empregado para
				// refer�ncia a EmpregadoComissionadoComSalario
				EmpregadoComissionadoComSalario empregado = (EmpregadoComissionadoComSalario) empregadoCorrente;

				double salarioBaseAntigo = empregado.getSalarioBase();
				empregado.setSalarioBase(1.10 * salarioBaseAntigo);
				System.out.printf("novo salario base com 10%% de aumento �: $%,.2f\n",
						empregado.getSalarioBase());
			}

			System.out.printf("Recebeu $%,.2f\n\n", empregadoCorrente.proventos());
		}

		// obt�m o nome do tipo de cada objeto no vetor empregados
		for (int j = 0; j < empregados.length; j++)
			System.out.printf("Empregado %d � um %s\n", j, empregados[j].getClass().getName());
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
