package a06_02_EmpregadoHierarquia;

/**
 * Classe EmpregadoHorista estende Empregado.
 * 
 * @author Matrakas
 * 
 */
public class EmpregadoHorista extends Empregado
{
	/**
	 * Sal�rio por hora
	 */
	private double	valorHora;
	/**
	 * Horas trabalhadas durante a semana
	 */
	private double	horasTrabalhadas;

	/**
	 * Construtor de cinco argumentos
	 * 
	 * @param primeiro
	 *            String com o primeiro nome do empregado.
	 * @param ultimo
	 *            String com o �ltimo nome do empregado.
	 * @param cpf
	 *            String com o n�mero do CPF do empregado.
	 * @param valorHora
	 *            Ponto flutuante com o valor da hora trabalhada do empregado.
	 * @param horasTrabalhadas
	 *            Ponto flutuante com o n�mero de horas trabalhadase do empregado.
	 */
	public EmpregadoHorista(String primeiro, String ultimo, String cpf, double valorHora,
			double horasTrabalhadas)
	{
		// chamada impl�cita para o construtor Object ocorre aqui
		super(primeiro, ultimo, cpf);
		setValorHora(valorHora); // valida a remunera��o por hora
		setHorasTrabalhadas(horasTrabalhadas); // valida as horas trabalhadas
	}

	/**
	 * M�todo de acesso para configurar o valor da hora do empregado.
	 * 
	 * @param valor
	 *            Ponto flutuante com o valor da hora do empregado.
	 */
	public void setValorHora(double valor)
	{
		valorHora = (valor < 0.0) ? 0.0 : valor;
	}

	/**
	 * M�todo de acesso para receber o valor da hora do empregado.
	 * 
	 * @return Ponto flutuante com o valor da hora do empregado.
	 */
	public double getValorHora()
	{
		return valorHora;
	}

	/**
	 * M�todo de acesso para configurar as horas trabalhadas do empregado.
	 * 
	 * @param horas
	 *            Ponto flutuante com as horas trabalhadas do empregado.
	 */
	public void setHorasTrabalhadas(double horas)
	{
		horasTrabalhadas = ((horas >= 0.0) && (horas <= 168.0)) ? horas : 0.0;
	}

	/**
	 * M�todo de acesso para receber as horas trabalhadas do empregado.
	 * 
	 * @return Ponto flutuante com as horas trabalhadas do empregado.
	 */
	public double getHorasTrabalhadas()
	{
		return horasTrabalhadas;
	}

	/**
	 * Calcula os proventos totais do empregado.
	 * 
	 * Sobrescreve o m�todo proventos em Empregado.
	 * 
	 * @return Ponto flutuante com o valor total a ser pago ao empregado.
	 */
	public double proventos()
	{
		if (getHorasTrabalhadas() <= 40) // nenhuma hora extra
			return getValorHora() * getHorasTrabalhadas();
		else
			return 40 * getValorHora() + (getHorasTrabalhadas() - 40) * getValorHora() * 1.5;
	}

	/**
	 * Implementa��o do m�todo toString, que devolve a representa��o String do objeto
	 * EmpregadoHorista
	 */
	public String toString()
	{
		return String.format("Empregado horista: %s\n%s: $%,.2f; %s: %,.2f", super.toString(),
				"Valor da hora", getValorHora(), "Horas trabalhadas", getHorasTrabalhadas());
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
