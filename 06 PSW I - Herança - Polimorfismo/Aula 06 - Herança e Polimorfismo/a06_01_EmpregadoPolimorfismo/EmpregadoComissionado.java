package a06_01_EmpregadoPolimorfismo;

/**
 * A classe EmpregadoComissionado representa um empregado comissionado.
 * 
 * @author Matrakas
 * 
 */
public class EmpregadoComissionado
{
	/**
	 * Primeiro nome do empregado
	 */
	private String	primeiroNome;
	/**
	 * Sobrenome do empregado
	 */
	private String	ultimoNome;
	/**
	 * N�mero do CPF do empregado
	 */
	private String	numeroCPF;
	/**
	 * Vendas brutas semanais
	 */
	private double	vendasBrutas;
	/**
	 * Porcentagem da comiss�o
	 */
	private double	taxaComissao;

	/**
	 * Construtor de cinco argumentos
	 * 
	 * @param primeiro
	 *            String com o primeiro nome do empregado.
	 * @param ultimo
	 *            String com o �ltimo nome do empregado.
	 * @param cpf
	 *            String com o n�mero do CPF do empregado.
	 * @param vendas
	 *            Ponto flutuante com o valor de vendas realizadas pelo empregado.
	 * @param taxa
	 *            Ponto flutuante com o valor da taxa de comiss�o do empregado.
	 */
	public EmpregadoComissionado(String primeiro, String ultimo, String cpf,
			double vendas, double taxa)
	{
		// chamada impl�cita para o construtor Object ocorre aqui
		primeiroNome = primeiro;
		ultimoNome = ultimo;
		numeroCPF = cpf;
		setVendasBrutas(vendas); // valida e armazena as vendas brutas
		setTaxaComissao(taxa); // valida e armazena a taxa de comiss�o
	}

	/**
	 * M�todo de acesso para configurar o primeiro nome do empregado.
	 * 
	 * @param primeiro
	 *            String com o primeiro nome do empregado.
	 */
	public void setPrimeiroNome(String primeiro)
	{
		primeiroNome = primeiro;
	}

	/**
	 * M�todo de acesso para receber o primeiro nome do empregado.
	 * 
	 * @return String com o primeiro nome do empregado.
	 */
	public String getPrimeiroNome()
	{
		return primeiroNome;
	}

	/**
	 * M�todo de acesso para configurar o �ltimo nome do empregado.
	 * 
	 * @param ultimo
	 *            String com o �ltimo nome do empregado.
	 */
	public void setUltimoNome(String ultimo)
	{
		ultimoNome = ultimo;
	}

	/**
	 * M�todo de acesso para configurar o �ltimo nome do empregado.
	 * 
	 * @return String com o �ltimo nome do empregado.
	 */
	public String getUltimoNome()
	{
		return ultimoNome;
	}

	/**
	 * M�todo de acesso para receber o CPF do empregado.
	 * 
	 * @param cpf
	 *            String com o CPF do empregado.
	 */
	public void setNumeroCPF(String cpf)
	{
		numeroCPF = cpf;
	}

	/**
	 * M�todo de acesso para configurar o CPF do empregado.
	 * 
	 * @return String com o CPF do empregado.
	 */
	public String getNumeroCPF()
	{
		return numeroCPF;
	}

	/**
	 * M�todo de acesso para configurar a quantidade de vendas brutas.
	 * 
	 * @param vendasBrutas
	 *            Ponto flutuante com o valor de vendas brutas do empregado.
	 */
	public void setVendasBrutas(double vendasBrutas)
	{
		this.vendasBrutas = (vendasBrutas < 0.0) ? 0.0 : vendasBrutas;
	}

	/**
	 * M�todo de acesso para receber a quantidade de vendas brutas.
	 * 
	 * @return Ponto flutuante com o valor de vendas brutas do empregado.
	 */
	public double getVendasBrutas()
	{
		return vendasBrutas;
	}

	/**
	 * M�todo de acesso para configurar a taxa de comiss�o.
	 * 
	 * @param taxaComissao
	 *            Ponto flutuante com o valor da taxa de comiss�o do empregado.
	 */
	public void setTaxaComissao(double taxaComissao)
	{
		this.taxaComissao = (taxaComissao > 0.0 && taxaComissao < 1.0) ? 
				taxaComissao : 0.0;
	}

	/**
	 * M�todo de acesso para receber a taxa de comiss�o.
	 * 
	 * @return Ponto flutuante com o valor da taxa de comiss�o do empregado.
	 */
	public double getTaxaComissao()
	{
		return taxaComissao;
	}

	/**
	 * Calcula os proventos totais do empregado.
	 * 
	 * @return Ponto flutuante com o valor total a ser pago ao empregado.
	 */
	public double proventos()
	{
		return getTaxaComissao() * getVendasBrutas();
	}

	/**
	 * Implementa��o do m�todo toString, que devolve a representa��o String do objeto
	 * EmpregadoComissionado
	 */
	public String toString()
	{
		return String.format("%s: %s %s\n%s: %s\n%s: %.2f\n%s: %.2f",
				"Empregado comissionado",
				getPrimeiroNome(), getUltimoNome(), 
				"Cadastro de Pessoa F�sica (CPF)",
				getNumeroCPF(), "Vendas brutas", getVendasBrutas(), 
				"Taxa de comiss�o",
				getTaxaComissao());
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
