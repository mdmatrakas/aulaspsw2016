package a06_01_EmpregadoPolimorfismo;



/**
 *  Atribuindo refer�ncias de superclasse e subclasse a vari�veis de superclasse e de subclasse.
 * @author Matrakas
 *
 */
public class TestePolimorfismo  
{
   public static void main( String args[] ) 
   {
      // atribui uma refer�ncia de superclasse a vari�vel de superclasse            
      EmpregadoComissionado empregadoComissionado = 
    	  new EmpregadoComissionado(
         "Suzana", "Silva", "123.123.123-55", 10000, .06 );                  

      // atribui uma refer�ncia de subclasse a vari�vel de subclasse       
      EmpregadoComissionadoComSalario empregadoComissionadoSalario =
         new EmpregadoComissionadoComSalario(                     
         "Jo�o", "Siqueira", "321.321.321-66", 5000, .04, 300 );     

      // invoca toString no objeto de superclasse utilizando a vari�vel de superclasse
      System.out.printf( "%s %s:\n\n%s\n\n", 
         "Chama o m�todo toString de EmpregadoComissionado usando a referencia ",
         "para o objeto da superclasse", empregadoComissionado.toString() );

      // invoca toString no objeto de subclasse utilizando a vari�vel de subclasse  
      System.out.printf( "%s %s:\n\n%s\n\n", 
         "Chama o m�todo toString de EmpregadoComissionadoComSalario usando a referencia ",
         "para o objeto da subclasse", 
         empregadoComissionadoSalario.toString() );

      // invoca toString no objeto de subclasse utilizando a vari�vel de superclasse
      EmpregadoComissionado empregadoComissionado2 =
         empregadoComissionadoSalario;           
      System.out.printf( "%s %s:\n\n%s\n", 
         "Chama o m�todo toString de EmpregadoComissionado usando a referencia ",
         "para o objeto da subclasse", empregadoComissionado2.toString() );
   } 
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/