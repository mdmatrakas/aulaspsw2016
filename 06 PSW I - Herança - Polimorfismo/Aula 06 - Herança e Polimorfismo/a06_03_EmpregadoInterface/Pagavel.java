package a06_03_EmpregadoInterface;

/**
 * Declaração da interface Pagavel.
 * 
 * @author Matrakas
 * 
 */
public interface Pagavel
{
	/**
	 * Calcula os proventos totais do empregado. Nenhuma implementação
	 * 
	 * @return Ponto flutuante com o valor total a ser pago ao empregado.
	 */
	double getQuantiaPagamento();
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
