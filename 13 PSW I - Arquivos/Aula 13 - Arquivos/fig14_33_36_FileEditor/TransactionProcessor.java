package fig14_33_36_FileEditor;
// Fig. 14.35: TransactionProcessor.java
// Um programa de processamento de transa��es c/ arquivos de acesso aleat�rio.
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import fig14_23_AccountRecord.RandomAccessAccountRecord;

public class TransactionProcessor
{
   private FileEditor dataFile;
   private RandomAccessAccountRecord record;
   private MenuOption choices[] = { MenuOption.PRINT,
      MenuOption.UPDATE, MenuOption.NEW,
      MenuOption.DELETE, MenuOption.END };

   private Scanner input = new Scanner( System.in );

   // obt�m o nome de arquivo e abre o arquivo
   private boolean openFile()
   {
      try // tenta abrir o arquivo
      {
         // chama o m�todo auxiliar para abrir o arquivo
         dataFile = new FileEditor( "clients.dat" ); 
      } // fim do try
      catch ( IOException ioException )
      {
         System.err.println( "Error opening file." );
         return false;
      } // fim do catch
       
      return true;
   } // fim do m�todo openFile

   // fecha o arquivo e termina o aplicativo
   private void closeFile() 
   {
      try // fecha o arquivo
      {
         dataFile.closeFile();
      } // fim do try
      catch ( IOException ioException )
      {
         System.err.println( "Error closing file." );
         System.exit( 1 );
      } // fim do catch
   } // fim do m�todo closeFile

   // cria, atualiza ou exclui o registro
   private void performAction( MenuOption action )
   {
      int accountNumber = 0; // n�mero de conta do registro
      String firstName; // primeiro nome da conta
      String lastName; // sobrenome da conta
      double balance; // saldo da conta
      double transaction; // valor monet�rio a alterar no saldo

      try // tenta manipular arquivos com base na op��o selecionada
      {
         switch ( action ) // comuta com base na op��o selecionada
         {
            case PRINT:
               System.out.println();
               dataFile.readRecords();
               break;
            case NEW:
               System.out.printf( "\n%s%s\n%s\n%s",
                  "Enter account number,",
                  " first name, last name and balance.",
                  "(Account number must be 1 - 100)", "? " );

               accountNumber = input.nextInt(); // l� o n�mero de conta
               firstName = input.next(); // l� o primeiro nome
               lastName = input.next(); // l� o sobrenome
               balance = input.nextDouble(); // l� o saldo

               dataFile.newRecord( accountNumber, firstName,  
                  lastName, balance ); // cria um novo registro   
               break;
            case UPDATE:
               System.out.print(
                  "\nEnter account to update ( 1 - 100 ): " );
               accountNumber = input.nextInt();
               record = dataFile.getRecord( accountNumber );

               if ( record.getAccount() == 0 )
                  System.out.println( "Account does not exist." );
               else
               {
                  // exibe o conte�do de registro
                  System.out.printf( "%-10d%-12s%-12s%10.2f\n\n",
                     record.getAccount(), record.getFirstName(),
                     record.getLastName(), record.getBalance() );

                  System.out.print(
                     "Enter charge ( + ) or payment ( - ): " );
                  transaction = input.nextDouble();
                  dataFile.updateRecord( accountNumber, // atualiza o registro
                     transaction );                                     

                  // recupera o registro atualizado
                  record = dataFile.getRecord( accountNumber );

                  // exibi��o o registro atualizado
                  System.out.printf( "%-10d%-12s%-12s%10.2f\n",
                     record.getAccount(), record.getFirstName(),
                     record.getLastName(), record.getBalance() );
               } // fim do else
               break;
            case DELETE:
               System.out.print(
                  "\nEnter an account to delete (1 - 100): " );
               accountNumber = input.nextInt();
         
               dataFile.deleteRecord( accountNumber ); // exclui o registro
               break;
            default:
               System.out.println( "Invalid action." );
               break;
         } // fim do switch
      } // fim do try
      catch ( NumberFormatException format )
      {
         System.err.println( "Bad input." );
      } // fim do catch
      catch ( IllegalArgumentException badAccount )
      {
         System.err.println( badAccount.getMessage() );
      } // fim do catch
      catch ( IOException ioException )
      {
         System.err.println( "Error writing to the file." );
      } // fim do catch
      catch ( NoSuchElementException elementException )
      {
         System.err.println( "Invalid input. Please try again." );
         input.nextLine(); // descarta entrada para o usu�rio tentar de novo
      } // fim do catch
   } // fim do m�todo performAction

   // permite ao usu�rio inserir escolhas de menu
   private MenuOption enterChoice()
   {
      int menuChoice = 1;

      // exibe op��es dispon�veis
      System.out.printf( "\n%s\n%s\n%s\n%s\n%s\n%s",
         "Enter your choice", "1 - List accounts",
         "2 - Update an account", "3 - Add a new account",
         "4 - Delete an account", "5 - End program\n? " );

      try
      {
         menuChoice = input.nextInt();
      }
      catch ( NoSuchElementException elementException )
      {
         System.err.println( "Invalid input." );
         System.exit( 1 );
      } // fim do catch

      return choices[ menuChoice - 1 ]; // retorna a escolha do usu�rio
   } // fim do enterChoice

   public void processRequests()
   {
      openFile();

      // obt�m a solicita��o do usu�rio
      MenuOption choice = enterChoice();

      while ( choice != MenuOption.END )
      {
         performAction( choice );
         choice = enterChoice();
      } // fim do while
 
      closeFile();
   } // fim do m�todo processRequests
} // fim da classe TransactionProcessor


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
