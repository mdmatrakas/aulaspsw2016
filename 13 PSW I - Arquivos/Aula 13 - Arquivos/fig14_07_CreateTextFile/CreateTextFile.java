package fig14_07_CreateTextFile;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.SecurityException;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import fig14_06_AccountRecord.AccountRecord;

//import Deitel.ch14.ch14.fig16_06_AccountRecord;

public class CreateTextFile
{
	// private Formatter output; // objeto utilizado para gerar sa�da de texto
	// no arquivo
	private FileWriter	output; // objeto utilizado para gerar sa�da de texto no
								// arquivo

	// permite ao usu�rio abrir o arquivo
	public void openFile()
	{
		try
		{
			// output = new Formatter( "clients.txt" );
			// output = new Formatter(new File("clientes.txt"));
			output = new FileWriter("clientes.txt");
		}
		catch (SecurityException securityException)
		{
			System.err.println("You do not have write access to this file.");
			System.exit(1);
		}
		catch (FileNotFoundException filesNotFoundException)
		{
			System.err.println("Error creating file.");
			System.exit(1);
		}
		catch (IOException e)
		{
			System.err.println("Error creating file.");
			System.exit(1);
			// e.printStackTrace();
		}
	}

	// adiciona registros ao arquivo
	public void addRecords()
	{
		// objeto a ser gravado no arquivo
		AccountRecord record = new AccountRecord();

		Scanner input = new Scanner(System.in);

		System.out.printf("%s\n%s\n%s\n%s\n\n",
				"To terminate input, type the end-of-file indicator ",
				"when you are prompted to enter input.",
				"On UNIX/Linux/Mac OS X type <ctrl> d then press Enter",
				"On Windows type <ctrl> z then press Enter");

		System.out
				.printf("%s\n%s",
						"Enter account number (> 0), first name, last name and balance.",
						"? ");

		try
		// gera sa�da dos valores para o arquivo
		{
			while (input.hasNext()) // faz um loop at� o indicador de fim de arquivo
			{
				// recupera os dados para sa�da
				record.setAccount(input.nextInt()); // l� o n�mero de conta
				record.setFirstName(input.next()); // l� o primeiro nome
				record.setLastName(input.next()); // l� o sobrenome
				record.setBalance(input.nextDouble()); // l� o saldo

				if (record.getAccount() > 0)
				{
					// grava um novo registro
					// output.format( "%d %s %s %.2f\n", record.getAccount(),
					// record.getFirstName(), record.getLastName(),
					// record.getBalance() );
					try
					{
						// output.
						output.append(String.format("%d \n %s %s \n %.2f\n\n",
								record.getAccount(), record.getFirstName(),
								record.getLastName(), record.getBalance()));
					}
					catch (IOException e)
					{
						System.err.println("Error writing to file.");
						System.exit(1);
						// e.printStackTrace();
					}
				}
				else
				{
					System.out
							.println("Account number must be greater than 0.");
				}
			}
		}
		catch ( IllegalStateException stateException )
		{
			System.err.println( "Error reading from file." );
			//System.exit( 1 );
		} // fim do catch
		//catch (FormatterClosedException formatterClosedException)
		//{
		//	System.err.println("Error writing to file.");
		//	return;
		//}
		catch (NoSuchElementException elementException)
		{
			System.err.println("Invalid input. Please try again.");
			input.nextLine(); // descarta entrada para o usu�rio tentar de
								// novo
		}

		System.out.printf("%s %s\n%s", "Enter account number (>0),",
				"first name, last name and balance.", "? ");
		input.close();
	}

	// fecha o arquivo
	public void closeFile()
	{
		if (output != null)
			try
			{
				output.close();
			}
			catch (IOException e)
			{
				// Auto-generated catch block
				e.printStackTrace();
			}
	}
}

/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and * Pearson Education,
 * Inc. All Rights Reserved. * * DISCLAIMER: The authors and publisher of this
 * book have used their * best efforts in preparing the book. These efforts
 * include the * development, research, and testing of the theories and programs
 * * to determine their effectiveness. The authors and publisher make * no
 * warranty of any kind, expressed or implied, with regard to these * programs
 * or to the documentation contained in these books. The authors * and publisher
 * shall not be liable in any event for incidental or * consequential damages in
 * connection with, or arising out of, the * furnishing, performance, or use of
 * these programs. *
 *************************************************************************/
