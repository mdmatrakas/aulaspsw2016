package fig13_05_usando;
// Fig. 13.5: UsingExceptions.java
// Demonstra��o do tratamento de exce��es try...catch...finally 
// Mecanismo.

public class UsingExceptions 
{
   public static void main( String args[] )
   {
      try 
      { 
         throwException(); // chama m�todo throwException 
      } // fim de try
      catch ( Exception exception ) // exce��o lan�ada por throwException
      {
         System.err.println( "Exception handled in main" );
      } // fim de catch

      doesNotThrowException();
   } // fim de main

   // demonstra try...catch...finally
   public static void throwException() throws Exception
   {
      try // lan�a uma exce��o e imediatamente a captura
      { 
         System.out.println( "Method throwException" );
         throw new Exception(); // gera a exce��o
      } // fim de try
      catch ( Exception exception ) // captura exce��o lan�ada em try
      {
         System.err.println(
            "Exception handled in method throwException" );
         throw exception; // lan�a novamente para processamento adicional

         // qualquer c�digo aqui n�o seria alcan�ado

      } // fim de catch
      finally // executa independentemente do que ocorre em try...catch
      {                                                             
         System.err.println( "Finally executed in throwException" );
      } // fim de finally 

      // qualquer c�digo aqui n�o seria atingido, exce��o lan�ada de novo em catch

   } // fim de m�todo throwException 

   // demonstra finally quando nenhuma exce��o ocorrer
   public static void doesNotThrowException()
   {
      try // bloco try n�o lan�a uma exce��o
      { 
         System.out.println( "Method doesNotThrowException" );
      } // fim de try
      catch ( Exception exception ) // n�o executa
      {
         System.err.println( exception );
      } // fim de catch
      finally // executa independentemente do que ocorre em try...catch
      {                                                           
         System.err.println(                                      
            "Finally executed in doesNotThrowException" );        
      } // fim de finally 

      System.out.println( "End of method doesNotThrowException" );
   } // fim de m�todo doesNotThrowException
} // fim da classe UsingExceptions 


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/