package fig13_02_DivZ;
import java.util.InputMismatchException;
import java.util.Scanner;

public class DivideByZeroWithExceptionHandling
{
   // demonstra o lan�amento de uma exce��o quando ocorre uma divis�o por zero 
   public static int quotient( int numerator, int denominator )
      throws ArithmeticException
   {
      return numerator / denominator; // poss�vel divis�o por zero
   } // fim de m�todo quotient 

   public static void main( String args[] )
   {
      Scanner scanner = new Scanner( System.in ); // scanner para entrada
      boolean continueLoop = true; // determina se mais entradas s�o necess�rias

      do                                                                  
      {                                                                   
         try // l� dois n�meros e calcula o quociente
         {                                                                
            System.out.print( "Please enter an integer numerator: " );    
            int numerator = scanner.nextInt();                            
            System.out.print( "Please enter an integer denominator: " );  
            int denominator = scanner.nextInt();                          
                                                                          
            int result = quotient( numerator, denominator );              
            System.out.printf( "\nResult: %d / %d = %d\n", numerator,     
               denominator, result );                                     
            continueLoop = false; // entrada bem-sucedida; fim de loop
         } 
         catch ( InputMismatchException inputMismatchException )          
         {                                                                
            System.err.printf( "\nException: %s\n",                       
               inputMismatchException );
            System.err.flush();
            scanner.nextLine(); // descarta entrada para o usu�rio tentar novamente
            System.out.println(                                           
               "You must enter integers. Please try again.\n" );          
         }
         catch ( ArithmeticException arithmeticException )                
         {                                                                
            System.err.printf( "\nException: %s\n", arithmeticException );
            System.err.flush();
            System.out.println(                                           
               "Zero is an invalid denominator. Please try again.\n" );   
         } 
      } while ( continueLoop );     
      scanner.close();
   } 
} 


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/