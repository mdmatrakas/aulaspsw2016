/**
 * 
 */
package coffee_shop.DAO;

import java.sql.ResultSet;
//import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import coffee_shop.DBConnection;
import coffee_shop.data.Coffee;

/**
 * @author mdmatrakas
 * 
 */
public class CoffeeDAO
{
	private Coffee		coffee;
	private Statement	statement;
	private ResultSet	resultSet;
	// private ResultSetMetaData metaData;
	private int			numberOfRows;
	private String		query	= "SELECT * FROM coffees";
	private String		update = "UPDATE (name, price, sales, sup_id, total) from coffees with ";

	static private final String[] columnsNames = { "Nome do Cafe", 
		"Identificador do fornecedor", "Pre�o", "Vendas da semana", "Total de vandas"  
	};
	
	static private final int numColumns = 5;

	public CoffeeDAO()
	{
		coffee = new Coffee();
		try
		{
			// cria Statement para consultar banco de dados
			statement = DBConnection.getConnection().createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			// configura consulta e a executa
			setQuery();
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}

	public void moveToRow(int row)
	{
		try
		{
			resultSet.absolute(row);
			coffee.setName(resultSet.getString(1));
			coffee.setSup_Id(resultSet.getInt(2));
			coffee.setPrice(resultSet.getDouble(3));
			coffee.setSales(resultSet.getInt(4));
			coffee.setTotal(resultSet.getInt(5));
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}

	public Coffee getCoffee()
	{
		return coffee;
	}

	public int getNumberOfRows()
	{
		return numberOfRows;
	}

	static public int getNumColumns()
	{
		return numColumns;
	}
	
	static public Class<?> getColumnClassName(int column)
	{
		try
		{
			switch(column)
			{
				case 1:
					return Class.forName(String.class.getName());
				case 2:
					return Class.forName(Integer.class.getName());
				case 3:
					return Class.forName(Double.class.getName());
				case 4:
				case 5:
					return Class.forName(Integer.class.getName());
			}
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
		return Object.class; // se ocorrerem os problemas acima, assume tipo Object
	}
	
	public Object getColumn(int column)
	{
		switch(column)
		{
			case 1:
				return coffee.getName();
			case 2:
				return coffee.getSup_Id();
			case 3:
				return coffee.getPrice();
			case 4:
				return coffee.getSales();
			case 5:
				return coffee.getTotal();
		}
		return null;
	}
	
	
	static public String getColumnName(int column)
	{
		if(column > 0 && column < columnsNames.length)
			return columnsNames[column];
		return "";
	}
	
	public void save() throws SQLException, IllegalStateException
	{
		String newData = update + "('" + coffee.getName() + "', '" +
			coffee.getPrice() + "', '" + coffee.getSales() + "', '" + coffee.getSup_Id() +
			"', '" + coffee.getTotal() + "');";
		statement.executeUpdate(newData);
	}
	
	// configura nova string de consulta de banco de dados
	public void setQuery() throws SQLException, IllegalStateException
	{
		// especifica consulta e a executa
		resultSet = statement.executeQuery(query);

		// obt�m metadados para ResultSet
		// metaData = resultSet.getMetaData();

		// determina o n�mero de linhas em ResultSet
		resultSet.last(); // move para a �ltima linha
		numberOfRows = resultSet.getRow(); // obt�m n�mero de linha
		resultSet.first();

		coffee.setName(resultSet.getString(1));
		coffee.setSup_Id(resultSet.getInt(2));
		coffee.setPrice(resultSet.getDouble(3));
		coffee.setSales(resultSet.getInt(4));
		coffee.setTotal(resultSet.getInt(5));
	} // fim do m�todo setQuery

	// fecha Statement e Connection
	public void disconnectFromDatabase()
	{
		// fecha Statement e Connection
		try
		{
			statement.close();
			DBConnection.getConnection().close();
		} // fim do try
		catch (SQLException sqlException)
		{
			sqlException.printStackTrace();
		} // fim do catch
	} // fim do m�todo disconnectFromDatabase

}
