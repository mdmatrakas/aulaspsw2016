/**
 * 
 */
package coffee_shop.GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import coffee_shop.tableModel.CoffeeTableModel;
import coffee_shop.tableModel.SupplierTableModel;

/**
 * @author mdmatrakas
 * 
 */
public class DisplayTables extends JFrame
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	CoffeeTableModel			coffeeTableModel;
	SupplierTableModel			supplierTableModel;
	boolean						coffeeSupplier		= true;
	JTable						resultTable;
	JButton coffeeButton;
	JButton supplierButton;

	public DisplayTables()
	{
		coffeeTableModel = new CoffeeTableModel();
		supplierTableModel = new SupplierTableModel();

		// configura o JButton para enviar consulta
		coffeeButton = new JButton("Coffees");
		supplierButton = new JButton("Suppliers");

		// cria o Box para gerenciar o posicionamento da queryArea e do
		// submitButton na GUI
		Box box = Box.createHorizontalBox();
		box.add(coffeeButton);
		box.add(supplierButton);
		coffeeButton.setEnabled(false);
		supplierButton.setEnabled(true);

		// cria o delegado JTable para tableModel
		resultTable = new JTable(coffeeTableModel);

		// posiciona os componentes GUI no painel de conte�do
		add(box, BorderLayout.NORTH);
		add(new JScrollPane(resultTable), BorderLayout.CENTER);

		// cria evento ouvinte para coffeeButton
		coffeeButton.addActionListener(new ActionListener()
		{
			// passa consulta para modelo de tabela
			public void actionPerformed(ActionEvent event)
			{
				if (coffeeSupplier)
					return;

				// troca o modela da tabela
				resultTable.setModel(coffeeTableModel);
				resultTable.setRowSelectionInterval(0, 0);
				coffeeButton.setEnabled(false);
				supplierButton.setEnabled(true);
				coffeeSupplier = !coffeeSupplier;
			
			} // fim do m�todo actionPerformed
		} // fim da classe ActionListener interna
				); // fim da chamada para addActionListener

		// cria evento ouvinte para supplierButton
		supplierButton.addActionListener(new ActionListener()
		{
			// passa consulta para modelo de tabela
			public void actionPerformed(ActionEvent event)
			{
				if (!coffeeSupplier)
					return;

				// troca o modela da tabela
				resultTable.setModel(supplierTableModel);
				resultTable.setRowSelectionInterval(0, 0);
				coffeeButton.setEnabled(true);
				supplierButton.setEnabled(false);
				coffeeSupplier = !coffeeSupplier;
				
			} // fim do m�todo actionPerformed
		} // fim da classe ActionListener interna
				); // fim da chamada para addActionListener

		resultTable.setRowSelectionInterval(0, 0);
		setSize(500, 250); // configura o tamanho da janela
		setVisible(true); // exibe a janela

		// disp�e da janela quando o usu�rio fecha o aplicativo (isso
		// sobrescreve o padr�o de HIDE_ON_CLOSE)
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// assegura que a conex�o de banco de dados � fechada quando usu�rio
		// fecha o aplicativo
		addWindowListener(new WindowAdapter()
		{
			// desconecta-se do banco de dados e sai quando a janela for fechada
			public void windowClosed(WindowEvent event)
			{
				//coffeeTableModel.disconnectFromDatabase();
				supplierTableModel.disconnectFromDatabase();
				System.exit(0);
			} // fim do m�todo windowClosed
		} // fim da classe WindowAdapter interna
		); // fim da chamada a addWindowListener
	}
}
