/**
 * 
 */
package coffee_shop;

import coffee_shop.GUI.DisplayTables;

/**
 * @author mdmatrakas
 * 
 */
public class App
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new DisplayTables();
	}

}
