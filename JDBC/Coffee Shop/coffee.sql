CREATE DATABASE IF NOT EXISTS coffee_shop;

USE coffee_shop;

DROP TABLE IF EXISTS coffees;
DROP TABLE IF EXISTS suppliers;

CREATE TABLE suppliers (
	SUP_ID INT NOT NULL,
	SUP_NAME varchar (50) NOT NULL,
	STREET varchar (50) NOT NULL,
	CITY varchar (50) NOT NULL,
	STATE varchar (25) NOT NULL,
	ZIP int NOT NULL,
	PRIMARY KEY (SUP_ID)
) ;

CREATE TABLE coffees (
	COF_NAME varchar (32) NOT NULL,
	SUP_ID INT NOT NULL,
	PRICE REAL NOT NULL,
	SALES INT,
	TOTAL INT,
	PRIMARY KEY (COF_NAME),
	INDEX(SUP_ID),
	FOREIGN KEY (SUP_ID) REFERENCES suppliers(SUP_ID)
) ;

insert into suppliers (SUP_ID, SUP_NAME, STREET, CITY, STATE, ZIP) values (101, 'Acme, Inc.', '99 Market Street', 'Groundsville', 'CA', 0);
insert into suppliers (SUP_ID, SUP_NAME, STREET, CITY, STATE, ZIP) values (49, 'Superior Coffee', '1 Party Place', 'Mendocino', 'CA', 0);
insert into suppliers (SUP_ID, SUP_NAME, STREET, CITY, STATE, ZIP) values (150, 'The High Ground', '100 Coffee Lane', 'Meadows', 'CA', 0);

insert into coffees (COF_NAME, SUP_ID, PRICE, SALES, TOTAL) values ('Colombian', 101, 7.99, 0, 0);
insert into coffees (COF_NAME, SUP_ID, PRICE, SALES, TOTAL) values ('French_Roast', 49, 8.99, 0, 0);
insert into coffees (COF_NAME, SUP_ID, PRICE, SALES, TOTAL) values ('Espresso', 150, 9.99, 0, 0);
insert into coffees (COF_NAME, SUP_ID, PRICE, SALES, TOTAL) values ('Colombian_Decaf', 101, 8.99, 0, 0);
insert into coffees (COF_NAME, SUP_ID, PRICE, SALES, TOTAL) values ('French_Roast_Decaf', 49, 9.99, 0, 0);

