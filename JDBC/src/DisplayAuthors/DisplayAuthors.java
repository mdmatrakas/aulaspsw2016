package DisplayAuthors;

// Fig. 25.25: DisplayAuthors.java
// Exibindo o conte�do da tabela authors.
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class DisplayAuthors
{
	// nome do driver JDBC e URL do banco de dados
	static final String	JDBC_DRIVER		= "com.mysql.jdbc.Driver";
	static final String	DATABASE_URL	= "jdbc:mysql://localhost/books";

	// carrega o aplicativo
	public static void main(String args[])
	{
		Connection connection = null; // gerencia a conex�o
		Statement statement = null; // instru��o de consulta

		// conecta-se ao banco de dados books e o consulta
		try
		{
			Class.forName(JDBC_DRIVER); // carrega classe de driver do banco de dados

			// estabelece conex�o com o banco de dados
			String usr = JOptionPane.showInputDialog("Usu�rio");
			String pass = JOptionPane.showInputDialog("Senha");
			connection = DriverManager.getConnection(DATABASE_URL, usr, pass);

			// cria Statement para consultar banco de dados
			statement = connection.createStatement();

			// consulta o banco de dados
			ResultSet resultSet = statement.executeQuery("SELECT authorID, firstName, lastName FROM authors");

			// processa resultados da consulta
			ResultSetMetaData metaData = resultSet.getMetaData();
			int numberOfColumns = metaData.getColumnCount();
			System.out.println("Authors Table of Books Database:");

			for (int i = 1; i <= numberOfColumns; i++)
				System.out.printf("%-8s\t", metaData.getColumnName(i));
			System.out.println();

			while (resultSet.next())
			{
				for (int i = 1; i <= numberOfColumns; i++)
					System.out.printf("%-8s\t", resultSet.getObject(i));
				System.out.println();
			} // fim do while
		} // fim do try
		catch (SQLException sqlException)
		{
			sqlException.printStackTrace();
			System.exit(1);
		} // fim do catch
		catch (ClassNotFoundException classNotFound)
		{
			classNotFound.printStackTrace();
			System.exit(1);
		} // fim do catch
		finally
		// assegura que a instru��o e conex�o s�o fechadas adequadamente
		{
			try
			{
				statement.close();
				connection.close();
			} // fim do try
			catch (Exception exception)
			{
				exception.printStackTrace();
				System.exit(1);
			} // fim do catch
		} // fim do finally
	} // fim de main
} // fim da classe DisplayAuthors

/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and * Pearson Education,
 * Inc. All Rights Reserved. * * DISCLAIMER: The authors and publisher of this
 * book have used their * best efforts in preparing the book. These efforts
 * include the * development, research, and testing of the theories and programs
 * * to determine their effectiveness. The authors and publisher make * no
 * warranty of any kind, expressed or implied, with regard to these * programs
 * or to the documentation contained in these books. The authors * and publisher
 * shall not be liable in any event for incidental or * consequential damages in
 * connection with, or arising out of, the * furnishing, performance, or use of
 * these programs. *
 *************************************************************************/
