package InsertAuthors;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class DisplayInsertAuthors 
{
	   // nome do driver JDBC e URL do banco de dados
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";        
	   static final String DATABASE_URL = "jdbc:mysql://localhost/books";
      static Connection connection = null; // gerencia a conex�o
      static Statement statement = null; // instru��o de consulta 

      static void connect()
      {
	      // conecta-se ao banco de dados books e o consulta 
	      try 
	      {
	         Class.forName( JDBC_DRIVER ); // carrega classe de driver do banco de dados

	         // estabelece conex�o com o banco de dados
	         connection =                                                     
	            //DriverManager.getConnection( DATABASE_URL, "jhtp6", "jhtp6" );
	         	DriverManager.getConnection( DATABASE_URL, "root", "root" );

	         // cria Statement para consultar banco de dados
	         statement = connection.createStatement();
	      }  // fim do try
	      catch (SQLException sqlException)                                
	      {                                                                  
	         sqlException.printStackTrace();
	         System.exit( 1 );                                               
	      } // fim do catch
	      catch (ClassNotFoundException classNotFound)                     
	      {                                                                  
	         classNotFound.printStackTrace();            
	         System.exit( 1 );                                               
	      } // fim do catch      
      }
      
      static void disconnect()
      {
	         try                                                        
	         {                                                          
	            statement.close();                                      
	            connection.close();                                     
	         } // fim do try
	         catch ( Exception exception )                              
	         {                                                          
	            exception.printStackTrace();                            
	            System.exit( 1 );                                       
	         } // fim do catch    	  
      }

      static void show()
      {
	      try 
	      {
	         // consulta o banco de dados 
	         ResultSet resultSet = statement.executeQuery(            
	            "SELECT authorID, firstName, lastName FROM authors" );
	         
	         // processa resultados da consulta
	         ResultSetMetaData metaData = resultSet.getMetaData();
	         int numberOfColumns = metaData.getColumnCount();     
	         System.out.println( "Authors Table of Books Database:" );
	         
	         
	         for ( int i = 1; i <= numberOfColumns; i++ )
	            System.out.printf( "%-8s\t", metaData.getColumnName( i ));
	         System.out.println();
	         
	         while (resultSet.next()) 
	         {
	            for ( int i = 1; i <= numberOfColumns; i++ )
	               System.out.printf( "%-8s\t", resultSet.getObject( i ));
	            System.out.println();
	         } // fim do while
	      }  // fim do try
	      catch (SQLException sqlException)                                
	      {                                                                  
	         sqlException.printStackTrace();
	         System.exit( 1 );                                               
	      } // fim do catch    	  
      }
      
      static void insert()
      {
    	  try
    	  {
	         statement.execute("insert into authors (firstName,lastName) values ('Jose','Silva')" );
	         
    	  }
	      catch (SQLException sqlException)                                
	      {                                                                  
	         sqlException.printStackTrace();
	         System.exit( 1 );                                               
	      } // fim do catch    	  
      }
      
      // carrega o aplicativo
	   public static void main( String args[] )
	   {
		   connect();
		   show();
		   insert();
		   show();
		   disconnect();
	   } // fim de main
}
