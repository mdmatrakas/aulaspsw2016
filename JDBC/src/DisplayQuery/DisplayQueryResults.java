package DisplayQuery;
// Fig. 25.31: DisplayQueryResults.java
// Exibe o conte�do da tabela Authors no
// banco de dados Books.
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTable;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.Box;

@SuppressWarnings("serial")
public class DisplayQueryResults extends JFrame 
{
   // driver JDBC e URL de banco de dados
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
   static final String DATABASE_URL = "jdbc:mysql://localhost/books";
   static final String USERNAME= "root"; //"jhtp6";
   static final String PASSWORD= "root"; //"jhtp6";
   
   // consulta padr�o seleciona todas as linhas de tabela authors
   static final String DEFAULT_QUERY = "SELECT * FROM authors";
   
   private ResultSetTableModel tableModel;
   private JTextArea queryArea;
   
   // cria o ResultSetTableModel e GUI
   public DisplayQueryResults() 
   {   
      super( "Displaying Query Results" );
         
      // cria o ResultSetTableModel e exibe tabela de banco de dados
      try 
      {
         // cria o TableModel para resultados da consulta SELECT * FROM authors
         tableModel = new ResultSetTableModel( JDBC_DRIVER, DATABASE_URL,
            USERNAME, PASSWORD, DEFAULT_QUERY );                         

         // configura JTextArea em que o usu�rio digita consultas
         queryArea = new JTextArea( DEFAULT_QUERY, 3, 100 );
         queryArea.setWrapStyleWord( true );
         queryArea.setLineWrap( true );
         
         JScrollPane scrollPane = new JScrollPane( queryArea,
            ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, 
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER );
         
         // configura o JButton para enviar consulta
         JButton submitButton = new JButton( "Submit Query" );

         // cria o Box para gerenciar o posicionamento da queryArea e do
         // submitButton na GUI
         Box box = Box.createHorizontalBox();
         box.add( scrollPane );
         box.add( submitButton );

         // cria o delegado JTable para tableModel
         JTable resultTable = new JTable( tableModel );
         
         // posiciona os componentes GUI no painel de conte�do
         add( box, BorderLayout.NORTH );
         add( new JScrollPane( resultTable ), BorderLayout.CENTER );

         // cria evento ouvinte para submitButton
         submitButton.addActionListener( 
         
            new ActionListener() 
            {
               // passa consulta para modelo de tabela
               public void actionPerformed( ActionEvent event )
               {
                  // realiza uma nova consulta
                  try 
                  {
                     tableModel.setQuery( queryArea.getText() );
                  } // fim do try
                  catch ( SQLException sqlException ) 
                  {
                     JOptionPane.showMessageDialog( null, 
                        sqlException.getMessage(), "Database error", 
                        JOptionPane.ERROR_MESSAGE );
                     
                     // tenta recuperar a partir da consulta de usu�rio inv�lida
                     // executando consulta padr�o
                     try 
                     {
                        tableModel.setQuery( DEFAULT_QUERY );
                        queryArea.setText( DEFAULT_QUERY );
                     } // fim do try
                     catch ( SQLException sqlException2 ) 
                     {
                        JOptionPane.showMessageDialog( null, 
                           sqlException2.getMessage(), "Database error", 
                           JOptionPane.ERROR_MESSAGE );
         
                        // assegura que a conex�o de banco de dados est� fechada
                        tableModel.disconnectFromDatabase();   
         
                        System.exit( 1 ); // termina o aplicativo
                     } // fim do catch interno
                  } // fim do catch externo
               } // fim do m�todo actionPerformed
            } // fim da classe ActionListener interna 
         ); // fim da chamada para addActionListener

         setSize( 500, 250 ); // configura o tamanho da janela
         setVisible( true ); // exibe a janela 
      } // fim do try
      catch ( ClassNotFoundException classNotFound ) 
      {
         JOptionPane.showMessageDialog( null, 
            "MySQL driver not found", "Driver not found",
            JOptionPane.ERROR_MESSAGE );
         
         System.exit( 1 ); // termina o aplicativo
      } // fim do catch
      catch ( SQLException sqlException ) 
      {
         JOptionPane.showMessageDialog( null, sqlException.getMessage(), 
            "Database error", JOptionPane.ERROR_MESSAGE );
         
         // assegura que a conex�o de banco de dados est� fechada
         tableModel.disconnectFromDatabase();   
         
         System.exit( 1 );   // termina o aplicativo
      } // fim do catch
      
      // disp�e da janela quando o usu�rio fecha o aplicativo (isso sobrescreve
      // o padr�o de HIDE_ON_CLOSE)
      setDefaultCloseOperation( DISPOSE_ON_CLOSE );
      
      // assegura que a conex�o de banco de dados � fechada quando usu�rio fecha o aplicativo
      addWindowListener(
      
         new WindowAdapter() 
         {
            // desconecta-se do banco de dados e sai quando a janela for fechada
            public void windowClosed( WindowEvent event )              
            {                                                          
               tableModel.disconnectFromDatabase();                    
               System.exit( 0 );                                       
            } // fim do m�todo windowClosed 
         } // fim da classe WindowAdapter interna 
      ); // fim da chamada a addWindowListener
   } // fim do construtor DisplayQueryResults
   
   // executa o aplicativo
   public static void main( String args[] ) 
   {
      new DisplayQueryResults();     
   } // fim de main
} // fim da classe DisplayQueryResults 


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/