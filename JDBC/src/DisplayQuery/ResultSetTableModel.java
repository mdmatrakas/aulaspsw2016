package DisplayQuery;
// Fig. 25.28: ResultSetTableModel.java
// Um TableModel que fornece dados ResultSet a uma JTable.
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.table.AbstractTableModel;

// Linhas e colunas do ResultSet s�o contadas a partir de 1 e linhas e
// colunas JTable s�o contadas a partir de 0. Ao processar
// linhas ou colunas de ResultSet para utiliza��o em uma JTable, �
// necess�rio adicionar 1 ao n�mero de linha ou coluna para manipular
// a coluna apropriada de ResultSet (isto �, coluna 0 de JTable � a
// coluna de ResultSet 1 e a linha de JTable 0 � a linha de ResultSet 1).
@SuppressWarnings("serial")
public class ResultSetTableModel extends AbstractTableModel
{
   private Connection connection;
   private Statement statement;
   private ResultSet resultSet;
   private ResultSetMetaData metaData;
   private int numberOfRows;

   // monitora o status da conex�o de banco de dados
   private boolean connectedToDatabase = false;
   
   // construtor inicializa resultSet e obt�m seu objeto de metadados;
   // determina n�mero de linhas
   public ResultSetTableModel( String driver, String url, 
      String username, String password, String query ) 
      throws SQLException, ClassNotFoundException
   {
      // carrega classe de driver do banco de dados
      Class.forName( driver );

      // conecta-se ao banco de dados
      connection = DriverManager.getConnection( url, username, password );

      // cria Statement para consultar banco de dados
      statement = connection.createStatement(
         ResultSet.TYPE_SCROLL_INSENSITIVE,   
         ResultSet.CONCUR_READ_ONLY );        

      // atualiza status de conex�o de banco de dados 
      connectedToDatabase = true;         

      // configura consulta e a executa
      setQuery( query );
   } // fim do construtor ResultSetTableModel 

   // obt�m a classe que representa o tipo de coluna
   public Class<?> getColumnClass( int column )throws IllegalStateException
   {
      // assegura que o banco de dados conex�o est� dispon�vel
      if ( !connectedToDatabase )                                       
         throw new IllegalStateException( "Not Connected to Database" );

      // determina a classe Java de coluna
      try 
      {
         String className = metaData.getColumnClassName( column + 1 );
         
         // retorna objeto Class que representa className
         return Class.forName( className );              
      } // fim do try
      catch ( Exception exception ) 
      {
         exception.printStackTrace();
      } // fim do catch
      
      return Object.class; // se ocorrerem os problemas acima, assume tipo Object
   } // fim do m�todo getColumnClass 

   // obt�m n�mero de colunas em ResultSet
   public int getColumnCount()throws IllegalStateException
   {   
      // assegura que o banco de dados conex�o est� dispon�vel
      if ( !connectedToDatabase ) 
         throw new IllegalStateException( "Not Connected to Database" );

      // determina n�mero de colunas
      try 
      {
         return metaData.getColumnCount(); 
      } // fim do try
      catch ( SQLException sqlException ) 
      {
         sqlException.printStackTrace();
      } // fim do catch
      
      return 0; // se ocorrerem os problemas acima, retorna 0 para o n�mero de colunas
   } // fim do m�todo getColumnCount 

   // obt�m nome de uma coluna particular em ResultSet
   public String getColumnName( int column )throws IllegalStateException
   {    
      // assegura que o banco de dados conex�o est� dispon�vel
      if ( !connectedToDatabase ) 
         throw new IllegalStateException( "Not Connected to Database" );

      // determina o nome de coluna
      try 
      {
         return metaData.getColumnName( column + 1 );  
      } // fim do try
      catch ( SQLException sqlException ) 
      {
         sqlException.printStackTrace();
      } // fim do catch
      
      return ""; // se ocorrerem problemas, retorna string vazia para nome de coluna
   } // fim do m�todo getColumnName

   // retorna n�mero de linhas em ResultSet
   public int getRowCount()throws IllegalStateException
   {      
      // assegura que o banco de dados conex�o est� dispon�vel
      if ( !connectedToDatabase ) 
         throw new IllegalStateException( "Not Connected to Database" );
 
      return numberOfRows;
   } // fim do m�todo getRowCount 

   // obt�m valor na linha e coluna particular
   public Object getValueAt( int row, int column )
      throws IllegalStateException
   {
      // assegura que o banco de dados conex�o est� dispon�vel
      if ( !connectedToDatabase ) 
         throw new IllegalStateException( "Not Connected to Database" );

      // obt�m um valor na linha e coluna de ResultSet especificada 
      try 
      {
         resultSet.absolute( row + 1 );
         return resultSet.getObject( column + 1 );
      } // fim do try
      catch ( SQLException sqlException ) 
      {
         sqlException.printStackTrace();
      } // fim do catch
      
      return ""; // se ocorrerem problemas, retorna objeto string vazio
   } // fim do m�todo getValueAt 
   
   // configura nova string de consulta de banco de dados
   public void setQuery( String query ) 
      throws SQLException, IllegalStateException 
   {
      // assegura que o banco de dados conex�o est� dispon�vel
      if ( !connectedToDatabase ) 
         throw new IllegalStateException( "Not Connected to Database" );

      // especifica consulta e a executa
      resultSet = statement.executeQuery( query );

      // obt�m metadados para ResultSet
      metaData = resultSet.getMetaData(); 

      // determina o n�mero de linhas em ResultSet
      resultSet.last();                   // move para a �ltima linha
      numberOfRows = resultSet.getRow();  // obt�m n�mero de linha
      
      // notifica a JTable de que modelo foi alterado
      fireTableStructureChanged();           
   } // fim do m�todo setQuery 

   // fecha Statement e Connection
   public void disconnectFromDatabase()            
   {                                               
      if ( !connectedToDatabase )                  
         return;                                   
                                                   
      // fecha Statement e Connection
      try                                          
      {                                            
         statement.close();                        
         connection.close();                       
      } // fim do try
      catch ( SQLException sqlException )          
      {                                            
         sqlException.printStackTrace();           
      } // fim do catch
      finally  // atualiza status de conex�o de banco de dados 
      {                                            
         connectedToDatabase = false;              
      } // fim do finally                            
   } // fim do m�todo disconnectFromDatabase 
}  // fim da classe ResultSetTableModel 


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/