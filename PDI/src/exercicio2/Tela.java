package exercicio2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Random;

public class Tela {

	private JFrame frame;
	JRadioButton radioButton;
	JRadioButton radioButton_1;
	JRadioButton radioButton_2;
	JRadioButton radioButton_3;
	JRadioButton radioButton_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tela window = new Tela();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Tela() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final desenho janela = new desenho();
		janela.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		frame.getContentPane().add(janela);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JButton btnOlharAtravezDa = new JButton("Olhar atrav\u00E9s da camera:");
		btnOlharAtravezDa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(radioButton.isSelected()){
					janela.Camera = 1;
				}else{
					if(radioButton_1.isSelected()){
						janela.Camera = 2;
					}else{
						if(radioButton_2.isSelected()){
							janela.Camera = 3;
						}else{
							if(radioButton_3.isSelected()){
								janela.Camera = 4;
							}else{
								janela.Camera = 5;
							}
						}
					}
				}
			}
		});
		menuBar.add(btnOlharAtravezDa);
		
		radioButton = new JRadioButton("1");
		radioButton.setSelected(true);
		menuBar.add(radioButton);
		
		radioButton_1 = new JRadioButton("2");
		menuBar.add(radioButton_1);
		
		radioButton_2 = new JRadioButton("3");
		menuBar.add(radioButton_2);
		
		radioButton_3 = new JRadioButton("4");
		menuBar.add(radioButton_3);
		
		radioButton_4 = new JRadioButton("5");
		menuBar.add(radioButton_4);
		ButtonGroup bg = new ButtonGroup();  
		bg.add(radioButton_1);
		bg.add(radioButton_2);
		bg.add(radioButton_3);
		bg.add(radioButton_4);
		bg.add(radioButton);
		
		JButton btnInserirPontosPara = new JButton("Inserir pontos para a nova curva");
		btnInserirPontosPara.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Point3D[] potosCurva = new Point3D[8];
				try { 
					for(int i = 0; i<8;i++){
						int k = i+1;
						float x = Float.parseFloat(JOptionPane.showInputDialog("Informe a cordenada x do ponto "+k, "Digite aqui."));
						float y = Float.parseFloat(JOptionPane.showInputDialog("Informe a cordenada y do ponto "+k, "Digite aqui."));
						float z = Float.parseFloat(JOptionPane.showInputDialog("Informe a cordenada z do ponto "+k, "Digite aqui."));
						potosCurva[i] = new Point3D(x,y,z);
					}
					janela.novosPontosCurva(potosCurva);
				} 
				catch (Exception e) {  }
				
			}
		});
		menuBar.add(btnInserirPontosPara);
	}
	
	class desenho extends JPanel implements Runnable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		
		Thread MoveCubo; 
		Desenha3d zo;
		Obj3D os,os2;
		double pre=0,t1=0;

		int mx,my,k,i,i1,i2,i3,i4,parameter;
		double p1,x2,p2,d1,d2,d3,d4;
		double[][] mocnina = new double[12][42];
		double[][] rv = new double[30][3];
		double[][][] rvl = new double[30][30][3];

		Color CorCurva = new Color(20,250,20);

		int pres,polx,poly;
		int Camera=1;

		int nPoints=30;
		int[] xP = new int[nPoints];
		int[] yP = new int[nPoints];

		Image offscreen;
		Dimension offscreensize;
		Graphics go; 
		double alpha = 0;

		/**
		 * Create the application.
		 */
		public desenho() {
			Random gerador = new Random();
			for (int m = 0; m < 8 ; m++) {
				for (int n = 0; n < 3; n++) {
					rv[m][n] = gerador.nextInt(200);
				}
			}
			start();
			
		}
		
		public void novosPontosCurva(Point3D[] potosCurva){
			
			for (int i = 0; i < 8; i++) {
				rv[i][0]=potosCurva[i].x;
				rv[i][1]=potosCurva[i].y;
				rv[i][2]=potosCurva[i].z;
			}
			start();
		}
		public void start() { 
			MoveCubo = new Thread(this); 
			MoveCubo.start(); 
		}

		public void run() 
		{
			Thread me = Thread.currentThread();
			while (MoveCubo == me) 
			{
				try { 
					Thread.currentThread();
					Thread.sleep(80);
					pre += 0.005D;
					alpha += 0.01;
				} 
				catch (InterruptedException e) {  }
				if (parameter!=3)    repaint();

			}
		}
		
		public void paint(Graphics g) 
		{
			Dimension d = getSize();
			polx=(int)(d.width/2);
			poly=(int)(d.height/2);

			if ((offscreen == null) || (d.width != offscreensize.width) || (d.height != offscreensize.height)) 
			{
				offscreen = createImage(d.width, d.height);
				offscreensize = d;
				go = offscreen.getGraphics();
				go.setFont(getFont());
			}

			go.setColor(getBackground());
			go.fillRect(0, 0, d.width, d.height);
			go.setColor(Color.black);
			double[] arrayOfDouble2;
			double[] arrayOfDouble1;
			arrayOfDouble1 = new double[3];
			arrayOfDouble2 = new double[] { mx, my, 0.0 };
			os = new Obj3D(arrayOfDouble1, Camera);
			zo = new Desenha3d(polx, poly, Camera);
			arrayOfDouble2[0] = 200.0D;
			arrayOfDouble2[1] = 0.0D;
			arrayOfDouble2[2] = 0.0D;
			os2 = new Obj3D(arrayOfDouble2, Camera);
			zo.drawline(go,os, os2);
			arrayOfDouble2[0] = 0.0D;
			arrayOfDouble2[1] = 200.0D;
			arrayOfDouble2[2] = 0.0D;
			os2 = new Obj3D(arrayOfDouble2, Camera);
			zo.drawline(go, os, os2);
			arrayOfDouble2[0] = 0.0D;
			arrayOfDouble2[1] = 0.0D;
			arrayOfDouble2[2] = 200.0D;
			os2 = new Obj3D(arrayOfDouble2, Camera);
			zo.drawline(go, os, os2);

			zo.curvaBezier(go, CorCurva, rv, 7);
			zo.DesenhaObjCurva(go, CorCurva, rv, 7,t1);
			t1 += 0.005D;
			if (t1 > 1.0D) {
				t1 = 0.0D;
			}

			g.drawImage(offscreen, 0, 0, null); 
		}
	}
}
