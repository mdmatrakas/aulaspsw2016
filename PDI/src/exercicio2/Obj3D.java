package exercicio2;

public class Obj3D {
	double[] bod3d = new double[3];
	int[] pix = new int[2];
	double[] pixwin = new double[4];
	double[] pa1 = { -0.87D, -0.7D, -0.8D, -0.71D, -0.43D, -1.0D };
	double[] pa2 = { -0.5D, -0.3D, -0.25D, -0.71D, -0.25D, 0 };
	double[] qa1 = { 0.87D, 0.8D, 0.7D, 0.71D, 1.0D, 0.43D };
	double[] qa2 = { -0.5D, -0.2D, -0.3D, -0.71D, 0, -0.25D };
	double[] ra2 = { 1.0D, 0.8D, 0.9D, 1.0D, 1.0D, 1.0D };
	int kt;

	Obj3D(double[] paramArrayOfDouble, int paramInt) {
		this.bod3d[0] = paramArrayOfDouble[0];
		this.bod3d[1] = paramArrayOfDouble[1];
		this.bod3d[2] = paramArrayOfDouble[2];
		this.kt = paramInt;
	}

	public int x() {
		int i = (int)(this.bod3d[0] * this.pa1[this.kt] + this.bod3d[1] * this.qa1[this.kt]);
		return i;
	}

	public int y() {
		int i = (int)(this.bod3d[0] * this.pa2[this.kt] + this.bod3d[1] * this.qa2[this.kt] + this.bod3d[2] * this.ra2[this.kt]);
		return i;
	}

	public int z() {
		int i = (int)(this.bod3d[2] * 2.0D);
		return i;
	}
}




