package exercicio2;


import java.awt.Color;
import java.awt.Graphics;



class Desenha3d  {
	double taxa = 1.5;
	double[] bod3 = new double[3];
	int stredx,stredy,typpre;
	Obj3D[] os = new Obj3D[30];
	Obj3D[][] osl = new Obj3D[30][30];
	Cubo obj = new Cubo();
	Color CorLinhas = new Color(255,255,255);
	Obj3D osp;
	int centerX, centerY;
	int i1,i2,i3,i4,i5,i6,i7,i8;
	double x2,po1,po2,t,u,v;
	public double fact(int paramInt)
	{
		x2 = 1.0D;
		if (paramInt != 0) {
			for (int i = 1; i < paramInt + 1; i++) {
				x2 *= i;
			}
		}
		return x2;
	}

	public double calc(int paramInt1, int paramInt2, double paramDouble)
	{
		if (paramDouble > 0.001D)
		{
			po1 = fact(paramInt1);
			po2 = fact(paramInt2);
			po1 /= po2;
			po2 = fact(paramInt1 - paramInt2);
			if (po2 == 0.0D)
			{
				po1 = 1.0D;
				po2 = 1.0D;
			}
			else
			{
				po1 /= po2;
			}
			po2 = fx(1.0D - paramDouble, paramInt1 - paramInt2);
			po1 *= po2;
			po2 = fx(paramDouble, paramInt2);
			po1 *= po2;
			x2 = po1;
		}
		else
		{
			x2 = 0.0D;
		}
		return x2;
	}
	
	public double fx(double paramDouble, int paramInt)
	{
		x2 = 1.0D;
		if ((paramInt > 0.001D) && (paramDouble >= 0.001D)) {
			for (int i = 1; i < paramInt + 1; i++) {
				x2 *= paramDouble;
			}
		}
		return x2;
	}

	public void curvaBezier(Graphics paramGraphics, Color paramColor, double[][] paramArrayOfDouble, int paramInt)
	{
		double[] arrayOfDouble = { 0.0D, 0.0D, 0.0D };
		int i = paramInt;
		paramGraphics.setColor(CorLinhas);
		for (int j = 0; j < i + 1; j++)
		{
			arrayOfDouble[0] = paramArrayOfDouble[j][0];arrayOfDouble[1] = paramArrayOfDouble[j][1];arrayOfDouble[2] = paramArrayOfDouble[j][2];
			os[j] = new Obj3D(arrayOfDouble, typpre);
		}
		for (int k = 0; k < i; k++)
		{
			i1 = (int) (os[k].x()*taxa + stredx);
			i2 = (int) (stredy - os[k].y()*taxa);

			i3 = (int) (os[(k + 1)].x()*taxa + stredx);
			i4 = (int) (stredy - os[(k + 1)].y()*taxa);
			paramGraphics.drawLine(i3, i4, i1, i2);
		}
		paramGraphics.setColor(paramColor);



		int m = 0;int n = 0;
		for (int i9 = 1; i9 < 101; i9++)
		{
			t = i9;
			t /= 101.0D;

			double d2 = 0.0D;
			double d3 = 0.0D;
			double d5 = 0.0D;
			for (int i10 = 0; i10 < i + 1; i10++)
			{
				double d1 = calc(i, i10, t);

				double d4 = paramArrayOfDouble[i10][0];d2 += d4 * d1;
				d4 = paramArrayOfDouble[i10][1];d3 += d4 * d1;
				d4 = paramArrayOfDouble[i10][2];d5 += d4 * d1;
			}
			arrayOfDouble[0] = d2;arrayOfDouble[1] = d3;arrayOfDouble[2] = d5;
			osp = new Obj3D(arrayOfDouble, typpre);
			i1 = (int) (osp.x()*taxa + stredx);
			i2 = (int) (stredy - osp.y()*taxa);
			if (i9 == 1)
			{
				m = i1;n = i2;
			}
			paramGraphics.drawLine(i1, i2, m, n);
			m = i1;n = i2;
		}
	}
	
	Desenha3d(int paramInt1, int paramInt2, int paramInt3)
	{
		stredx = paramInt1;
		stredy = paramInt2;
		typpre = paramInt3;
	}

	public void drawline(Graphics paramGraphics, Obj3D paramObject3d1, Obj3D paramObject3d2)
	{
		i1 = (int) (paramObject3d1.x()*taxa + stredx);
		i2 = (int) (stredy - paramObject3d1.y()*taxa);
		i3 = (int) (paramObject3d2.x()*taxa + stredx);
		i4 = (int) (stredy - paramObject3d2.y()*taxa);
		paramGraphics.drawLine(i3, i4, i1, i2);
	}

	public void catmulrom(Graphics paramGraphics, Color paramColor, double[][] paramArrayOfDouble, int paramInt)
	{
		double[] arrayOfDouble = { 0.0D, 0.0D, 0.0D };
		int i = paramInt;
		//      paramGraphics.drawString("pocetujkkh =" + paramInt, 100, 100);
		paramGraphics.setColor(CorLinhas);
		for (int j = 0; j < i + 1; j++)
		{
			arrayOfDouble[0] = paramArrayOfDouble[j][0];arrayOfDouble[1] = paramArrayOfDouble[j][1];arrayOfDouble[2] = paramArrayOfDouble[j][2];
			os[j] = new Obj3D(arrayOfDouble, typpre);
		}
		for (int k = 0; k < i; k++)
		{
			i1 = (int) (os[k].x()*taxa + stredx);
			i2 = (int) (stredy - os[k].y()*taxa);

			i3 = (int) (os[(k + 1)].x()*taxa + stredx);
			i4 = (int) (stredy - os[(k + 1)].y()*taxa);
			paramGraphics.drawLine(i3, i4, i1, i2);
		}
		paramGraphics.setColor(paramColor);


		double[][] arrayOfDouble1 = { { 1.0D, -0.5D, 1.0D, -0.5D }, { -1.0D, 0.0D, 2.5D, 1.5D }, { 0.0D, 0.5D, 2.0D, -1.5D }, { 0.5D, -0.5D, -0.5D, 0.5D } };
		int m = 0;int n = 0;
		for (int i9 = 1; i9 < paramInt - 2; i9++) {
			for (int i10 = 1; i10 < 101; i10++)
			{
				for (int i11 = 0; i11 < 3; i11++)
				{
					t = i10;
					t /= 101.0D;
					double d4 = 1.0D;
					double d1 = d4 * t;
					double d2 = d1 * t;
					double d3 = d2 * t;
					double d5 = 0.0D;
					for (int i12 = 0; i12 < 4; i12++) {
						d5 += (d4 * arrayOfDouble1[0][i12] + d1 * arrayOfDouble1[1][i12] + d2 * arrayOfDouble1[2][i12] + d3 * arrayOfDouble1[3][i12]) * paramArrayOfDouble[(i9 - 1 + i12)][i11];
					}
					arrayOfDouble[i11] = d5;
				}
				if ((i9 == 1) && (i10 == 1))
				{
					paramGraphics.drawString("pocetujkkh =" + arrayOfDouble[0], 100, 130);
					paramGraphics.drawString("pocetujkkh =" + arrayOfDouble[1], 100, 160);
					paramGraphics.drawString("pocetujkkh =" + arrayOfDouble[2], 100, 190);
				}
				osp = new Obj3D(arrayOfDouble, typpre);
				i1 = (int) (osp.x()*taxa + stredx);
				i2 = (int) (stredy - osp.y()*taxa);

				paramGraphics.drawLine(i1, i2, m, n);
				m = i1;n = i2;
			}
		}
	}

	public void DesenhaObjCurva(Graphics paramGraphics, Color corCurva, double[][] paramArrayOfDouble, int paramInt, double paramDouble)
	{
		double[] arrayOfDouble = { 0.0D, 0.0D, 0.0D };
		paramGraphics.setColor(corCurva);



		double d2 = 0.0D;double d3 = 0.0D;double d5 = 0.0D;
		for (int k = 0; k < paramInt + 1; k++)
		{
			double d1 = calc(paramInt, k, paramDouble);
			double d4 = paramArrayOfDouble[k][0];d2 += d4 * d1;
			d4 = paramArrayOfDouble[k][1];d3 += d4 * d1;
			d4 = paramArrayOfDouble[k][2];d5 += d4 * d1;
		}
		arrayOfDouble[0] = d2;arrayOfDouble[1] = d3;arrayOfDouble[2] = d5;
		osp = new Obj3D(arrayOfDouble, typpre);
		i1 = (int) (osp.x()*taxa + stredx);
		i2 = (int) (stredy - osp.y()*taxa);

		DesenhaCubo(paramGraphics, i1, i2);

	}

	public void DesenhaCubo(Graphics g, int x, int y) //printa o objeto na curva(cubo)
	{  

		int tam = 20;
		centerX = (int) (x/taxa); centerY = (int) (y/taxa);
		obj.d = obj.rho * tam / obj.objSize;
		obj.eyeAndScreen();  
		// Horizontal edges at the bottom:
		line(g, 0, 1); line(g, 1, 2); line(g, 2, 3); line(g, 3, 0);
		// Horizontal edges at the top:
		line(g, 4, 5); line(g, 5, 6); line(g, 6, 7); line(g, 7, 4);
		// Vertical edges:
		line(g, 0, 4); line(g, 1, 5); line(g, 2, 6); line(g, 3, 7);
	}

	void line(Graphics g, int i, int j)
	{  
		Point2D P = obj.vScr[i], Q = obj.vScr[j];
		g.setColor(Color.BLUE);
		g.drawLine((int) (iX(P.x)*taxa), (int) ((int) (iY(P.y))*taxa), (int) (iX(Q.x)*taxa), (int) ((int) (iY(Q.y))*taxa));
	}

	int iX(double d){return (int) Math.round(centerX + d);}
	int iY(double d){return (int) Math.round(centerY - d);}

	public void superficie(Graphics paramGraphics, Color paramColor, double[][][] paramArrayOfDouble, int paramInt1, int paramInt2)
	{
		double[] arrayOfDouble = { 0.0D, 0.0D, 0.0D };
		int[][][] arrayOfInt = new int[100][100][2];
		int i = 10;int j = 10;
		paramGraphics.setColor(CorLinhas);
		for (int k = 0; k < paramInt1 + 1; k++) {
			for (int m = 0; m < paramInt2 + 1; m++)
			{
				arrayOfDouble[0] = paramArrayOfDouble[k][m][0];arrayOfDouble[1] = paramArrayOfDouble[k][m][1];arrayOfDouble[2] = paramArrayOfDouble[k][m][2];
				osl[k][m] = new Obj3D(arrayOfDouble, typpre);
			}
		}
		for (int n = 0; n < paramInt1 + 1; n++) {
			for (int i9 = 0; i9 < paramInt2 + 1; i9++)
			{
				i5 = (osl[n][i9].x() + stredx);
				i6 = (stredy - osl[n][i9].y());
				if (n < paramInt1)
				{
					i3 = (osl[(n + 1)][i9].x() + stredx);
					i4 = (stredy - osl[(n + 1)][i9].y());
				}
				paramGraphics.drawOval(i3 - 2, i4 - 2, 4, 4);
				if (i9 < paramInt2)
				{
					i3 = (osl[n][(i9 + 1)].x() + stredx);
					i4 = (stredy - osl[n][(i9 + 1)].y());
				}
				paramGraphics.drawOval(i5 - 2, i6 - 2, 4, 4);
				paramGraphics.drawOval(i3 - 2, i4 - 2, 4, 4);
			}
		}
		paramGraphics.setColor(paramColor);


		int i15;
		for (int i12 = 0; i12 < i; i12++) {
			for (int i13 = 0; i13 < j; i13++)
			{
				u = i12;
				u /= i;
				v = i13;
				v /= j;

				double d2 = 0.0D;double d3 = 0.0D;double d5 = 0.0D;double d4 = 0.0D;
				for (int i14 = 0; i14 < paramInt1 + 1; i14++) {
					for (i15 = 0; i15 < paramInt2 + 1; i15++)
					{
						double d1 = calc(paramInt1, i14, u);
						double d6 = calc(paramInt2, i15, v);
						d4 = paramArrayOfDouble[i14][i15][0];d2 += d4 * d1 * d6;
						if ((i12 == 1) && (i13 == 1)) {
							paramGraphics.drawString("" + d2, 50 + i14 * 100, 40 + 30 * i15);
						}
						d4 = paramArrayOfDouble[i14][i15][1];d3 += d4 * d1 * d6;
						d4 = paramArrayOfDouble[i14][i15][2];d5 += d4 * d1 * d6;
					}
				}
				if ((i12 == 0) && (i13 == 0)) {
					paramGraphics.drawString("" + d4 + "<>" + d2 + "<>" + d3, 50, 40);
				}
				arrayOfDouble[0] = d2;arrayOfDouble[1] = d3;arrayOfDouble[2] = d5;
				osp = new Obj3D(arrayOfDouble, typpre);
				i7 = (osp.x() + stredx);
				i8 = (stredy - osp.y());
				arrayOfInt[i12][i13][0] = i7;arrayOfInt[i12][i13][1] = i8;

			}
		}
		for (int i14 = 0; i14 < i; i14++) {
			for (i15 = 0; i15 < j - 1; i15++)
			{
				paramGraphics.drawLine(arrayOfInt[i14][i15][0], arrayOfInt[i14][i15][1], arrayOfInt[(i14 + 1)][i15][0], arrayOfInt[(i14 + 1)][i15][1]);
				paramGraphics.drawLine(arrayOfInt[i14][i15][0], arrayOfInt[i14][i15][1], arrayOfInt[i14][(i15 + 1)][0], arrayOfInt[i14][(i15 + 1)][1]);
			}
		}
		paramGraphics.setColor(Color.BLUE);

		paramGraphics.drawLine(arrayOfInt[0][j][0], arrayOfInt[0][j][1], arrayOfInt[0][0][0], arrayOfInt[0][0][1]);
		paramGraphics.drawLine(arrayOfInt[i][0][0], arrayOfInt[i][0][1], arrayOfInt[0][0][0], arrayOfInt[0][0][1]);
	}
	
	public void PontoC(Graphics paramGraphics, Color paramColor, double[][] paramArrayOfDouble, int paramInt, double paramDouble)
	{
		double[] arrayOfDouble = { 0.0D, 0.0D, 0.0D };
		int i = paramInt;
		paramGraphics.setColor(CorLinhas);
		for (int j = 0; j < i + 1; j++)
		{
			arrayOfDouble[0] = paramArrayOfDouble[j][0];arrayOfDouble[1] = paramArrayOfDouble[j][1];arrayOfDouble[2] = paramArrayOfDouble[j][2];
			os[j] = new Obj3D(arrayOfDouble, typpre);
		}
		for (int k = 0; k < i; k++)
		{
			i1 = (int) (os[k].x()*taxa + stredx);
			i2 = (int) (stredy - os[k].y()*taxa);

			i3 = (int) (os[(k + 1)].x()*taxa + stredx);
			i4 = (int) (stredy - os[(k + 1)].y()*taxa);
			paramGraphics.drawLine(i3, i4, i1, i2);
		}
		paramGraphics.setColor(paramColor);



		double[][] arrayOfDouble1 = { { 1.0D, -0.5D, 1.0D, -0.5D }, { -1.0D, 0.0D, 2.5D, 1.5D }, { 0.0D, 0.5D, 2.0D, -1.5D }, { 0.5D, -0.5D, -0.5D, 0.5D } };
		int m = 0;int n = 0;
		for (int i9 = 1; i9 < paramInt - 2; i9++) {
			for (int i10 = 1; i10 < 101; i10++)
			{
				for (int i11 = 0; i11 < 3; i11++)
				{
					t = i10;
					t /= 101.0D;
					double d4 = 1.0D;
					double d1 = d4 * t;
					double d2 = d1 * t;
					double d3 = d2 * t;
					double d5 = 0.0D;
					for (int i12 = 0; i12 < 4; i12++) {
						d5 += (d4 * arrayOfDouble1[0][i12] + d1 * arrayOfDouble1[1][i12] + d2 * arrayOfDouble1[2][i12] + d3 * arrayOfDouble1[3][i12]) * paramArrayOfDouble[(i9 - 1 + i12)][i11];
					}
					arrayOfDouble[i11] = d5;
				}
				osp = new Obj3D(arrayOfDouble, typpre);
				i1 = (int) (osp.x() + stredx);
				i2 = (int) (stredy - osp.y());

				paramGraphics.drawLine(i1, i2, m, n);
				m = i1;n = i2;
			}
		}
	}
	
	class Cubo // Contains 3D object data
	{  
		float rho, theta=0.3F, phi=1.3F, d, objSize,
				v11, v12, v13, v21, v22, v23, v32, v33, v43;
		// Elements of viewing matrix V
		Point3D[] w;     // World coordinates
		Point2D[] vScr;  // Screen coordinates

		Cubo()
		{  w = new Point3D[8];
		vScr = new Point2D[8];
		// Bottom surface:
		w[0] = new Point3D( 1, -1, -1);
		w[1] = new Point3D( 1,  1, -1);
		w[2] = new Point3D(-1,  1, -1);
		w[3] = new Point3D(-1, -1, -1);
		// Top surface:
		w[4] = new Point3D( 1, -1,  1);
		w[5] = new Point3D( 1,  1,  1);
		w[6] = new Point3D(-1,  1,  1);
		w[7] = new Point3D(-1, -1,  1);
		objSize = (float)Math.sqrt(12F); 
		// = sqrt(2 * 2 + 2 * 2 + 2 * 2) 
		// = distance between two opposite vertices.
		rho = 5 * objSize; // For reasonable perspective effect
		}

		void initPersp()
		{  
			float costh = (float)Math.cos(theta), 
					sinth = (float)Math.sin(theta),
					cosph = (float)Math.cos(phi), 
					sinph = (float)Math.sin(phi);
			v11 = -sinth; v12 = -cosph * costh; v13 = sinph * costh;
			v21 = costh;  v22 = -cosph * sinth; v23 = sinph * sinth; 
			v32 = sinph;          v33 = cosph; 
			v43 = -rho;  
		} 

		void eyeAndScreen()
		{  
			initPersp();
			for (int i=0; i<8; i++)
			{  Point3D P = w[i];
			float x = (float) (v11 * P.x + v21 * P.y),
					y = (float) (v12 * P.x + v22 * P.y + v32 * P.z),
					z = (float) (v13 * P.x + v23 * P.y + v33 * P.z + v43);
			vScr[i] = new Point2D(-d * x/z, -d * y/z);
			} 
		}
	}
}
