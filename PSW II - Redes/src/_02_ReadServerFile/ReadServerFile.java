package _02_ReadServerFile;
// Fig. 24.3: ReadServerFile.java
// Utiliza um JEditorPane para exibir o conte�do de um arquivo em um servidor Web.
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.HyperlinkEvent;   
import javax.swing.event.HyperlinkListener;

@SuppressWarnings("serial")
public class ReadServerFile extends JFrame 
{
   private JTextField enterField; // JTextField para inserir o nome de site
   private JEditorPane contentsArea; // para exibir o site Web

   // configura a GUI
   public ReadServerFile()
   {
      super( "Simple Web Browser" );

      // cria o enterField e registra seu ouvinte
      enterField = new JTextField( "Enter file URL here" );
      enterField.addActionListener(
         new ActionListener() 
         {
            // obt�m o documento especificado pelo usu�rio
            public void actionPerformed( ActionEvent event )
            {
               getThePage( event.getActionCommand() );
            } // fim do m�todo actionPerformed
         } // fim da classe inner
      ); // fim da chamada para addActionListener

      add( enterField, BorderLayout.NORTH );

      contentsArea = new JEditorPane(); // cria a contentsArea
      contentsArea.setEditable( false );                       
      contentsArea.addHyperlinkListener(                       
         new HyperlinkListener()                               
         {                                                     
            // se usu�rio clicou no hyperlink, vai para a p�gina especificada
            public void hyperlinkUpdate( HyperlinkEvent event )
            {                                                  
               if ( event.getEventType() ==                    
                    HyperlinkEvent.EventType.ACTIVATED )       
                  getThePage( event.getURL().toString() );     
            } // fim do m�todo hyperlinkUpdate                    
         } // fim da classe inner
      ); // fim da chamada a addHyperlinkListener                   

      add( new JScrollPane( contentsArea ), BorderLayout.CENTER );
      setSize( 400, 300 ); // configura o tamanho da janela
      setVisible( true ); // mostra a janela
   } // fim do construtor ReadServerFile

   // carrega o documento
   private void getThePage( String location )
   {
      try // carrega o documento e exibe a localiza��o
      {
         contentsArea.setPage( location ); // configura a p�gina
         enterField.setText( location ); // configura o texto
      } // fim do try
      catch ( IOException ioException ) 
      {
         JOptionPane.showMessageDialog( this,
            "Error retrieving specified URL", "Bad URL", 
            JOptionPane.ERROR_MESSAGE );
      } // fim do catch
   } // fim do m�todo getThePage
} // fim da classe ReadServerFile


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/