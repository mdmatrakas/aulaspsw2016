package a05_12_ErroSobrecarga;

/**
 * Classe que demonstra que �todos sobrecarregados com assinaturas id�nticas resulta em erros de
 * compila��o, mesmo se os tipos de retorno forem diferentes.
 * 
 * @author Matrakas
 * 
 */
public class ErroSobrecargaMetodo
{
	/**
	 * Declara��o do m�todo quadrado com argumento int
	 * 
	 * @param x
	 *            Recebe um par�metro inteiro
	 * @return Retorna um valor inteiro
	 */
	@SuppressWarnings("static-method")
	public int quadrado(int x)
	{
		return x * x;
	}

	/**
	 * Segunda declara��o do m�todo quadrado com argumento int resulta em erros de compila��o mesmo
	 * que os tipos de retorno sejam diferentes
	 * 
	 * @param y
	 *            Recebe um par�metro inteiro
	 * @return Retorna um valor em ponto flutuante
	 */
/*	public double quadrado(int y)
	{
		return y * y;
	}
	*/
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
