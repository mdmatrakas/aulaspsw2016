package a05_01_1_Hora;


/**
 *  Declara��o de classe que mant�m a hora no formato de 24 horas.
 * @author Matrakas
 *
 */
public class Hora  
{
   private int hora;   // 0 - 23
   private int minuto; // 0 - 59
   private int segundo; // 0 - 59
   /**
    *  Configura um novo valor de hora usando UTC; 
    *  Assegura que os dados permane�am consistentes configurando valores inv�lidos como zero
    * @param h Novo valor de hora
    * @param m Novo valor de minuto
    * @param s Novo valor de segundo
    */
   public void setHora( int h, int m, int s )
   {
      hora = ( ( h >= 0 && h < 24 ) ? h : 0 );   // valida horas
      minuto = ( ( m >= 0 && m < 60 ) ? m : 0 ); // valida minutos
      segundo = ( ( s >= 0 && s < 60 ) ? s : 0 ); // valida segundos
   } 
   /**
    *  Converte em String no formato de hora universal (HH:MM:SS)
    * @return String no formato de hora universal (HH:MM:SS)
    */
   public String toStringUniversal()
   {
      return String.format( "%02d:%02d:%02d", hora, minuto, segundo );
   } 
   /**
    *  Converte em String no formato padr�o de hora (H:MM:SS AM ou PM)
    *  @return String no formato padr�o de hora (H:MM:SS AM ou PM)
    */
   public String toString()
   {
      return String.format( "%d:%02d:%02d %s",            
         ( ( hora == 0 || hora == 12 ) ? 12 : hora % 12 ),
         minuto, segundo, ( hora < 12 ? "AM" : "PM" ) );   
   } 
} 

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/
