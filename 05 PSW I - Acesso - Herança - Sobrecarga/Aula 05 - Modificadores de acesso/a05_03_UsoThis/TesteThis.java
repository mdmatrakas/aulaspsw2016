package a05_03_UsoThis;

/**
 * Demonstra��o do this utilizado impl�cita e explicitamente para refer�ncia a 
 * membros de um objeto.
 * 
 * @author Matrakas
 * 
 */
public class TesteThis
{
	public static void main(String args[])
	{
		HoraSimples hora1 = new HoraSimples(15, 30, 19);
		System.out.println(hora1.buildString());
		HoraSimples hora2 = new HoraSimples(5, 20, 05);
		System.out.println(hora2.buildString());
	}
}

/**
 * Classe HoraSimples para demonstrar o uso da refer�ncia "this"
 * 
 * @author Matrakas
 * 
 */
class HoraSimples
{
	private int	hora;		// 0-23
	private int	minuto;	// 0-59
	private int	segundo;	// 0-59

	/**
	 * se o construtor utilizar nomes de par�metro id�nticos a nomes de vari�veis 
	 * de inst�ncia a refer�ncia "this" ser� exigida para distinguir entre nomes
	 * 
	 * @param hora
	 *            Novo valor de hora
	 * @param minuto
	 *            Novo valor de minuto
	 * @param segundo
	 *            Novo valor de segundo
	 */
	public HoraSimples(int hora, int minuto, int segundo)
	{
		this.hora = hora; // configura a hora do objeto "this"
		this.minuto = minuto; // configura os minuto do objeto "this"
		this.segundo = segundo; // configura os segundo do objeto "this"
	}

	/**
	 * Utilizando "this" expl�cito e impl�cito para chamar toUniversalString
	 * 
	 * @return String com o resultado de duas chamadas ao m�todo 
	 * toStringUniversal()
	 */
	public String buildString()
	{
		return String.format("%24s: %s\n%24s: %s", 
				"this.toStringUniversal()", this.toStringUniversal(), 
				"toStringUniversal()", toStringUniversal());
	}

	/**
	 * Converte em String no formato de data/hora universal (HH:MM:SS)
	 * 
	 * @return String no formato de hora universal (HH:MM:SS)
	 */
	public String toStringUniversal()
	{
		// "this" n�o � requerido aqui para acessar vari�veis de inst�ncia,
		// porque o m�todo n�o tem vari�veis locais com os mesmos
		// nomes das vari�veis de inst�ncia
		return String.format("%02d:%02d:%02d", this.hora, this.minuto, 
				this.segundo);
	}
}

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/
