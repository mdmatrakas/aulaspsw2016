package a05_07_Empregado_static;

/**
 * Classe Empregado com uma vari�vel est�tica utilizada para manter uma 
 * contagem do n�mero de objetos Employee na mem�ria.
 * 
 * @author Matrakas
 */
public class Empregado
{
	private String		primeiroNome;
	private String		ultimoNome;
	/**
	 * Vari�vel est�tica, utilizada para contar o n�mero de inst�ncias da classe 
	 * Empregado existentes na mem�ria.
	 */
	private static int	 cont	= 0;	

	/**
	 * Inicializa Empregado, adiciona 1 a static cont e gera a sa�da de String 
	 * indicando que o construtor foi chamado
	 * 
	 * @param primeiro
	 *            String contendo o primeiro nome do empregado.
	 * @param ultimo
	 *            String contendo o �ltimo nome do empregado
	 */
	public Empregado(String primeiro, String ultimo)
	{
		primeiroNome = primeiro;
		ultimoNome = ultimo;

		cont++; // incrementa contagem est�tica de empregados
		System.out.printf("Construtor Empregado: %s %s; cont = %d\n", 
				primeiroNome, ultimoNome,	cont);
	}

	/**
	 * M�todo de finaliza��o do objeto. � chamado pelo coletor de lixo do Java. 
	 * Subtrai 1 do contador estatico cont quando o coletor de lixo chama finalize
	 * para limpar o objeto; E confirma se finalize foi chamado
	 */
	protected void finalize()
	{
		cont--; // decrementa contagem est�tica de empregados
		System.out.printf("Finalizador de Empregado: %s %s; cont = %d\n", 
				primeiroNome, ultimoNome,	cont);
	}

	/**
	 * M�todo est�tico para obter valor da contagem est�tica
	 * 
	 * @return O n�mero de inst�ncias de Empregado
	 */
	public static int getCont()
	{
		return cont;
	}

	/**
	 * M�todo de acesso que obt�m o primeiro nome
	 * 
	 * @return Uma String com o primeiro nome do empregado.
	 */
	public String getPrimeiroNome()
	{
		return primeiroNome;
	}

	/**
	 * M�todo de acesso que obt�m o ultimo nome
	 * 
	 * @return Uma String com o �ltimo nome do empregado.
	 */
	public String getUltimoNome()
	{
		return ultimoNome;
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/