package a05_11_SobrecargaMetodo;

/**
 * Declara��es de m�todos sobrecarregados.
 * 
 * @author Matrakas
 * 
 */
public class SobrecargaDeMetodos
{
	/**
	 * Teste de m�todos square sobrecarregados
	 */
	public void testeMetodosSobrecarregados()
	{
		System.out.printf("Quadrado do inteiro 7 � %d\n", quadrado(7));
		System.out.printf("Quadrado do valor em ponto flutuante 7.5 � %f\n", quadrado(7.5));
	}

	/**
	 * M�todo quadrado com argumento inteiro
	 * 
	 * @param valor
	 *            Inteiro para o qual ser� calculado o quadrado.
	 * @return Inteiro representando o quadrado do par�metro fornecido.
	 */
	@SuppressWarnings("static-method")
	public int quadrado(int valor)
	{
		System.out.printf("\nChamada de quadrado com o argumento inteiro: %d\n", valor);
		return valor * valor;
	}

	/**
	 * M�todo quadrado com argumento em ponto flutuante
	 * 
	 * @param valor
	 *            Ponto flutuante para o qual ser� calculado o quadrado.
	 * @return Inteiro representando o quadrado do par�metro fornecido.
	 */
	@SuppressWarnings("static-method")
	public double quadrado(double valor)
	{
		System.out.printf("\nChamada de quadrado com argumento em ponto flutuante: %f\n", valor);
		return valor * valor;
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
