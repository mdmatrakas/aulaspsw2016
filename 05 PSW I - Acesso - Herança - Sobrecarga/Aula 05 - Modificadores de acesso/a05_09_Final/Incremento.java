package a05_09_Final;

/**
 * Vari�vel de inst�ncia final em uma classe.
 * 
 * @author Matrakas
 */
public class Incremento
{
	private int			total	= 0;	// total de todos os incrementos
	private final int	INCREMENT;		// vari�vel constante (n�o-inicializada)

	/**
	 * Construtor inicializa vari�vel de inst�ncia final INCREMENT
	 * 
	 * @param ValorIncremento
	 *            Quantidade a ser usada de incremento
	 */
	public Incremento(int ValorIncremento)
	{
		INCREMENT = ValorIncremento; // inicializa vari�vel constante (uma vez)
	}

	/**
	 * Adiciona INCREMENT ao total
	 */
	public void SomaIncrementoEmTotal()
	{
		total += INCREMENT;
	}

	/**
	 * Retorna representa��o de String dos dados de um objeto Increment
	 */
	public String toString()
	{
		return String.format("total = %d", total);
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/