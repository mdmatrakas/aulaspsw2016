package a05_06_Enumeracao;

import java.util.EnumSet;

/**
 * Testando o tipo enum Livro.
 * 
 * @author Matrakas
 */
public class TesteEnum
{
	public static void main(String args[])
	{
		System.out.println("Todos os livros:\n");

		// imprime todos os livros na enumeração Livro
		for (Livro livro : Livro.values())
			System.out.printf("%-10s%-45s%s\n", livro, livro.getTitulo(), livro.getAnoDireitos());

		System.out.println("\nMostrar um conjunto de constantes enum:\n");

		// imprime os primeiros quatro livros da enumeração Livro
		for (Livro livro : EnumSet.range(Livro.JHTP6, Livro.CPPHTP4))
			System.out.printf("%-10s%-45s%s\n", livro, livro.getTitulo(), livro.getAnoDireitos());
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
