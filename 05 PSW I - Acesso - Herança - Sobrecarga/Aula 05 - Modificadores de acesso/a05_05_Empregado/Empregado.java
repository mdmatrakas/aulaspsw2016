package a05_05_Empregado;

/**
 * Classe Empregado com refer�ncia a outros objetos.
 * 
 * @author Matrakas
 */
public class Empregado
{
	private String	primeiroNome;
	private String	ultimoNome;
	private Data	dataNascimento;
	private Data	dataContratacao;

	/**
	 * Construtor para inicializar nome, data de nascimento e data de contrata��o
	 * 
	 * @param primeiro
	 *            String com o primeiro nome do empregado
	 * @param ultimo
	 *            String com o �ltimo nome do empregado
	 * @param dtNasc
	 *            Objeto Data com a data de nascimento do empregado
	 * @param dtContr
	 *            Objeto Data com a data de contrata��o do empregado
	 */
	public Empregado(String primeiro, String ultimo, Data dtNasc, Data dtContr)
	{
		primeiroNome = primeiro;
		ultimoNome = ultimo;
		dataNascimento = dtNasc;
		dataContratacao = dtContr;
		
		// teste 1
		// dataNascimento = new Data(dtNasc);
		// dataContratacao = new Data(dtContr);
				
	}

	public Data getDataContratacao()
	{
		return dataContratacao;
		// teste 2
		// return new Data(dataContratacao);
	}

	public void setDataContratacao(Data dtContratacao)
	{
		this.dataContratacao = dtContratacao;
		// teste 2
		// this.dataContratacao = new Data(dtContratacao);
	}

	/**
	 * Converte Empregado em formato de String
	 */
	public String toString()
	{
		return String.format("%s, %s  Contratado em: %s  Nascido em: %s", 
				ultimoNome, primeiroNome,	dataContratacao, dataNascimento);
	}
}

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/
