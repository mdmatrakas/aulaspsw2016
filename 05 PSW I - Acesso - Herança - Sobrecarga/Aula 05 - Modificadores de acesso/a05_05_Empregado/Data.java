package a05_05_Empregado;

/**
 * Declara��o da classe Data.
 * 
 * @author Matrakas
 * 
 */
public class Data
{
	private int	mes;	// 1-12
	private int	dia;	// 1-31 conforme o m�s
	private int	ano;	// qualquer ano

	/**
	 * Construtor: chama validaMes para confirmar o valor adequado para mes; 
	 * chama validaDia para confirmar o valor adequado para dia
	 * 
	 * @param oMes
	 *            Novo valor para o mes de 1 a 12
	 * @param oDia
	 *            Novo valor para o dia de 1 a 31, conforme o m�s
	 * @param oAno
	 *            Novo valor para o ano, sem valida��o
	 */
	public Data(int oMes, int oDia, int oAno)
	{
		mes = validaMes(oMes); // valida mes
		ano = oAno; // poderia validar ano
		dia = validaDia(oDia); // valida dia

		System.out.printf("Construtor de objetos Data para a data: %s\n", this);
	}
	
	public Data(Data d)
	{
		mes = d.mes; // valida mes
		ano = d.ano; // poderia validar ano
		dia = d.dia; // valida dia

		System.out.printf("Construtor de objetos Data para a data: %s\n", this);
	}

	/**
	 * m�todo utilit�rio para confirmar o valor adequado de mes
	 * 
	 * @param testeMes
	 *            O valor a testar, de 1 a 12
	 * @return O valor validado
	 */
	@SuppressWarnings("static-method")
	private int validaMes(int testeMes)
	{
		if (testeMes > 0 && testeMes <= 12) // valida mes
			return testeMes;
		else // mes � inv�lido	
		{
			System.out.printf("M�s inv�lido: (%d); configurado para 1.", testeMes);
			return 1; // mant�m objeto em estado consistente
		}
	}

	/**
	 * M�todo utilit�rio para confirmar o valor adequado de dia com base em mes 
	 * e ano
	 * 
	 * @param testeDia
	 *            O valor a testar, de 1 a 31, conforme o m�s
	 * @return O valor validado
	 */
	private int validaDia(int testeDia)
	{
		int diasPorMes[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		// verifica se dia est� no intervalo para mes
		if (testeDia > 0 && testeDia <= diasPorMes[mes])
			return testeDia;

		// verifica ano bissexto
		if (mes == 2 && testeDia == 29 && (ano % 400 == 0 || 
				(ano % 4 == 0 && ano % 100 != 0)))
			return testeDia;

		System.out.printf("Dia inv�lido: (%d), configurado para 1.", testeDia);
		return 1; // mant�m objeto em estado consistente
	}

	/**
	 * Retorna uma String no formato m�s/dia/ano
	 */
	public String toString()
	{
		return String.format("%02d/%02d/%04d", mes, dia, ano);
	}

	public int getMes()
	{
		return mes;
	}

	public void setMes(int mes)
	{
		this.mes = validaMes(mes); // valida mes
	}

	public int getDia()
	{
		return dia;
	}

	public void setDia(int dia)
	{
		this.dia = validaDia(dia); // valida dia
	}

	public int getAno()
	{
		return ano;
	}

	public void setAno(int ano)
	{
		this.ano = ano;
	}
}

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/
