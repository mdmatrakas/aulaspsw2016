package anglo.exercicio15;

import java.util.Random;

public class Bola implements Runnable {
	private int x;
	private int y;
	private int dx;
	private int dy;
	private final int MAX_X = 300;
	private final int MAX_Y = 300;
	private static final Random generator = new Random();

	public Bola(int xPos, int yPos) {
		dx = generator.nextInt(5) + 1;
		dy = generator.nextInt(5) + 1;
		x = xPos;
		y = yPos;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(6);
			} catch (InterruptedException exception) {
				exception.printStackTrace();
			}

			x += dx;
			y += dy;

			if (y <= 0 || y >= MAX_Y - 10)
				dy = -dy;

			if (x <= 0 || x >= MAX_X - 10)
				dx = -dx;
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}