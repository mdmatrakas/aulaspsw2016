package anglo.exercicio15;

import javax.swing.JFrame;

public class BolaMexe extends JFrame
{
    private final PanelBola ballCanvas;

    public BolaMexe()
    {
        ballCanvas = new PanelBola( this );
        add( ballCanvas );

        pack();
        setVisible( true );
    }

    public static void main( String args[] )
    {
        BolaMexe application = new BolaMexe();
        application.setDefaultCloseOperation( EXIT_ON_CLOSE );
    }
}