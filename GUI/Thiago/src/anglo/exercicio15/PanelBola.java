package anglo.exercicio15;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PanelBola extends JPanel {
	private Bola bola;
	private ExecutorService threadExecutor;
	private JFrame parentWindow;
	private final int MAX_X = 300;
	private final int MAX_Y = 300;

	public PanelBola(JFrame window) {
		parentWindow = window;

		threadExecutor = Executors.newCachedThreadPool();

		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent event) {
				criaBola(event);
				RepaintTimer timer = new RepaintTimer(parentWindow);
				threadExecutor.execute(timer);
			}
		});
	}

	private void criaBola(MouseEvent event) {
		if (bola == null) {
			int x = event.getX();
			int y = event.getY();
			bola = new Bola(x, y);
			threadExecutor.execute(bola);
		}
	}

	public Dimension getMinimumSize() {
		return getPreferredSize();
	}

	public Dimension getPreferredSize() {
		return new Dimension(MAX_X, MAX_Y);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (bola != null) {
			g.setColor(Color.blue);
			g.fillOval(bola.getX(), bola.getY(), 10, 10);
		}
	}
}
