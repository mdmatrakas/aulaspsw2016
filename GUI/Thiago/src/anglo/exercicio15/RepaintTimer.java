package anglo.exercicio15;

import javax.swing.JFrame;

public class RepaintTimer implements Runnable {
	private final JFrame repaintComponent;

	public RepaintTimer(JFrame frame) {
		repaintComponent = frame;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(2);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}

			repaintComponent.repaint();
		}
	}
}