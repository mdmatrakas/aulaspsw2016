package src.home.exercicios;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PainelDesenho extends JPanel {
	
	public static void main(String[] args) {
		PainelDesenho painel = new PainelDesenho();
		JFrame app = new JFrame("Desenhando...");
		
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.add(painel);
		app.setSize(250,  250);
		app.setVisible(true);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int largura = getWidth();
		int altura = getHeight();
		
		g.drawLine(0, 0, largura, altura);
		g.drawLine(0, largura, altura, 0);
	}

}
