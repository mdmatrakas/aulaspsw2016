package src.home.exercicios;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class DesenhoAlgo extends JPanel {

	public static void main(String[] args) {
		
		DesenhoAlgo painel = new DesenhoAlgo();
		
		JFrame app = new JFrame();
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.add(painel);
		app.setSize(300, 300);
		app.setVisible(true);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawRect(4, 3, 20, 30);
		g.drawLine(90, 33, 92, 10);
		g.drawOval(49, 20, 19, 10);
	}

}
