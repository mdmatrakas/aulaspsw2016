package src.home.exercicios;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class BotaoFrame extends JFrame {

	private JButton botaoPalhaco;

	public BotaoFrame() {
		super("háaa palhaço!");
		setLayout(new FlowLayout());

		botaoPalhaco = new JButton("NARIZ");
		botaoPalhaco.setToolTipText("veeem dar uma bitoca no meu nariz!");
		add(botaoPalhaco);
		
		ButtonHandler buttonHandler = new ButtonHandler();
		botaoPalhaco.addActionListener(buttonHandler);
	}

	private class ButtonHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(BotaoFrame.this,
					String.format("HÁÁÁÁÁÁ PALHAÇO!!!!!!!", e.getActionCommand()));

		}

	}

}
