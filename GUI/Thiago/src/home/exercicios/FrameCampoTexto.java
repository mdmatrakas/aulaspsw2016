package src.home.exercicios;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class FrameCampoTexto extends JFrame {

	private JTextField campoUsuario;
	private JPasswordField campoSenha;

	public FrameCampoTexto() {
		super("Telinha de Loginis");
		setLayout(new FlowLayout());

		campoUsuario = new JTextField(40);
		add(campoUsuario);

		campoSenha = new JPasswordField(40);
		add(campoSenha);

		OuvinteCampoTexto ouvinte = new OuvinteCampoTexto();
		campoUsuario.addActionListener(ouvinte);
		campoSenha.addActionListener(ouvinte);
	}

	public JTextField getCampoUsuario() {
		return campoUsuario;
	}

	public void setCampoUsuario(JTextField campoUsuario) {
		this.campoUsuario = campoUsuario;
	}

	public JPasswordField getCampoSenha() {
		return campoSenha;
	}

	public void setCampoSenha(JPasswordField campoSenha) {
		this.campoSenha = campoSenha;
	}

	public class OuvinteCampoTexto implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String string = "";

			if (e.getSource() == getCampoUsuario()) {
				string = String.format("campoUsuario: %s", e.getActionCommand());
			} else if (e.getSource() == getCampoSenha()) {
				string = String.format("campoSenha: %s", e.getActionCommand());
			}

			JOptionPane.showMessageDialog(null, string);

		}

	}

}
