package src.home.exercicios;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class FrameTelaInteira extends JFrame {
	
	private JLabel label;

	public static void main(String[] args) {

		FrameTelaInteira f = new FrameTelaInteira();

		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500, 200);
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		f.setVisible(true);
	}

	public FrameTelaInteira() {
		super("Framizinho c/ texto :)");
		
		label = new JLabel("Aqui vai um testículo!");
		label.setToolTipText("O que é isto?");
		add(label);
	}

}
