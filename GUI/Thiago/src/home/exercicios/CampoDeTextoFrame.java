package src.home.exercicios;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class CampoDeTextoFrame extends JFrame {
	
	private JTextField campo1;
	private JTextField campo2;
	private JTextField campo3;
	private JPasswordField campoSenha;
	
	public CampoDeTextoFrame() {
		super("Um lugar para campo de texto e senha!");
		
		setLayout(new FlowLayout());
		campo1 = new JTextField(10);
		add(campo1);
		
		campo2 = new JTextField("Digite texto aqui");
		add(campo2);
		
		campo3 = new JTextField("Campo não editável", 30);
		campo3.setEditable(false);
		add(campo3);
		
		campoSenha = new JPasswordField(10);
		add(campoSenha);
		
	}
	
	public static void main(String[] args) {
		CampoDeTextoFrame f = new CampoDeTextoFrame();
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500, 200);
		f.setVisible(true);
	}

}
