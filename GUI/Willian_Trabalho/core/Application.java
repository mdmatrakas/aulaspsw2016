/*
 * Descrição da classe: Classe para iniciar a aplicação do pomodoro
 * 
 * Autor: Willian Munari Raupp
 * Data: 14/04/2016
 * 
 * Modificações: (O que? Quem? Quando?)
 * 14/04/2016 - Willian Munari Raupp
 *                      Criação da classe Application com padrão Singleton.
 *
 */
package core;

import controllers.PomodoroController;
import views.PomodoroWindow;

public class Application {
	
	private static Application instance;
	private static PomodoroController pctrl;
	private static PomodoroWindow pomo;



	// Construtor. 
	private Application() {
		pctrl = new PomodoroController();
		pomo = new PomodoroWindow();
		pomo.showPomodoro();
	}
	
	public static void main(String[] args) {
		Application.getInstance();
	}

	// Método público estático de acesso único ao objeto
	public static Application getInstance()
	{
		if(instance == null) {
			inicializaInstancia();
		}
		return instance;
	}

	/*
	 * Método para evitar que sejam criados mais de uma instancia.
	 */
	private static synchronized void inicializaInstancia() 
	{
		if (instance == null) 
		{
			instance = new Application();
		}
	}

	public static PomodoroController getPctrl() {
		return pctrl;
	}

	public static void setPctrl(PomodoroController pctrl) {
		Application.pctrl = pctrl;
	}

	public static PomodoroWindow getPomo() {
		return pomo;
	}

	public static void setPomo(PomodoroWindow pomo) {
		Application.pomo = pomo;
	}
	
	
	
	
}
