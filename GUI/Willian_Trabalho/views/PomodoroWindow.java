/*
 * Descrição da classe: Classe de frame que tem como objetivo em exibir a janela para trabalhar com o pomodoro.
 * 
 * Autor: Willian Munari Raupp
 * Data: 14/04/2016
 * 
 * Modificações: (O que? Quem? Quando?)
 * 14/04/2016 - Willian Munari Raupp
 *                      Criação da Classe
 * 14/04/2016 - Willian Munari Raupp
 *                      Criação e implementação dos Componentes
 */
package views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JLabel;

import core.Application;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PomodoroWindow {

	private JFrame frmSimplePomodoro;
	public static JLabel lblStats = new JLabel("Stoped");


	/**
	 * Launch the application.
	 */
	public void showPomodoro() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PomodoroWindow window = new PomodoroWindow();
					window.frmSimplePomodoro.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PomodoroWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSimplePomodoro = new JFrame();
		frmSimplePomodoro.setTitle("Simple Pomodoro");
		frmSimplePomodoro.setBounds(100, 100, 450, 100);
		frmSimplePomodoro.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSimplePomodoro.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		
		frmSimplePomodoro.getContentPane().add(lblStats);
		
		JButton btnPomodoro = new JButton("Pomodoro");
		btnPomodoro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Application.getPctrl().pomodoro();
				lblStats.setText("Started! Pomodoro");
			}
		});
		frmSimplePomodoro.getContentPane().add(btnPomodoro);
		
		JButton btnLong = new JButton("Long Break");
		btnLong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Application.getPctrl().longBreak();
				lblStats.setText("Started! Long Break");
			}
		});
		frmSimplePomodoro.getContentPane().add(btnLong);
		
		JButton btnShort = new JButton("Short Break");
		btnShort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Application.getPctrl().shortBreak();
				lblStats.setText("Started! Short Break");
			}
		});
		frmSimplePomodoro.getContentPane().add(btnShort);
		
		JButton btnResult = new JButton("Generate Results");
		frmSimplePomodoro.getContentPane().add(btnResult);
	}

}
