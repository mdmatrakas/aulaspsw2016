/*
 * Descrição da classe: Classe que tem como objetivo fazer o controle de uso do timer do pomodoro.
 * 
 * Autor: Willian Munari Raupp
 * Data: 14/04/2016
 * 
 * Modificações: (O que? Quem? Quando?)
 * 14/04/2016 - Willian Munari Raupp
 *                      Criação da Classe
 * 14/04/2016 - Willian Munari Raupp
 *                      Criação e Implementação dos métodos necessários Construtor,pomodoro,shortbreak,longbreak, e classe para acordar a thread.
 */
package controllers;

import java.util.Timer;
import java.util.TimerTask;

import views.PomodoroWindow;

public class PomodoroController 
{
	private Timer timer;
	private boolean isExecuting = false;
	private boolean itWasPomodoroExecuting = false;
	
	/**
	 * Método Responsavel por fazer o timer aguardar 25 minutos e então gerar um aviso para a view.
	 */
	public void pomodoro()
	{
		if (isExecuting) {
			timer.cancel();
		}
		timer = new Timer();
        timer.schedule(new RemindTask(), 1500*1000);
        isExecuting = true;
        itWasPomodoroExecuting = true;
	}
	
	/**
	 * Método responsavel por fazer o timer aguardar 5 minutos e então gerar um aviso para a view.
	 */
	public void shortBreak()
	{
		if (isExecuting) {
			timer.cancel();
		}
		timer = new Timer();
        timer.schedule(new RemindTask(), 300*1000);
        isExecuting = true;
	}
	
	/**
	 * Método responsavel por fazer o timer aguardar 15 minutos e então gerar um aviso para a view.
	 */
	public void longBreak()
	{
		if (isExecuting) {
			timer.cancel();
		}
		timer = new Timer();
        timer.schedule(new RemindTask(), 900*1000);
        isExecuting = true;
	}
	
	/**
	 * classe thread que é executada assim que a task chega no seu tempo.
	 */
	class RemindTask extends TimerTask {
        public void run() {
        	System.out.println("Done!");// for debug
			PomodoroWindow.lblStats.setText("Done!");
            timer.cancel();
            if(itWasPomodoroExecuting) {
            	PomodoroWindow.lblStats.setText("Short Break Executing!");
            	pomodoro();
            	itWasPomodoroExecuting = false;
            }
        }
    }
}
