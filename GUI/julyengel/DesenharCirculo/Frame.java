package DesenharCirculo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;



public class Frame extends JFrame{
	private JLabel barraInfo;
	private Painel painel;
	private int x1, y1, x2, y2;
	public static void main(String args[])
	{
		Frame f = new Frame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(500, 500); 
		f.setVisible(true); 
	}
	public Frame() {
		super("Desenhando com o mouse");
		painel = new Painel();
		painel.setBackground(Color.white);
		add(painel, BorderLayout.CENTER);
		
		OuvinteMouse ouvinte = new OuvinteMouse();
		painel.addMouseListener(ouvinte);
		painel.addMouseMotionListener(ouvinte);
		
	}

	private class OuvinteMouse implements MouseListener, MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent e) {
			x2 = e.getX();
			y2 = e.getY();
			Circulo circulo = new Circulo(x1, y1, x2, y2, Color.black);
			System.out.println(x1);
			painel.setCirculo(circulo);
			painel.repaint();

		}

		@Override
		public void mouseMoved(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mousePressed(MouseEvent e) {
			x1 = e.getX();
			y1 = e.getY();
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			x2 = e.getX();
			y2 = e.getY();
			Circulo circulo = new Circulo(x1, y1, x2, y2, Color.black);
			System.out.println(x1);
			painel.setCirculo(circulo);
			painel.repaint();

		}

	}
}
