package DesenharCirculo;

import java.awt.Graphics;

import javax.swing.JPanel;



public class Painel extends JPanel {
	private Circulo circulo;
	private int x1, y1, x2, y2;

	public void setCirculo(Circulo circulo) {
		this.circulo = circulo;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		circulo.draw(g);
	}
}
