package aula2;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Frame extends JFrame {

	private JTextField campoTexto1;
	private JTextField campoTexto2;
	private JTextField campoTexto3;
	private JButton bsoma,bsub,bdiv,bmult;
	
		
	
	int num1 = 0;
	int num2 = 0;
	String result = "";
	int resultado;

	private JLabel label;

	public static void main(String[] args) {
		Frame f = new Frame();
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		f.setSize(500, 200);

		f.setVisible(true);
	}

	public Frame() {

		super("Testando JTextField e JPasswordField");
		setLayout(new FlowLayout()); // configura o layout de frame
		// constr�i campoTexto com 10 colunas
		campoTexto1 = new JTextField(10);
		add(campoTexto1); // adiciona campoTexto1 ao JFrame
		// constr�i campo de texto com texto padr�o
		campoTexto2 = new JTextField(10);
		add(campoTexto2); // adiciona campoTexto2 ao JFrame
		// constr�i campoTexto com o texto padr�o e 21 colunas
		campoTexto3 = new JTextField("Resultado: ", 10);
		campoTexto3.setText(result);
		add(campoTexto3); // adiciona campoTexto3 ao JFrame
		OuvinteCampoTexto ouvinte = new OuvinteCampoTexto();
		bsoma = new JButton("+");
		bsub = new JButton("-");
		bdiv = new JButton("/");
		bmult = new JButton("x");
		
		add(bsoma);
		add(bsub);
		add(bdiv);
		add(bmult);
		
		bsoma.addActionListener(ouvinte);
		bdiv.addActionListener(ouvinte);
		bsub.addActionListener(ouvinte);
		bmult.addActionListener(ouvinte);
		
		campoTexto1.addActionListener(ouvinte);
		campoTexto2.addActionListener(ouvinte);
		campoTexto3.addActionListener(ouvinte);

	}

	private class OuvinteCampoTexto implements ActionListener {
		// processa eventos de campo de texto
		private int operador = 0 ;
		public void actionPerformed(ActionEvent event) {
			String string = ""; // declara string a ser exibida

			if (event.getSource() == campoTexto1) {
				string = String.format("campoTexto1: %s\ntexto %s", event.getActionCommand(), campoTexto1.getText());
				num1 = Integer.parseInt(campoTexto1.getText());
				System.out.println(num1);
			} else if (event.getSource() == campoTexto2) {
				string = String.format("campoTexto2: %s", event.getActionCommand());
				num2 = Integer.parseInt(campoTexto2.getText());	
				System.out.println(num1);
				
				if(operador == 1)
				resultado = num1 + num2;
				else if(operador == 2)
					resultado = num1 - num2;
				else if(operador == 3)
					resultado = num1/num2;
				else if(operador == 4)
					resultado = num1*num2;				
				result = Integer.toString(resultado);
				campoTexto3.setText(result);
			}else if(event.getSource() == bsoma){
				operador = 1;
			}else if(event.getSource() == bsub){
				operador = 2;
			}else if(event.getSource() == bdiv){
				operador = 3;
			}else if(event.getSource() == bmult){
				operador = 4;
			}
			
			
			
		}
	}

}
