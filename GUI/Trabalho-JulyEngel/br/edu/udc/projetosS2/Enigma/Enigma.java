package br.edu.udc.projetosS2.Enigma;

public class Enigma {

	char[] _rotor1 = { 'E', 'K', 'M', 'F', 'L', 'G', 'D', 'Q', 'V', 'Z', 'N', 'T', 'O', 'W', 'Y', 'H', 'X', 'U', 'S',
			'P', 'A', 'I', 'B', 'R', 'C', 'J' };

	char[] _rotor2 = { 'A', 'J', 'D', 'K', 'S', 'I', 'R', 'U', 'X', 'B', 'L', 'H', 'W', 'T', 'M', 'C', 'Q', 'G', 'Z',
			'N', 'P', 'Y', 'F', 'V', 'O', 'E' };

	char[] _rotor3 = { 'B', 'D', 'F', 'H', 'J', 'L', 'C', 'P', 'R', 'T', 'X', 'V', 'Z', 'N', 'Y', 'E', 'I', 'W', 'G',
			'A', 'K', 'M', 'U', 'S', 'Q', 'O' };

	char[] _plugboard = { 'Z', 'P', 'H', 'N', 'M', 'S', 'W', 'C', 'I', 'Y', 'T', 'Q', 'E', 'D', 'O', 'B', 'L', 'R', 'F',
			'K', 'U', 'V', 'G', 'X', 'J', 'A' };

	char[] _reflector = { 'Y', 'R', 'U', 'H', 'Q', 'S', 'L', 'D', 'P', 'X', 'N', 'G', 'O', 'K', 'M', 'I', 'E', 'B', 'F',
			'Z', 'C', 'W', 'V', 'J', 'A', 'T' };
	char text[];

	int r1, r2, r3;

	Rotor rotor1 = new Rotor(_rotor1);
	Rotor rotor2 = new Rotor(_rotor2);
	Rotor rotor3 = new Rotor(_rotor3);
	Rotor plugboard = new Rotor(_plugboard);
	Rotor reflector = new Rotor(_reflector);

	public void setRotors(int a, int b, int c) {
		this.r1 = a;
		this.r2 = b;
		this.r3 = c;

	}

	public void setMens(String mensagem) {
		String mens = mensagem.toUpperCase();   
		this.text = mens.toCharArray();
	}

	public String encript() {

		for (int i = 0; i < text.length; i++) {

			text[i] = plugboard.changeLetter(text[i]);

			for (int j = 0; j < r1; j++) {
				rotor1.spin();
			}
			text[i] = rotor1.changeLetter(text[i]);

			for (int j = 0; j < r2; j++) {
				rotor2.spin();
			}
			text[i] = rotor2.changeLetter(text[i]);

			for (int j = 0; j < r3; j++) {
				rotor3.spin();
			}
			text[i] = rotor3.changeLetter(text[i]);

			text[i] = reflector.changeLetter(text[i]);

			text[i] = rotor3.changeLetterBack(text[i]);

			text[i] = rotor2.changeLetterBack(text[i]);

			text[i] = rotor1.changeLetterBack(text[i]);

			text[i] = plugboard.changeLetterBack(text[i]);

		}
		String answer = new String(text);

		return answer;
	}

}
