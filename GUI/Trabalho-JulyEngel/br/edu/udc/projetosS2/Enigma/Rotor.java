package br.edu.udc.projetosS2.Enigma;

import java.util.Arrays;

public class Rotor {
	private char[] rotorpart1 = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
			'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
	private char[] rotorPart2 = new char[26];
	char a;
	char b;

	public String toString() {
		return "Rotor [rotorPart2=" + Arrays.toString(rotorPart2) + "]";
	}

	public Rotor(char[] rotorPart2) {
		super();
		this.rotorPart2 = rotorPart2;
	}

	public void spin() {

		char a = rotorPart2[25];
	
		for (int i = 25; i >= 0; i--) {
			if (i == 0) {
				rotorPart2[i] = a;
			
			} else {
				rotorPart2[i] = rotorPart2[i - 1];
			
			}

		}
	}

	public char changeLetter(char letter) {

		for (int i = 0; i < 26; i++) {
			if ((rotorpart1[i] == letter) == true) {

				letter = rotorPart2[i];
				break;
			}
		}

		return letter;

	}

	public char changeLetterBack(char letter) {

		for (int i = 0; i < 26; i++) {
			if ((rotorPart2[i] == letter) == true) {
				letter = rotorpart1[i];
				break;
			}
		}

		return letter;
	}
}
