package br.edu.udc.projetosS2.Janelas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MenuPrincipal extends JFrame {
	JButton login;
	JButton cadastro;

	public MenuPrincipal() {
		super("Menu Principal");
		setLayout(new FlowLayout());
		login = new JButton("Login");
		add(login);
		cadastro = new JButton("Cadastro");
		add(cadastro);
		ButtonHandler ouvinte = new ButtonHandler();
		cadastro.addActionListener(ouvinte);
		login.addActionListener(ouvinte);
	}

	public class ButtonHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == login) {
				Login login = new  Login();
				login.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				login.setSize(500, 500); 
				login.setVisible(true);	
				System.out.println("Teste");
			}
			if (event.getSource() == cadastro) {
				Cadastro cadastro = new  Cadastro();
				cadastro.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				cadastro.setSize(500, 500); 
				cadastro.setVisible(true);	
				System.out.println("Teste");
				
			}

		}

	}
}
