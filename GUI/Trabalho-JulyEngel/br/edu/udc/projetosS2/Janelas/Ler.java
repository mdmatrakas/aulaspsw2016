package br.edu.udc.projetosS2.Janelas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import br.edu.usc.projetosS2.usuario.Control;
import br.edu.usc.projetosS2.usuario.Lista;
import br.edu.usc.projetosS2.usuario.User;

public class Ler extends JFrame{
	
	JButton escrever;	
	JLabel mens;
	private JTextArea mensagem;
	JLabel nomeDoArquivo;
	JTextField nomeArq;
	Lista lista = new Lista();
	User user = new User();
	String texto = new String();
	Control control = new Control();
	
			Ler(User user){
				super("ler");
				setLayout(new FlowLayout());
				texto = " ";
				user = user;
				control.setUserLogado(user);
				nomeDoArquivo = new JLabel("Nome Do Arquivo");
				nomeArq = new JTextField(10);
				add(nomeDoArquivo);
				add(nomeArq);									
				escrever = new JButton("Carregar");		
				add(escrever);
				ButtonHandler ouvinte = new ButtonHandler();
				escrever.addActionListener(ouvinte);
			}
			
			public class ButtonHandler implements ActionListener{
				@Override
				public void actionPerformed(ActionEvent event) {
					if(event.getSource() == escrever){
						try {
							
							texto = control.desencript(nomeArq.getText());
							Texto janelatexto = new Texto(texto);					
							janelatexto.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
							janelatexto.setSize(500, 500); // configura o tamanho do frame
							janelatexto.setVisible(true);
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				}
			}

			
			
}
