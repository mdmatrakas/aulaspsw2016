package br.edu.udc.projetosS2.Janelas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


import br.edu.usc.projetosS2.usuario.User;

public class MenuEnigma extends JFrame{
	JButton escrever;
	JButton ler;
	User user = new User();
	public MenuEnigma(User user){
		super("Menu Enigma");
		this.user = user;
		setLayout(new FlowLayout());
		escrever = new JButton("Escrever Mensagem");
		add(escrever);
		ler = new JButton("Ler Mensagem");
		add(ler);
		ButtonHandler ouvinte = new ButtonHandler();
		escrever.addActionListener(ouvinte);
		ler.addActionListener(ouvinte);
	}

	public class ButtonHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == ler) {
				Ler ler = new  Ler(user);
				ler.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				ler.setSize(500, 500); 
				ler.setVisible(true);	
				
			}
			if (event.getSource() == escrever) {
				Escrever escrever = new  Escrever(user);
				escrever.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				escrever.setSize(500, 500); 
				escrever.setVisible(true);	
				System.out.println("Teste");
				
			}

		}

	}
}
