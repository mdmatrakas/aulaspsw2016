package br.edu.udc.projetosS2.Janelas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import br.edu.usc.projetosS2.usuario.Lista;
import br.edu.usc.projetosS2.usuario.User;

public class Cadastro extends JFrame {
	private JTextField login; // campo de texto com tamanho configurado
	private JTextField nome; // campo de texto constru�do com texto
	private JTextField endereco; // campo de texto com texto e tamanho
	private JPasswordField passwd;
	private JTextField telefone;
	private JLabel labelLogin;
	private JLabel labelNome;
	private JLabel labelEndereco;
	private JLabel labelPasswd;
	private JLabel labelTelefone;
	private JButton cadastrar;
	private JButton sair;
	Lista lista = new Lista();
	User user = new User();

	public Cadastro() {

		super("Cadastro");
		lista.loadUsers();
		setLayout(new FlowLayout());
		nome = new JTextField(10);
		labelNome = new JLabel("NOME: ");
		add(labelNome);
		add(nome);
		labelEndereco = new JLabel("ENDERECO: ");
		add(labelEndereco);
		endereco = new JTextField(10);
		add(endereco);

		labelTelefone = new JLabel("TELEFONE: ");
		add(labelTelefone);
		telefone = new JTextField(10);
		add(telefone);

		labelLogin = new JLabel("Login: ");
		add(labelLogin);
		login = new JTextField(10);
		add(login);

		labelPasswd = new JLabel("SENHA: ");
		add(labelPasswd);
		passwd = new JPasswordField(10);
		add(passwd);

		cadastrar = new JButton("Registrar");
		sair = new JButton("Sair");
		add(cadastrar);
		add(sair);
		ButtonHandler ouvinte = new ButtonHandler();
		cadastrar.addActionListener(ouvinte);
		sair.addActionListener(ouvinte);
		login.addActionListener(ouvinte);

	}

	public void adicionar() {
		lista.adiciona(user);
		lista.saveUsers();
	}

	private void fechar() {
		this.dispose();
	}

	private class ButtonHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {

			if (event.getSource() == cadastrar) {

				if (!lista.contem(login.getText())) {
					user.setEndereco(endereco.getText());
					user.setLogin(login.getText());
					user.setNome(nome.getText());
					user.setPasswd(passwd.getText());
					Random rand = new Random();
					int randomNum = rand.nextInt((13 - 0) + 1) + 0;
					user.setR1(randomNum);
					randomNum = rand.nextInt((13 - 0) + 1) + 0;
					user.setR2(randomNum);
					randomNum = rand.nextInt((13 - 0) + 1) + 0;
					user.setR3(randomNum);
					JOptionPane.showMessageDialog(null, "Usu�rio Cadastrado!");
					adicionar();
					fechar();

				}else{
					JOptionPane.showMessageDialog( null,
				    		  "Esse Login J� Existe!" );
				}
			}
			if (event.getSource() == sair) {
				fechar();
			}

		}

	}

}