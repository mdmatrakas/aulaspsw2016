package br.edu.udc.projetosS2.Janelas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import br.edu.usc.projetosS2.usuario.Lista;
import br.edu.usc.projetosS2.usuario.User;

public class Login extends JFrame {
	JTextField login;
	JLabel loginLabel;
	JLabel senhaLabel;
	JPasswordField senha;
	JButton blogin;
	Lista lista = new Lista();
	public Login() {
		super("Login");
		lista.loadUsers();
		setLayout(new FlowLayout());
		loginLabel = new JLabel("Login");
		login = new JTextField(10);
		add(loginLabel);
		add(login);
		senhaLabel = new JLabel("Senha");
		senha =new JPasswordField(10);
		add(senhaLabel);
		add(senha);		
		blogin = new JButton("Login");
		add(blogin);		
		ButtonHandler ouvinte = new ButtonHandler();
		blogin.addActionListener(ouvinte);
	}
	public class ButtonHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent event) {
			if(event.getSource() == blogin){
				if(lista.contem(login.getText())){					
					if(lista.verificarSenha(login.getText(), senha.getText())){
						User user = new User();
						user = lista.returnUser(login.getText());
						MenuEnigma menu = new MenuEnigma(user);					
						menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						menu.setSize(500, 500); // configura o tamanho do frame
						menu.setVisible(true);
					}else{
						JOptionPane.showMessageDialog( null,
					    		  "Senha Incorreta!" );
					}
				}else{
					JOptionPane.showMessageDialog( null,
				    		  "Esse Login N�o Existe!" );
				}
			}
			
		}
		
	}
}
