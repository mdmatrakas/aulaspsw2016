package br.edu.udc.projetosS2.Janelas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import br.edu.usc.projetosS2.usuario.Control;
import br.edu.usc.projetosS2.usuario.Lista;
import br.edu.usc.projetosS2.usuario.User;

public class Escrever extends JFrame {
	JTextField dest;
	JButton escrever;
	JLabel destLabel;
	JLabel mens;
	private JTextArea mensagem;
	JLabel nomeDoArquivo;
	JTextField nomeArq;
	Lista lista = new Lista();
	User user = new User();
	Control control = new Control();

	public Escrever(User user) {
		super("Escrever");
		user = user;
		control.setUserLogado(user);
		lista.loadUsers();
		setLayout(new FlowLayout());
		dest = new JTextField(10);
		destLabel = new JLabel("Destinatario");
		add(destLabel);
		add(dest);
		nomeDoArquivo = new JLabel("Nome Do Arquivo");
		nomeArq = new JTextField(10);
		add(nomeDoArquivo);
		add(nomeArq);
		escrever = new JButton("Enviar");
		mens = new JLabel(" Mensagem: ");
		add(mens);
		mensagem = new JTextArea(10, 20);
		add(mensagem);
		escrever = new JButton("Enviar");
		add(escrever);
		ButtonHandler ouvinte = new ButtonHandler();
		escrever.addActionListener(ouvinte);
	}

	public class ButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (event.getSource() == escrever) {
				
				if (lista.contem(dest.getText())) {
					
					User userDest = new User();
					userDest = lista.returnUser(dest.getText());				
					System.out.println(lista.returnUser(dest.getText()).getLogin());
					try {
						
						control.encript(userDest, mensagem.getText(), nomeArq.getText());
						JOptionPane.showMessageDialog( null,
					    		  "Mensagem Enviada!" );
						
					} catch (IOException e) {
						e.printStackTrace();
						
					}

				}
			}

		}
	}

}
