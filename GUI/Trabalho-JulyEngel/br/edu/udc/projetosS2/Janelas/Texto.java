package br.edu.udc.projetosS2.Janelas;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JTextArea;

public class Texto extends JFrame {
	private JTextArea mensagem;

	public Texto(String texto) {
		super("Texto");
		setLayout(new FlowLayout());
		mensagem = new JTextArea(texto,10, 20);
		add(mensagem);
	}
}
