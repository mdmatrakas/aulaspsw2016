package br.edu.projetosS2.Principal;

import javax.swing.JFrame;

import br.edu.udc.projetosS2.Janelas.MenuPrincipal;
import br.edu.usc.projetosS2.usuario.Control;

public class Main {
	public static void main(String args[]){
		Control controle = new Control();
		MenuPrincipal menu = new  MenuPrincipal();
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu.setSize(500, 500); // configura o tamanho do frame
		menu.setVisible(true);
	}
}
