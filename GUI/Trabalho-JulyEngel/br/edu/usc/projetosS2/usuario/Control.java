package br.edu.usc.projetosS2.usuario;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import br.edu.udc.projetosS2.Enigma.Enigma;
import br.edu.usc.projetosS2.usuario.User;

public class Control {

	
	private User userLogado = new User();
	

	public void setUserLogado(User user) {// esse metodo de login para guardar o
		this.userLogado = user;// usuario logado no momento
	}

	public void encript(User dest, String mens, String nomeArquivo) throws IOException {// classe
																						// para
		System.out.println("Here"); // encriptografar
		String result;
		Enigma enigma = new Enigma();// criacao do objeto enigma
		int r1 = 0, r2 = 0, r3 = 0;

		r1 = dest.getR1() + userLogado.getR1();// as configuracoes da maquina �
												// a chave
		r2 = dest.getR2() + userLogado.getR2();// do usuario que quer mandar a
												// mensagem
		r3 = dest.getR3() + userLogado.getR3();// mais a chave do usuario a quem
												// a mensagem se destina.
		enigma.setMens(mens);// enigma recebe a mensagem
		enigma.setRotors(r1, r2, r3);// os rotores s�o configurados
		result = enigma.encript();// a mensagem e encriptografada

		File file = new File(nomeArquivo);// criacao do arquivo
		String content;

		try (FileOutputStream fop = new FileOutputStream(file)) {

			if (!file.exists()) {
				file.createNewFile();
			}
			fop.write(userLogado.getR1());
			fop.write(userLogado.getR2());
			fop.write(userLogado.getR3());
			fop.write(result.getBytes());
			fop.flush();
			fop.close();
			System.out.println("Done!");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public String desencript(String nomeArquivo) throws IOException {

		int r1 = 0, r2 = 0, r3 = 0;
		int i = 0;
		char mensagem[] = new char[100];

		InputStream inputstream;

		try {
			inputstream = new FileInputStream(nomeArquivo);
			r1 = inputstream.read();
			r2 = inputstream.read();
			r3 = inputstream.read();

			int data;
			data = inputstream.read();
			while (data != -1) {
				mensagem[i] = (char) data;
				data = inputstream.read();
				i++;
			}
			inputstream.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		char novo[] = new char[i];
		for (int j = 0; j < i; j++) {
			novo[j] = mensagem[j];
		}

		String answer = new String(novo);
		r1 = r1 + this.userLogado.getR1();
		r2 = r2 + this.userLogado.getR2();
		r3 = r3 + this.userLogado.getR3();
		System.out.println(r1);
		System.out.println(r2);
		System.out.println(r3);

		Enigma enigma = new Enigma();
		enigma.setMens(answer);
		enigma.setRotors(r1, r2, r3);
		String result = enigma.encript();

		return result;
	}
}