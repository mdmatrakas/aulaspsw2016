package br.edu.usc.projetosS2.usuario;

public class User {
			private String login,passwd,nome,endereco,telefone;
			private int r1,r2,r3;
			@Override
			public String toString() {
				return "User [login=" + login + ", passwd=" + passwd + ", nome=" + nome + ", endereco=" + endereco
						+ ", telefone=" + telefone + ", r1=" + r1 + ", r2=" + r2 + ", r3=" + r3 + "]";
			}
			public String getLogin() {
				return login;
			}
			public void setLogin(String login) {
				this.login = login;
			}
			public String getPasswd() {
				return passwd;
			}
			public void setPasswd(String passwd) {
				this.passwd = passwd;
			}
			public String getNome() {
				return nome;
			}
			public void setNome(String nome) {
				this.nome = nome;
			}
			public String getEndereco() {
				return endereco;
			}
			public void setEndereco(String endereco) {
				this.endereco = endereco;
			}
			public String getTelefone() {
				return telefone;
			}
			public void setTelefone(String telefone) {
				this.telefone = telefone;
			}
			public int getR1() {
				return r1;
			}
			public void setR1(int r1) {
				this.r1 = r1;
			}
			public int getR2() {
				return r2;
			}
			public void setR2(int r2) {
				this.r2 = r2;
			}
			public int getR3() {
				return r3;
			}
			public void setR3(int r3) {
				this.r3 = r3;
			}
			
}
