package br.edu.usc.projetosS2.usuario;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

public class Lista {
	LinkedList<User> _users = new LinkedList();

	public void adiciona(User elemento) {
		_users.add(elemento);
	}
	
	public LinkedList<User> get_users() {
		return _users;
	}

	public void set_users(LinkedList<User> _users) {
		this._users = _users;
	}

	public User pega(User elemento) {
		int i = 0;
		while (i < _users.size()) {
			if (elemento.equals(this._users.get(i))) {
				return _users.get(i);
			}
			i++;
		}
		return null;
	}

	public void remove(User elemento) {
		int i = 0;
		while (i < this.tamanho()) {
			if (elemento.equals(this._users.get(i))) {
				_users.remove(this._users.get(i));
			}
		}
	}

	public int tamanho() {
		return this._users.size();
	}

	public boolean contem(String login) {
		int i = 0;
		while (i < this.tamanho()) {
			if (this._users.get(i).getLogin().equals(login)) {
				return true;
			}
			i++;
		}
		return false;
	}
	public User returnUser(String login) {
		int i = 0;
		while (i < this.tamanho()) {
			if (this._users.get(i).getLogin().equals(login)) {
				return this._users.get(i);
			}
			i++;
		}
		return null;
	}
	public boolean verificarSenha(String login, String senha) {
		int i = 0;
		while (i < this.tamanho()) {
			if (this._users.get(i).getLogin().equals(login)) {
				if (_users.get(i).getPasswd().equals(senha)) {
					return true;
				}
				
			}
			i++;
		}
		return false;
	}
	public void saveUsers() {// Metodo para salvar usuario em um arquivo texto
		LinkedList<User> save;
		save = this.get_users();

		File arquivo = new File("users.txt");

		try (FileWriter fw = new FileWriter(arquivo)) {
			for (int i = 0; i < save.size(); i++) {
				System.out.println(save.get(i).getNome());
				fw.write(save.get(i).getNome());
				fw.write("\r\n");
				fw.write(save.get(i).getLogin());
				fw.write("\r\n");
				fw.write(save.get(i).getPasswd());
				fw.write("\r\n");
				fw.write(save.get(i).getEndereco());
				fw.write("\r\n");
				char b = Integer.toString(save.get(i).getR1()).charAt(0);
				fw.write(b);
				fw.write("\r\n");
				char c = Integer.toString(save.get(i).getR2()).charAt(0);
				fw.write(c);
				fw.write("\r\n");
				char d = Integer.toString(save.get(i).getR3()).charAt(0);
				fw.write(d);
				fw.write("\r\n");
			}
			fw.flush();
			fw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void loadUsers() {// fazer o load dos usuarios do arquivo texto

		BufferedReader br = null;

		try {

			String currentLine;

			br = new BufferedReader(new FileReader("users.txt"));

			while ((currentLine = br.readLine()) != null) {
				User user = new User();
				user.setNome(currentLine);
				currentLine = br.readLine();
				user.setLogin(currentLine);
				currentLine = br.readLine();
				user.setPasswd(currentLine);
				currentLine = br.readLine();
				user.setEndereco(currentLine);
				currentLine = br.readLine();
				char[] k = currentLine.toCharArray();
				char c = k[0];
				int a = Character.getNumericValue(c);
				user.setR1(a);
				currentLine = br.readLine();
				k = currentLine.toCharArray();
				c = k[0];
				a = Character.getNumericValue(c);
				user.setR2(a);
				currentLine = br.readLine();
				k = currentLine.toCharArray();
				c = k[0];
				a = Character.getNumericValue(c);
				user.setR3(a);
				_users.add(user);
			//	System.out.println(user.toString());
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}
}
