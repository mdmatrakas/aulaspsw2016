package Exer01_Interface_MathOperations;

import javax.swing.JOptionPane;

public class Exer01_Dialog_Interface {

	public static void main(String args[]) {

		String primeiro = JOptionPane.showInputDialog("Write the first integer number:");
		String operator = JOptionPane.showInputDialog("Write the math operator:");

		if (operator.equals("+") || operator.equals("-") || operator.equals("/") || operator.equals("*")) {

			String segundo = JOptionPane.showInputDialog("Write the second integer number:");

			int number1 = Integer.parseInt(primeiro);
			int number2 = Integer.parseInt(segundo);
			float result = 0;

			if (operator.equals("+")) {
				result = number1 + number2;
			} else if (operator.equals("-")) {
				result = number1 - number2;
			} else if (operator.equals("*")) {
				result = number1 * number2;
			} else if (operator.equals("/")) {
				result = number1 / number2;
			}

			JOptionPane.showMessageDialog(null, "The result is " + result, "Math Operation!",
					JOptionPane.PLAIN_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(null, "Wrong math operator, asshole!", "Math Operation!",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
/*
 * (C) Copyright 2015 by Eliezer de Siqueira
 */