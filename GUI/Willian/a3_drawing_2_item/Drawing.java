package a3_drawing_2_item;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.event.MouseAdapter;

public class Drawing extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int[] var = new int[4];
	public static int i = 0;
	
	private static JFrame app;
	/**
	 * Create the panel.
	 */
	public Drawing() {
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				var[0] = arg0.getXOnScreen();
			    var[1] = arg0.getYOnScreen();
			}
		});
		
		
		addMouseMotionListener(new MouseMotionAdapter() {
			

			@Override
			public void mouseDragged(MouseEvent e) {
				
				
					var[2] = e.getX();
					var[3] = e.getY();
				
				
				
				//System.out.println(e.getX()+"|"+e.getY());
				
				
				repaint();
				System.out.println(var[0]+"|"+var[1]+"|"+var[2]+"|"+var[3]);
				
				System.out.println(e.isConsumed());
				if(e.isConsumed()){
					i = 0;
				}
			}	
		});
	}
	
	public static void main(String[] args) {
		Drawing panel = new Drawing();
		
		app = new JFrame();
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.getContentPane().add(panel);
		app.setSize(500,500);
		app.setVisible(true);
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.drawRect(var[0], var[1], var[2], var[3]);
	}
}
