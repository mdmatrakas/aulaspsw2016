/**
 * Criado por
 * @author will
 * Frame que multiplica 2 valores dos campos txt1 e txt2 e seta o resultado no result.
 */
package a2_frame_multiplication;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MultiplicationWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txt2;
	private JTextField txt1;
	private JTextField result;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MultiplicationWindow frame = new MultiplicationWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MultiplicationWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		txt1 = new JTextField();
		contentPane.add(txt1);
		txt1.setColumns(10);
		
		txt2 = new JTextField();
		contentPane.add(txt2);
		txt2.setColumns(10);
		
		JButton okBtn = new JButton("Multiply");
		okBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int a = Integer.parseInt(txt1.getText());
				int b = Integer.parseInt(txt2.getText());
				int c = a * b;
				String re = Integer.toString(c);
				result.setText(re);
			}
		});
		contentPane.add(okBtn);
		
		result = new JTextField();
		result.setEditable(false);
		contentPane.add(result);
		result.setColumns(10);
	}

}
