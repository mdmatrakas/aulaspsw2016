package a1_dialog_multiplication;

import javax.swing.JOptionPane;

/**
 * Caixa de Dialogo básico, afins de aprendizagem.
 * @author will
 *
 */
public class MultiplicationDialog {
	
	private int num1;
	private int num2;
	private int result;
	private String a;
	private String b;
	
	
	
	public MultiplicationDialog() {
		super();
		this.a = JOptionPane.showInputDialog("Digite o primeiro numero para multiplicação");
		this.b = JOptionPane.showInputDialog("Digite o segundo numero para multiplicação");
		
		try {
			this.num1 = Integer.parseInt(a);
			this.num2 = Integer.parseInt(b);
		} catch (NumberFormatException e) {
			System.out.println(e);
			JOptionPane.showMessageDialog(null, "Você não digitou números. Porfavor use somente números");
		}
		
		this.result = this.num1 * this.num2;
		
		JOptionPane.showMessageDialog(null, result);
	}



	public static void main(String[] args) {
		new MultiplicationDialog();
	}
}
