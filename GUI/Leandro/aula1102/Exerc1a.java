package aula1102;

/*
 *O Seguinte codigo abaixo vai fazer uma multiplicacao de dois numeros. 
 *E fara por caixas de frame 
 */

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class Exerc1a extends JFrame {
	
	private JTextField	valor1;
	private JTextField	valor2;
	private JTextField	result;
	private JLabel		rotulo1;
	private JLabel		rotulo2;
	
	public Exerc1a(){
		super("Multiplicacao");
		
		setLayout(new FlowLayout());
		
		valor1 = new JTextField(5);
		add(valor1);
		
		rotulo1 = new JLabel(" * ");
		rotulo1.setForeground(Color.ORANGE);
		add(rotulo1);
		
		valor2 = new JTextField(5);
		add(valor2);
		
		rotulo2 = new JLabel(" = ");
		rotulo2.setForeground(Color.ORANGE);
		add(rotulo2);
		
		result = new JTextField(5);
		result.setEditable(false);
		add(result);
		
		Ouvinte ouvinte = new Ouvinte();
		valor1.addActionListener(ouvinte);
		valor2.addActionListener(ouvinte);

	}	
		private class Ouvinte implements ActionListener{
					
			public void actionPerformed(ActionEvent event){
				
				int numero1 = Integer.parseInt(valor1.getText());
				int numero2 = Integer.parseInt(valor2.getText());
				
				int multi = numero1 * numero2;
		
				String resultado = String.valueOf(multi);
				result.setText(resultado);
		
			}	
		}
		
	public static void main(String args[]) {
		Exerc1a exerc = new Exerc1a();
		exerc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		exerc.setSize(500, 200); // configura o tamanho do frame
		exerc.setVisible(true); // exibe o frame
		
	}

}
