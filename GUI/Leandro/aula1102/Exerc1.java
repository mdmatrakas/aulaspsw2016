package aula1102;

import javax.swing.JOptionPane;

public class Exerc1 {
	
	/*
	 * O seguinte exercicio vai chamar caixas de dialogo, onde o usuario devera colocar os valores inteiros e depois ira receber 
	 * o resultado final	
	 */

		public static void main(String[] args) {
			// TODO Auto-generated method stub

			String valor1 = JOptionPane.showInputDialog("Digite o primeiro valor:");
			String valor2 = JOptionPane.showInputDialog("Digite o primeiro valor:");
			
			int numero1 = Integer.parseInt(valor1);
			int numero2 = Integer.parseInt(valor2);
			
			int multi = numero1 * numero2;
			
			JOptionPane.showMessageDialog(null, numero1+" * "+numero2+" = " + multi, "Multiplicacao", JOptionPane.DEFAULT_OPTION);
		}
}
