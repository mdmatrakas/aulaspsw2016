package a09_05_PainelLinhas;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Implementa uma linha
 * @author MDMatrakas
 *
 */
public class Linha
{
   private int x1; // coordenada x da primeira extremidade final
   private int y1; // coordenada y da primeira extremidade final
   private int x2; // coordenada x da segunda extremidade final
   private int y2; // coordenada y da segunda extremidade final
   private Color cor; // cor dessa forma

   /**
    *  construtor com valores de entrada
    * @param x1 coordenada x da primeira extremidade final
    * @param y1 coordenada y da primeira extremidade final
    * @param x2 coordenada x da segunda extremidade final
    * @param y2 coordenada y da segunda extremidade final
    * @param cor cor dessa forma
    */
   public Linha( int x1, int y1, int x2, int y2, Color cor )
   {
      this.x1 = x1; // configura a coordenada x da primeira extremidade final
      this.y1 = y1; // configura a coordenada y da primeira extremidade final
      this.x2 = x2; // configura a coordenada x da segunda extremidade final
      this.y2 = y2; // configura a coordenada y da segunda extremidade final
      this.cor = cor; // configura a cor
   } 
   
   /**
    *  Desenha a linha na cor especificada
    * @param g
    */
   public void draw( Graphics g )
   {
      g.setColor( cor );
      g.drawLine( x1, y1, x2, y2 );
   }
} 