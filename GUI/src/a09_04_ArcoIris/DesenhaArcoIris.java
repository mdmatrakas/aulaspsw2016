package a09_04_ArcoIris;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Demonstra a utiliza��o de cores em um array.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class DesenhaArcoIris extends JPanel
{
	// Define as cores �ndigo e violeta
	final Color		VIOLET	= new Color(128, 0, 128);
	final Color		INDIGO	= new Color(75, 0, 130);

	// a utilizar no arco-�ris, iniciando da parte mais interna
	// As duas entradas em branco resultam em um arco vazio no centro
	private Color	cores[]	= { Color.WHITE, Color.WHITE, VIOLET, INDIGO,
			Color.BLUE, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED };

	public static void main(String args[])
	{
		DesenhaArcoIris painel = new DesenhaArcoIris();
		JFrame app = new JFrame();

		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.add(painel);
		app.setSize(400, 250);
		app.setVisible(true);
	}

	/**
	 * construtor
	 */
	public DesenhaArcoIris()
	{
		setBackground(Color.WHITE); // configura o fundo como branco
	}

	/**
	 * desenha um arco-�ris utilizando c�rculos conc�ntricos
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		int radius = 20; // raio de um arco

		// desenha o arco-�ris perto da parte central inferior
		int centerX = getWidth() / 2;
		int centerY = getHeight() - 10;

		// desenha arcos preenchidos com o mais externo
		for (int counter = cores.length; counter > 0; counter--)
		{
			// configura a cor para o arco atual
			g.setColor(cores[counter - 1]);

			// preenche o arco de 0 a 180 graus
			g.fillArc(centerX - counter * radius, centerY - counter * radius,
					counter * radius * 2, counter * radius * 2, 30, 120);
		}
	}
}