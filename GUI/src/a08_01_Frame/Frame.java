/**
 * 
 */
package a08_01_Frame;

import javax.swing.JFrame;

/**
 * Demonstra o uso da classe JFrame da API Java Swing.
 * 
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class Frame extends JFrame
{
	/**
	 * @param args N�o utilizado
	 */
	public static void main(String[] args)
	{
		new Frame();
	}
	
	public Frame()
	{
		// Configura um t�tulo para a janela
		super("Abrindo uma janela!");
		// Determina a opera��o padr�o ao se fechar a janela
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Determina o tamanho da janela
		setSize(500, 200);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		// Torna a janela visivel
		setVisible(true);
	}
}
