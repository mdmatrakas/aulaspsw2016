package a07_01_CaixasDialogoMensagem;

import javax.swing.JOptionPane;

/**
 * Entrada b�sica com uma caixa de di�logo.
 * 
 * O usu�rio � solicitado e entrar seu nome, e depis uma mensagem � exibida usando o nome fornecido.
 * A mensagem � formatada utilizando-se um objeto String, com o m�todo format().
 * 
 * @author Matrakas
 * 
 */
public class Dialogo03_Entrada
{
	/**
	 * M�todo de entrada da aplica��o de exemplo de uso de caixas de dialogo.
	 * 
	 * @param args
	 *            N�o utilizado.
	 */
	public static void main(String args[])
	{
		// pede para o usu�rio inserir seu nome
		String name = JOptionPane.showInputDialog("Qual � o seu nome?");
		// cria a mensagem
		String message = String.format("Bem vindo a programa��o Java, %s!", name);
		// exibe a mensagem para cumprimentar o usu�rio pelo nome
		JOptionPane.showMessageDialog(null, message);
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
