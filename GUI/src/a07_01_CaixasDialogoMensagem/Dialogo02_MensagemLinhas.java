package a07_01_CaixasDialogoMensagem;

import javax.swing.JOptionPane; // importa classe JOptionPane

/**
 * Imprimindo m�ltiplas linhas em uma caixa de di�logo.
 * 
 * @author Matrakas
 * 
 */
public class Dialogo02_MensagemLinhas
{
	/**
	 * M�todo de entrada da aplica��o de exemplo de uso de caixas de dialogo.
	 * 
	 * @param args
	 *            N�o utilizado.
	 */
	public static void main(String args[])
	{
		// exibe um di�logo com a mensagem
		JOptionPane.showMessageDialog(null, 
				"Uma mensagem\nde boas vindas �\nprograma��o Java!");
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
