package a07_01_CaixasDialogoMensagem;

import javax.swing.JOptionPane; // programa utiliza JOptionPane

/**
 * Programa de adi��o que utiliza JOptionPane para entrada e sa�da.
 * 
 * O usu�rio � solicitado a fornecer dois valores, estes s�o convertidos para vari�veis inteiras,
 * que s�o adicionados e uma mensagem apresentando a resposta � formatada e exibida.
 * 
 * Demonstra a formata��o da caixa de mensagem, fornecendo um t�tulo e especificando o tipo de
 * mensagem a ser exibida.
 * 
 * @author Matrakas
 */
public class Dialogo04_AdicionandoDoisNumeros
{
	/**
	 * M�todo de entrada da aplica��o de exemplo de uso de caixas de dialogo.
	 * 
	 * @param args
	 *            N�o utilizado.
	 */
	public static void main(String args[])
	{
		// obt�m a entrada de usu�rio a partir dos di�logos de entrada JOptionPane
		String primeiro = JOptionPane.showInputDialog("Digite um n�mero inteiro:");
		String segundo = JOptionPane.showInputDialog("Digite um segundo n�mero inteiro:");

		// converte String em valores inteiros para utiliza��o em um c�lculo
		int number1 = Integer.parseInt(primeiro);
		int number2 = Integer.parseInt(segundo);

		int sum = number1 + number2; // soma os n�meros

		// exibe o resultado em um di�logo de mensagem JOptionPane
		JOptionPane.showMessageDialog(null, 
				"A soma � " + sum, 
				"Soma de dois inteiros!",
				JOptionPane.PLAIN_MESSAGE);
		//JOptionPane.PLAIN_MESSAGE // mensagem simples
		//JOptionPane.ERROR_MESSAGE // mensagem de erro
		//JOptionPane.INFORMATION_MESSAGE // mensagem de informa��o (exclama��o)
		//JOptionPane.QUESTION_MESSAGE // menssagem de pergunta
		//JOptionPane.WANTS_INPUT_PROPERTY // mensagem de aviso
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/