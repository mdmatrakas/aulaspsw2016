package a10_03_ComboBox;

import java.awt.FlowLayout;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * Utilizando um JComboBox para selecionar uma imagem a exibir.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class FrameComboBox extends JFrame
{
	private JComboBox<String>	comboImagens;	// caixa de combina��o para armazenar nomes de �cones
	private JLabel		rotulo;			// r�tulo para exibir �cone selecionado

	private String		nomes[]		= { "bug1.gif", "bug2.gif",
			"travelbug.gif", "buganim.gif" };
	private Icon		icones[]	= {
			new ImageIcon(getClass().getResource(nomes[0])),
			new ImageIcon(getClass().getResource(nomes[1])),
			new ImageIcon(getClass().getResource(nomes[2])),
			new ImageIcon(getClass().getResource(nomes[3])) };

	public static void main(String args[])
	{
		FrameComboBox f = new FrameComboBox();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(450, 200); // configura o tamanho do frame
		f.setVisible(true); // exibe o frame
	}

	// construtor ComboBoxFrame adiciona JComboBox ao JFrame
	public FrameComboBox()
	{
		super("Testando JComboBox");
		setLayout(new FlowLayout()); // configura o layout de frame

		comboImagens = new JComboBox<String>(nomes); // configura JComboBox
		comboImagens.setMaximumRowCount(3); // exibe no m�ximo tr�s linhas

		comboImagens.addItemListener(
				new ItemListener() // classe interna an�nima
				{
					// trata evento JComboBox
					public void itemStateChanged(ItemEvent event)
					{
						// determina se caixa de sele��o est� marcada ou n�o
						if (event.getStateChange() == ItemEvent.SELECTED)
							rotulo.setIcon(icones[comboImagens.getSelectedIndex()]);
					}
				}
				);

		add(comboImagens); // adiciona combobox ao JFrame
		rotulo = new JLabel(icones[0]); // exibe primeiro �cone
		add(rotulo); // adiciona r�tulo ao JFrame
	}
}
