package a08_03_FrameLabel;

import javax.swing.JFrame;

/**
 * Aplica��o para teste da classe FrameJlabel.
 * @author MDMatrakas
 *
 */
public class AppFrameJLabel
{
   public static void main( String args[] )
   { 
      FrameJLabel labelFrame = new FrameJLabel(); // cria LabelFrame
      labelFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      labelFrame.setSize( 275, 180 ); // configura o tamanho do frame
      labelFrame.setVisible( true ); // exibe o frame
   } 
} 