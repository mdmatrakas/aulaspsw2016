package a13_01_FileChooser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.JFileChooser;

public class ReadTextFile {
	   private Scanner input;

	   
	   public void openFile(String name)
	   {
	      try
	      {
	         input = new Scanner( new File( name ) );
	      } 
	      catch ( FileNotFoundException fileNotFoundException )
	      {
	         System.err.println( "Error opening file." );
	         System.exit( 1 );
	      } 
	   } 

	   // l� um arquivo de texto
	   public void readRecords()
	   {
	      // linha de texto do arquivo
	      String line;

	      try // l� os registros no arquivo utilizando o objeto Scanner
	      {
	         while (input.hasNext())
	         {
	            line =  input.nextLine(); // l� uma linha

	            // exibe o conte�do da linha
	            System.out.println( line );
	         } 
	      } 
	      catch ( NoSuchElementException elementException )
	      {
	         System.err.println( "File improperly formed." );
	         input.close();
	         System.exit( 1 );
	      } 
	      catch ( IllegalStateException stateException )
	      {
	         System.err.println( "Error reading from file." );
	         System.exit( 1 );
	      } 
	   } 

	   // fecha o arquivo 
	   public void closeFile()
	   {
	      if ( input != null )
	         input.close(); // fecha o arquivo
	   } 

	   public static void main(String []a)
	   {
		   ReadTextFile rtf = new ReadTextFile();
		   
		   // Cria um objeto JFileChooser - Janela para escolher arquivos
		   JFileChooser jfc = new JFileChooser();
		   
		   // Especifica que s� podem ser selecionados arquivos
		   // (N�o retorna um diret�rio)
		   jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		   if(jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
		   {
			   rtf.openFile(jfc.getSelectedFile().getPath());
			   rtf.readRecords();
			   rtf.closeFile();
		   }
		   
		   
	   }
}
