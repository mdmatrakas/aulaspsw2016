package a12_02_DetalhesMouse;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Demonstrando cliques de mouse e distinguindo entre bot�es do mouse.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class FrameDetalhesMouse extends JFrame
{
	private String	detalhes;	// representa��o String
	private JLabel	barraInfo;	// JLabel que aparece na parte inferior da janela

	public static void main(String args[])
	{
		FrameDetalhesMouse f = new FrameDetalhesMouse();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(400, 150); // configura o tamanho do frame
		f.setVisible(true); // exibe o frame
	}

	/**
	 * Construtor configura String de barra de t�tulo e registra o listener de
	 * mouse
	 */
	public FrameDetalhesMouse()
	{
		super("Bot�es e cliques do mouse.");

		barraInfo = new JLabel("Clique um dos bot�s do mouse.");
		add(barraInfo, BorderLayout.SOUTH);
		addMouseListener(new OuvinteCliquesMouse());
	}

	/**
	 * classe interna para tratar eventos de mouse
	 * 
	 * @author MDMatrakas
	 * 
	 */
	private class OuvinteCliquesMouse extends MouseAdapter
	{
		/**
		 * trata evento de clique de mouse e determina qual bot�o foi
		 * pressionado
		 */
		public void mouseClicked(MouseEvent event)
		{
			int xPos = event.getX(); // obt�m posi��o x do mouse
			int yPos = event.getY(); // obt�m posi��o y do mouse

			detalhes = String.format("Clicado %d vez(es) em [%d,%d]",
					event.getClickCount(), xPos, yPos);

			if (event.isMetaDown()) // bot�o direito do mouse
				detalhes += " com o bot�o direito";
			else
				if (event.isAltDown()) // bot�o do meio do mouse
					detalhes += " com o bot�o do meio";
				else
					// bot�o esquerdo do mouse
					detalhes += " com o bot�o esquerdo";

			barraInfo.setText(detalhes); // exibe mensagem em barraInfo
		}
	}
}