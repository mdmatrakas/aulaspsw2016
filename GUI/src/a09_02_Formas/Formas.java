package a09_02_Formas;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Demonstra o desenho de diferentes formas.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class Formas extends JPanel
{
	private int	escolha; // escolha do usu�rio de qual forma desenhar

	public static void main(String args[])
	{
		// obt�m a escolha do usu�rio
		String entrada = JOptionPane.showInputDialog(
				"Entre com 1 para desenhar ret�ngulos\n" +
				"Entre com 2 para desenhar elipses");
		int escolha = Integer.parseInt(entrada); // converte a entrada em int

		// cria o painel com a entrada do usu�rio
		Formas painel = new Formas(escolha);

		JFrame app = new JFrame(); // cria um novo JFrame
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.add(painel); // adiciona o painel ao frame
		app.setSize(300, 300); // configura o tamanho desejado
		app.setVisible(true); // mostra o frame
	} 
	
	/**
	 *  construtor configura a escolha do usu�rio
	 * @param escolhaUsr Inteiro indicando a escolha de forma feita pelo usu�rio.
	 * 1 para retangulos
	 * 2 para elipses
	 */

	public Formas(int escolhaUsr)
	{
		escolha = escolhaUsr;
	} 

	/**
	 *  desenha uma cascata de formas que iniciam do canto superior esquerdo
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		for (int i = 0; i < 10; i++)
		{
			// seleciona a forma com base na escolha do usu�rio
			switch (escolha)
			{
				case 1: // desenha ret�ngulos
					// drawRect(x1, y1, x2, y2);
					g.drawRect(10 + i * 10, 10 + i * 15, 50 + i * 10,	50 + i * 15);
					break;
				case 2: // desenha elipses
					g.drawOval(10 + i * 10, 10 + i * 10, 50 + i * 10,	50 + i * 10);
					break;
			} 
		} 
	} 
} 