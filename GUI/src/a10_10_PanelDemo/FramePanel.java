package a10_10_PanelDemo;

import java.awt.GridLayout;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;

/**
 * Utilizando um JPanel para ajudar a fazer o layout dos componentes.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class FramePanel extends JFrame 
{
	/**
	 * painel para armazenar bot�es
	 */
   private JPanel panelBotoes; 
   /**
    * array de bot�es
    */
   private JButton botoes[]; 

   /**
    *  construtor sem argumento
    */
   public FramePanel()
   {
      super( "Panel Demo" );
      botoes = new JButton[ 6 ]; // cria bot�es de array
      panelBotoes = new JPanel(); // configura painel
      panelBotoes.setLayout( new GridLayout( 2, botoes.length/2 ) );

      // cria e adiciona bot�es
      for ( int count = 0; count < botoes.length; count++ ) 
      {
         botoes[ count ] = new JButton( "Bot�o " + ( count + 1 ) );
         panelBotoes.add( botoes[ count ] ); // adiciona bot�o ao painel
      } // for final

      add( panelBotoes, BorderLayout.SOUTH ); // adiciona painel ao JFrame
   } 
} 


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/