package a08_02_FrameLabel;

import javax.swing.JFrame; // fornece recursos b�sicos de janela
import javax.swing.JLabel; // exibe texto e imagens

/**
 * Demonstrar o uso da classe JLabel e da classe JFrame.
 *  * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class FrameJLabel extends JFrame
{
	private JLabel	label;	// JLabel texto

	public static void main(String[] args)
	{
		FrameJLabel f = new FrameJLabel();
		// Determina a opera��o padr�o ao se fechar a janela
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Determina o tamanho da janela
		f.setSize(500, 200);
		// Torna a janela visivel
		f.setVisible(true);
	}

	// construtor LabelFrame adiciona JLabels a JFrame
	public FrameJLabel()
	{
		super("Teste da classe JLabel");

		// Construtor JLabel com um argumento de string
		label = new JLabel("Campo de texto simples.");

		label.setToolTipText("Este � o r�tulo!");
		add(label); // adiciona o label ao JFrame
	}
}
