package a12_01_EventosMouse;

import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Demonstrando os eventos de mouse.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class FrameEventosMouse extends JFrame
{
	private JPanel	painelMouse;	// painel em que os eventos de mouse ocorrer�o
	private JLabel	barraInfo;		// r�tulo que exibe informa��es de evento

	public static void main(String args[])
	{
		FrameEventosMouse f = new FrameEventosMouse();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(300, 100); // configura o tamanho do frame
		f.setVisible(true); // exibe o frame
	}

	/**
	 * Construtor MouseTrackerFrame configura GUI e registra handlers de evento
	 * de mouse
	 */
	public FrameEventosMouse()
	{
		super("Demonstrando eventos do mouse");

		painelMouse = new JPanel(); // cria painel
		painelMouse.setBackground(Color.WHITE); // configura cor de fundo
		add(painelMouse, BorderLayout.CENTER); // adiciona painel ao JFrame

		barraInfo = new JLabel("Mouse fora do painel");
		add(barraInfo, BorderLayout.SOUTH); // adiciona r�tulo ao JFrame

		// cria e registra ouvinte para mouse e eventos de movimento de mouse
		OuvinteMouse ouvinte = new OuvinteMouse();
		painelMouse.addMouseListener(ouvinte);
		painelMouse.addMouseMotionListener(ouvinte);
	}

	private class OuvinteMouse implements MouseListener, 
	MouseMotionListener
	{
		// Handlers de evento de MouseMotionListener
		/**
		 * Trata evento quando usu�rio arrasta o mouse com o bot�o pressionado
		 */
		public void mouseDragged(MouseEvent event)
		{
			barraInfo.setText(String.format("Arrastado em [%d, %d]",
					event.getX(), event.getY()));
		}

		/**
		 * Trata evento quando usu�rio move o mouse
		 */
		public void mouseMoved(MouseEvent event)
		{
			barraInfo.setText(String.format("Movimentado em [%d, %d]",
					event.getX(), event.getY()));
		}
		
		
		// Handlers de evento de MouseListener
		/**
		 * Trata evento quando o mouse � liberado logo depois de pressionado
		 */
		public void mouseClicked(MouseEvent event)
		{
			barraInfo.setText(String.format("Clicado em [%d, %d]",
					event.getX(), event.getY()));
		}

		/**
		 * Trata evento quando mouse � pressionado
		 */
		public void mousePressed(MouseEvent event)
		{
			barraInfo.setText(String.format("pressionado em [%d, %d]",
					event.getX(), event.getY()));
		}

		/**
		 * Trata evento quando mouse � liberado depois da opera��o de arrastar
		 */
		public void mouseReleased(MouseEvent event)
		{
			barraInfo.setText(String.format("Liberado em [%d, %d]",
					event.getX(), event.getY()));
		}

		/**
		 * Trata evento quando mouse entra na �rea
		 */
		public void mouseEntered(MouseEvent event)
		{
			barraInfo.setText(String.format("Mouse entrou em [%d, %d]",
					event.getX(), event.getY()));
			painelMouse.setBackground(Color.GREEN);
		}

		/**
		 * Trata evento quando mouse sai da �rea
		 */
		public void mouseExited(MouseEvent event)
		{
			barraInfo.setText("Mouse saiu do painel");
			painelMouse.setBackground(Color.WHITE);
		}


	}
}