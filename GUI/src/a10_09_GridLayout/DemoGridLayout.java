package a10_09_GridLayout;

import javax.swing.JFrame;

/**
 * Testando GridLayoutFrame.
 * @author MDMatrakas
 *
 */
public class DemoGridLayout 
{
   public static void main( String args[] )
   { 
      FrameGridLayout grid = new FrameGridLayout(); 
      grid.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      grid.setSize( 300, 200 ); // configura o tamanho do frame
      grid.setVisible( true ); // exibe o frame
   } 
} 