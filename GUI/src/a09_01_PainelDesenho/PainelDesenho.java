package a09_01_PainelDesenho;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Desenha duas linhas que se cruzam em um painel.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class PainelDesenho extends JPanel
{
	public static void main(String args[])
	{
		// cria um painel que cont�m nosso desenho
		PainelDesenho painel = new PainelDesenho();

		// cria um novo quadro para armazenar o painel
		JFrame app = new JFrame("Desenhando ...");

		// configura o frame para ser encerrado quando ele � fechado
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		app.add(painel); // adiciona o painel ao frame
		app.setSize(250, 250); // configura o tamanho do frame
		app.setVisible(true); // torna o frame vis�vel
	}

	/**
	 * desenha um X a partir dos cantos do painel
	 */
	public void paintComponent(Graphics g)
	{
		// chama paintComponent para assegurar que o painel � exibido corretamente
		super.paintComponent(g);

		int largura = getWidth(); // largura total
		int altura = getHeight(); // altura total

		// desenha uma linha a partir do canto superior esquerdo at� o inferior direito
		g.drawLine(0, 0, largura, altura);

		// desenha uma linha a partir do canto inferior esquerdo at� o superior direito
		g.drawLine(0, altura, largura, 0);
	} 
}