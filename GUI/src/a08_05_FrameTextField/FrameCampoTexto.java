package a08_05_FrameTextField;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JOptionPane;

/**
 * Demonstrando a classe JTextField e JPasswordField com tratamento de eventos.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class FrameCampoTexto extends JFrame {
	private JTextField campoTexto1; // campo de texto com tamanho
									// configurado
	private JTextField campoTexto2; // campo de texto constru�do com
									// texto
	private JTextField campoTexto3; // campo de texto com texto e
									// tamanho
	private JPasswordField campoSenha; // campo de senha com texto
	int num1, num2;

	// construtor FrameCampoTexto adiciona JTextFields a JFrame
	public FrameCampoTexto() {
		super("Testando JTextField e JPasswordField");
		setLayout(new FlowLayout()); // configura o layout de frame
		// constr�i campoTexto com 10 colunas
		campoTexto1 = new JTextField(10);
		add(campoTexto1); // adiciona campoTexto1 ao JFrame
		// constr�i campo de texto com texto padr�o
		campoTexto2 = new JTextField("Digite texto aqui");
		add(campoTexto2); // adiciona campoTexto2 ao JFrame
		// constr�i campoTexto com o texto padr�o e 21 colunas
		campoTexto3 = new JTextField("Campo n�o edit�vel.", 21);
		campoTexto3.setEditable(false); // desativa a edi��o
		add(campoTexto3); // adiciona campoTexto3 ao JFrame
		// constr�i campoSenha com o texto padr�o
		campoSenha = new JPasswordField("Texto escondido");
		add(campoSenha); // adiciona campoSenha ao JFrame

		// handlers de evento registradores
		OuvinteCampoTexto ouvinte = new OuvinteCampoTexto();
		campoTexto1.addActionListener(ouvinte);
		campoTexto2.addActionListener(ouvinte);
		campoTexto3.addActionListener(ouvinte);
		campoSenha.addActionListener(ouvinte);
	}

	// classe interna private para tratamento de eventos
	private class OuvinteCampoTexto implements ActionListener {
		// processa eventos de campo de texto
		public void actionPerformed(ActionEvent event) {
			String string = ""; // declara string a ser exibida

			// usu�rio pressionou Enter no JTextField textField1
			if (event.getSource() == campoTexto1) {
				string = String.format("campoTexto1: %s\ntexto %s", event.getActionCommand(), campoTexto1.getText());
				num1 = Integer.parseInt(campoTexto1.getText());
			}
			// usu�rio pressionou Enter no JTextField textField2
			else if (event.getSource() == campoTexto2)
				string = String.format("campoTexto2: %s", event.getActionCommand());

			// usu�rio pressionou Enter no JTextField textField3
			else if (event.getSource() == campoTexto3)
				string = String.format("campoTexto3: %s", event.getActionCommand());

			// usu�rio pressionou Enter no JTextField passwordField
			else if (event.getSource() == campoSenha)
				string = String.format("campoSenha: %s", new String(campoSenha.getPassword()));

			// exibe o conte�do de JTextField
			JOptionPane.showMessageDialog(null, string);
		}
	}
}