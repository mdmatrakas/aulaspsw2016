package a08_05_FrameTextField;

import javax.swing.JFrame;

/**
 * Aplica��o para teste da classe FrameCampoTexto.
 * 
 * @author MDMatrakas
 * 
 */
public class AppCampoTexto
{
	public static void main(String args[])
	{
		FrameCampoTexto textFieldFrame = new FrameCampoTexto();
		textFieldFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		textFieldFrame.setSize(325, 100); // configura o tamanho do frame
		textFieldFrame.setVisible(true); // exibe o frame
	}
}