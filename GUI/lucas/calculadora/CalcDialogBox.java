package calculadora;

import javax.swing.JOptionPane;

public class CalcDialogBox {
	public static void main(String[] args)
	{
		float resposta=0;
		String valor1 = JOptionPane.showInputDialog("insira um valor");
		String valor2 = JOptionPane.showInputDialog("insira outro valor");
		
		int n1 = Integer.parseInt(valor1);
		int n2 = Integer.parseInt(valor2);
		
		String operador = JOptionPane.showInputDialog("insira um operador: / ,* ,+ ou -");
		
		if(operador.equals("/")){
		resposta=n1/n2;
		}

		else if(operador.equals("*")){
			resposta=n1*n2;
		}
		else if(operador.equals("+")){
			resposta=n1+n2;
		}
		else if(operador.equals("-")){
			resposta=n1-n2;
		}
		JOptionPane.showMessageDialog( null,
	    		  "resposta="+ resposta );
		
		
	}
}
