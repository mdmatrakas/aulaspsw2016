package agenda;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.BorderLayout;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import java.awt.Font;
import javax.swing.table.DefaultTableModel;

public class ClientWindow {

	private JFrame frmJagenda;
	private JTable table;

	/**
	 * Launch the application.
	 * @return 
	 */
	public static void runwindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientWindow window = new ClientWindow();
					window.frmJagenda.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ClientWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmJagenda = new JFrame();
		frmJagenda.getContentPane().setBackground(Color.DARK_GRAY);
		
		String[] columnNames = {"Name", "Phone"};
		Object[][] data = {
				{"Alan", "99880101"},
				{"Marina", "99880202"},
				{"Lucia", "99880303"}
		};
		table = new JTable(data, columnNames);
		table.setForeground(Color.WHITE);
		table.setBackground(Color.DARK_GRAY);
		table.setPreferredScrollableViewportSize(new Dimension(500, 50));
		table.setFillsViewportHeight(true);
		table.setFont(new Font("SansSerif", Font.PLAIN, 12));
		JScrollPane scrollpane = new JScrollPane(table);
		
		frmJagenda.getContentPane().add(scrollpane, BorderLayout.CENTER);
		frmJagenda.setTitle("JAgenda CC");
		frmJagenda.setBounds(100, 100, 450, 300);
//		frmJagenda.add();
		frmJagenda.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frmJagenda.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmOpenTxt = new JMenuItem("Open TXT");
		mnFile.add(mntmOpenTxt);
		
		JMenuItem mntmSaveTxt = new JMenuItem("Save TXT");
		mnFile.add(mntmSaveTxt);
	}

}
