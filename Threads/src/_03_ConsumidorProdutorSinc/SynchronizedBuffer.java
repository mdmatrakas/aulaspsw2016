package _03_ConsumidorProdutorSinc;
// Fig. 23.11: SynchronizedBuffer.java
// SynchronizedBuffer sincroniza acesso a um �nico inteiro compartilhado.
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;

public class SynchronizedBuffer implements Buffer
{
   // Bloqueio para controlar sincroniza��o com esse buffer
   private Lock accessLock = new ReentrantLock();     

   // condi��es para controlar leitura e grava��o 
   private Condition canWrite = accessLock.newCondition();
   private Condition canRead = accessLock.newCondition();

   private int buffer = -1; // compartilhado pelas threads Produtor e Consumidor
   private boolean occupied = false; // se o buffer estiver ocupado
   
   // coloca o valor int no buffer
   public void set( int value )
   {
      accessLock.lock(); // bloqueia esse objeto

      // envia informa��es de thread e de buffer para a sa�da, ent�o espera
      try
      {
         // enquanto o buffer n�o estiver vazio, coloca thread no estado de espera
         while ( occupied )
         {
            System.out.println( "Producer tries to write." );
            displayState( "Buffer full. Producer waits." );
            canWrite.await(); // espera at� que o buffer esteja vazio
         }

         buffer = value; // configura novo valor de buffer

         // indica que a produtora n�o pode armazenar outro valor
         // at� a consumidora recuperar valor atual de buffer
         occupied = true;
        
         displayState( "Producer writes " + buffer );

         // sinaliza a thread que est� esperando para ler a partir do buffer
         canRead.signal();                           
      } 
      catch ( InterruptedException exception )
      {
         exception.printStackTrace();
      }
      finally
      {
         accessLock.unlock(); // desbloqueia esse objeto
      } 
   }
    
   // retorna valor do buffer
   public int get()
   {
      int readValue = 0; // inicializa de valor lido a partir do buffer
      accessLock.lock(); // bloqueia esse objeto

      // envia informa��es de thread e de buffer para a sa�da, ent�o espera
      try
      {
         // enquanto os dados n�o s�o lidos, coloca thread em estado de espera
         while ( !occupied ) 
         {
            System.out.println( "Consumer tries to read." );
            displayState( "Buffer empty. Consumer waits." );
            canRead.await(); // espera at� o buffer tornar-se cheio
         }

         // indica que a produtora pode armazenar outro valor
         // porque a consumidora acabou de recuperar o valor do buffer 
         occupied = false;

         readValue = buffer; // recupera o valor do buffer
         displayState( "Consumer reads " + readValue );

         // sinaliza a thread que est� esperando o buffer tornar-se vazio
         canWrite.signal();                             
      }
      // se a thread na espera tiver sido interrompida, imprime o rastreamento de pilha
      catch ( InterruptedException exception ) 
      {
         exception.printStackTrace();
      }
      finally
      {
         accessLock.unlock(); // desbloqueia esse objeto
      }

      return readValue;
   }
    
   // exibe a opera��o atual e o estado de buffer
   public void displayState( String operation )
   {
      System.out.printf( "%-40s%d\t\t%b\n\n", operation, buffer, 
         occupied );
   } 
}


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
