package _07_ConsumidorProdutorSinc2;
// Fig. 23.19: SynchronizedBuffer.java
// SynchronizedBuffer sincroniza acesso a um �nico inteiro compartilhado.

public class SynchronizedBuffer implements Buffer
{
   private int buffer = -1; // compartilhado pelas threads Produtor e Consumidor
   private boolean occupied = false; // contagem de buffers ocupados
   
   // coloca o valor no buffer
   public synchronized void set( int value )
   {
      // enquanto n�o houver posi��es vazias, coloca a thread em estado de espera
      while ( occupied )
      {
         // envia informa��es de thread e de buffer para a sa�da, ent�o espera
         try 
         {
            System.out.println( "Producer tries to write." );
            displayState( "Buffer full. Producer waits." );
            wait();
         }
         catch ( InterruptedException exception ) 
         {
            exception.printStackTrace();
         }
      }
        
      buffer = value; // configura novo valor de buffer
        
      // indica que a produtora n�o pode armazenar outro valor
      // at� a consumidora recuperar valor atual de buffer
      occupied = true;                                
        
      displayState( "Producer writes " + buffer );
      
      notify(); // instrui a thread em espera a entrar no estado execut�vel
   }
    
   // retorna valor do buffer
   public synchronized int get()
   {
      // enquanto os dados n�o s�o lidos, coloca thread em estado de espera
      while ( !occupied )
      {
         // envia informa��es de thread e de buffer para a sa�da, ent�o espera
         try 
         {
            System.out.println( "Consumer tries to read." );
            displayState( "Buffer empty. Consumer waits." );
            wait();
         }
         catch ( InterruptedException exception ) 
         {
            exception.printStackTrace();
         }
      }

      // indica que a produtora pode armazenar outro valor
      // porque a consumidora acabou de recuperar o valor do buffer 
      occupied = false;                                

      int readValue = buffer; // armazena valor em buffer
      displayState( "Consumer reads " + readValue );
      
      notify(); // instrui a thread em espera a entrar no estado execut�vel

      return readValue;
   } 
    
   // exibe a opera��o atual e o estado de buffer
   public void displayState( String operation )
   {
      System.out.printf( "%-40s%d\t\t%b\n\n", operation, buffer, occupied );
   }
}


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
