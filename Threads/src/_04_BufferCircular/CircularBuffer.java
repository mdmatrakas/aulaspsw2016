package _04_BufferCircular;
// Fig. 23.13: CircularBuffer.java
// SynchronizedBuffer sincroniza acesso a um �nico inteiro compartilhado.
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;

import _02_ConsumidorProdutor.Buffer;

public class CircularBuffer implements Buffer
{
   // Bloqueio para controlar sincroniza��o com esse buffer
   private Lock accessLock = new ReentrantLock();     

   // condi��es para controlar leitura e grava��o 
   private Condition canWrite = accessLock.newCondition();
   private Condition canRead = accessLock.newCondition();

   private int[] buffer = { -1, -1, -1 };

   private int occupiedBuffers = 0; // conta n�mero de buffers utilizados
   private int writeIndex = 0; // �ndice para escrever o pr�ximo valor
   private int readIndex = 0; // �ndice para ler o pr�ximo valor
   
   // coloca o valor no buffer
   public void set( int value )
   {
      accessLock.lock(); // bloqueia esse objeto
               
      // envia informa��es de thread e de buffer para a sa�da, ent�o espera
      try
      {
         // enquanto n�o houver posi��es vazias, p�e o thread no estado de espera
         while ( occupiedBuffers == buffer.length ) 
         {
            System.out.printf( "All buffers full. Producer waits.\n" );
            canWrite.await();// espera at� um elemento buffer ser liberado
         }

         buffer[ writeIndex ] = value; // configura novo valor de buffer

         // atualiza �ndice de grava��o circular
         writeIndex = ( writeIndex + 1 ) % buffer.length;

         occupiedBuffers++; // mais um elemento buffer est� cheio
         displayState( "Producer writes " + buffer[ writeIndex ] );
         canRead.signal(); // sinaliza threads que est�o esperando para ler o buffer
      }
      catch ( InterruptedException exception )
      {
         exception.printStackTrace();
      } 
      finally
      {
         accessLock.unlock(); // desbloqueia esse objeto
      } 
   } 

   // retorna valor do buffer
   public int get()
   {
      int readValue = 0; // inicializa de valor lido a partir do buffer
      accessLock.lock(); // bloqueia esse objeto

      // espera at� que o buffer tenha dados, ent�o l� o valor
      try 
      {
         // enquanto os dados n�o s�o lidos, coloca thread em estado de espera
         while ( occupiedBuffers == 0 ) 
         {
            System.out.printf( "All buffers empty. Consumer waits.\n" );
            canRead.await(); // espera at� que um elemento buffer seja preenchido
         }

         readValue = buffer[ readIndex ]; // l� valor do buffer

         // atualiza �ndice de leitura circular 
         readIndex = ( readIndex + 1 ) % buffer.length;

         occupiedBuffers--; // mais um elemento buffer est� vazio
         displayState( "Consumer reads " + readValue );
         canWrite.signal(); // sinaliza threads que est�o esperando para gravar no buffer
      } 
      // se a thread na espera tiver sido interrompida, imprime o rastreamento de pilha
      catch ( InterruptedException exception ) 
      {
         exception.printStackTrace();
      }
      finally
      {
         accessLock.unlock(); // desbloqueia esse objeto
      } 

      return readValue;
   } 
    
   // exibe a opera��o atual e o estado de buffer
   public void displayState( String operation )
   {
      // gera sa�da de opera��o e n�mero de buffers ocupados
      System.out.printf( "%s%s%d)\n%s", operation, 
         " (buffers occupied: ", occupiedBuffers, "buffers:  " );

      for ( int value : buffer )
         System.out.printf( " %2d  ", value ); // gera a sa�da dos valores no buffer

      System.out.print( "\n         " );
      for ( int i = 0; i < buffer.length; i++ )
         System.out.print( "---- " );

      System.out.print( "\n         " );
      for ( int i = 0; i < buffer.length; i++ )
      {
         if ( i == writeIndex && i == readIndex )
            System.out.print( " WR" ); // �ndice de grava��o e leitura 
         else if ( i == writeIndex )
            System.out.print( " W   " ); // s� grava �ndice
         else if ( i == readIndex )
            System.out.print( "  R  " ); // s� l� �ndice
         else
            System.out.print( "     " ); // nenhum dos �ndices
      }

      System.out.println( "\n" );
   }
} 


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
