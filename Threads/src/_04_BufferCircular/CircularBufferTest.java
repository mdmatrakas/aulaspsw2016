package _04_BufferCircular;
// Fig. 23.14: CircularBufferTest.java
// Aplicativo mostra duas threads que manipulam um buffer circular.
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import _02_ConsumidorProdutor.Buffer;
import _02_ConsumidorProdutor.Consumer;
import _02_ConsumidorProdutor.Producer;

public class CircularBufferTest
{
   public static void main( String[] args )
   {
      // cria novo pool de threads com duas threads
      ExecutorService application = Executors.newFixedThreadPool( 2 );

      // cria CircularBuffer para armazenar ints
      Buffer sharedLocation = new CircularBuffer();

      try // tenta iniciar a produtora e a consumidora
      {
         application.execute( new Producer( sharedLocation ) );
         application.execute( new Consumer( sharedLocation ) );
      }
      catch ( Exception exception )
      {
         exception.printStackTrace();
      }

      application.shutdown();
   }
}


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/