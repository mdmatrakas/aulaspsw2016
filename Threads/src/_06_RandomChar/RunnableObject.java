package _06_RandomChar;
// Fig. 23.17: RunnableObject.java
// Runnable que grava um caractere aleat�rio em um JLabel
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import java.awt.Color;

public class RunnableObject implements Runnable
{
   private static Random generator = new Random(); // para letras aleat�rias
   private Lock lockObject; // bloqueio de aplicativo; passado para o construtor
   private Condition suspend; // utilizado para suspender e retomar thread
   private boolean suspended = false; // true se a thread for suspensa
   private JLabel output; // JLabel para a sa�da

   public RunnableObject( Lock theLock, JLabel label )
   {
      lockObject = theLock; // armazena o Lock para o aplicativo
      suspend = lockObject.newCondition(); // cria nova Condition
      output = label; // armazena JLabel para gerar sa�da de caractere
   }

   // coloca os caracteres aleat�rios na GUI
   public void run()                
   {
      // obt�m nome de thread em execu��o
      final String threadName = Thread.currentThread().getName();

      while ( true ) // loop infinito; ser� terminado de fora 
      {
         try 
         {
            // dorme por at� 1 segundo
            Thread.sleep( generator.nextInt( 1000 ) ); 

            lockObject.lock(); // obt�m o bloqueio
            try
            {
               while ( suspended ) // faz loop at� n�o ser suspenso
               {
                  suspend.await(); // suspende a execu��o do thread
               } 
            } 
            finally
            {
               lockObject.unlock(); // desbloqueia o bloqueio
            } 
         } 
         // se a thread foi interrompida durante espera/enquanto dormia
         catch ( InterruptedException exception ) 
         {
            exception.printStackTrace(); // imprime o rastreamento de pilha
         } 
            
         // exibe o caractere no JLabel correspondente
         SwingUtilities.invokeLater(                 
            new Runnable()                           
            {
               // seleciona o caractere aleat�rio e o exibe
               public void run()                      
               {
                  // seleciona a letra mai�scula aleat�ria
                  char displayChar = ( char ) ( generator.nextInt( 26 ) + 65 );

                  // gera sa�da de caractere em JLabel
                  output.setText( threadName + ": " + displayChar );
               } 
            } 
         ); 
      } 
   }

   // altera o estado suspenso/em execu��o
   public void toggle()
   {
      suspended = !suspended; // alterna booleano que controla estado

      // muda cor de r�tulo na suspens�o/retomada
      output.setBackground( suspended ? Color.RED : Color.GREEN );

      lockObject.lock(); // obt�m bloqueio
      try
      {
         if ( !suspended ) // se a thread foi retomada
         {
            suspend.signal(); // retoma a thread
         }
      }
      finally
      {
         lockObject.unlock(); // libera o bloqueio
      }
   }
}


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/