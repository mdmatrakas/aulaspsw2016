package _06_RandomChar;
// Fig. 23.18: RandomCharacters.java
// A classe RandomCharacters demonstra a interface Runnable
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class RandomCharacters extends JFrame implements ActionListener 
{
   private final static int SIZE = 7; // n�mero de threads
   private JCheckBox checkboxes[]; // array de JCheckBoxes
   private Lock lockObject = new ReentrantLock( true ); // �nico bloqueio

   // array de RunnableObjects para exibir caracteres aleat�rios
   private RunnableObject[] randomCharacters = new RunnableObject[ SIZE ];

   // configura GUI e arrays 
   public RandomCharacters()
   {
      checkboxes = new JCheckBox[ SIZE ]; // aloca espa�o para array
      setLayout( new GridLayout( SIZE, 2, 2, 2 ) ); // configura layout 

      // cria novo pool de threads com threads SIZE
      ExecutorService runner = Executors.newFixedThreadPool( SIZE );

      // loop itera SIZE vezes
      for ( int count = 0; count < SIZE; count++ )
      {
         JLabel outputJLabel = new JLabel(); // cria JLabel
         outputJLabel.setBackground( Color.GREEN ); // configura s cor
         outputJLabel.setOpaque( true ); // configura JLabel para ser opaco
         add( outputJLabel ); // adiciona JLabel a JFrame

         // cria JCheckBox para controlar estado de suspens�o/retomada
         checkboxes[ count ] = new JCheckBox( "Suspended" );

         // adiciona ouvinte que executa quando JCheckBox � clicado
         checkboxes[ count ].addActionListener( this );
         add( checkboxes[ count ] ); // adiciona JCheckBox a JFrame

         // cria um novo RunnableObject
         randomCharacters[ count ] = new RunnableObject( lockObject, outputJLabel );

         // executa RunnableObject
         runner.execute( randomCharacters[ count ] );
      }

      setSize( 400, 40+20*SIZE ); // configura o tamanho da janela
      setVisible( true ); // mostra a janela

      runner.shutdown(); // desliga quando as threads terminam
   }

   // trata os eventos JCheckBox 
   public void actionPerformed( ActionEvent event )
   {
      // faz loop em todas as JCheckBoxes no array
      for ( int count = 0; count < checkboxes.length; count++ ) 
      {
         // verifica se essa JCheckBox foi a origem do evento
         if ( event.getSource() == checkboxes[ count ] ) 
            randomCharacters[ count ].toggle(); // alterna o estado
      } 
   }

   public static void main( String args[] )
   {
      // cria novo objeto RandomCharacters
      RandomCharacters application = new RandomCharacters();

      // configura aplicativo para encerrar quando a janela for fechada
      application.setDefaultCloseOperation( EXIT_ON_CLOSE );
   }
}


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/