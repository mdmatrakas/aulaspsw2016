package _05_BufferCircularSincronizado;
// Fig. 23.15: BlockingBuffer.java
// Classe sincroniza acesso a um buffer de bloqueio.
import java.util.concurrent.ArrayBlockingQueue;

public class BlockingBuffer implements Buffer
{
   private ArrayBlockingQueue<Integer> buffer;

   public BlockingBuffer()
   {
      buffer = new ArrayBlockingQueue<Integer>( 3 );
   }
   
   // coloca o valor no buffer
   public void set( int value )
   {
      try
      {
         buffer.put( value ); // coloca o valor no buffer circular
         System.out.printf( "%s%2d\t%s%d\n", "Producer writes ", value,
            "Buffers occupied: ", buffer.size() );
      }
      catch ( Exception exception )
      {
         exception.printStackTrace();
      } 
   } 

   // retorna valor do buffer
   public int get()
   {
      int readValue = 0; // inicializa de valor lido a partir do buffer

      try
      {
         readValue = buffer.take(); // remove o valor do buffer circular
         System.out.printf( "%s %2d\t%s%d\n", "Consumer reads ",
            readValue, "Buffers occupied: ", buffer.size() );
      } 
      catch ( Exception exception )
      {
         exception.printStackTrace();
      } 

      return readValue;
   } 
} 


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/