package _01_2_contador;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class FrameContador extends JFrame implements Runnable
{
	private static final long	serialVersionUID	= 1L;
	private JPanel						jContentPane			= null;
	private JLabel						label							= null;
	private JLabel						lblNewLabel				= null;
	private int							limite;
	private int tempo;

	public FrameContador(int l, int t)
	{
		super();
		initialize();
		limite = l;
		tempo = t;
		lblNewLabel.setText(String.format("Contador %d", limite));
		setVisible(true);

	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize()
	{
		this.setSize(300, 200);
		this.setContentPane(getJContentPane());
		this.setTitle("JFrame");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane()
	{
		if (jContentPane == null)
		{
			jContentPane = new JPanel();
			jContentPane.setLayout(null);

			lblNewLabel = new JLabel("Contador");
			lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel.setBounds(10, 11, 91, 21);
			jContentPane.add(lblNewLabel);

			label = new JLabel("");
			label.setHorizontalAlignment(SwingConstants.CENTER);
			label.setBounds(10, 47, 91, 21);
			jContentPane.add(label);
		}
		return jContentPane;
	}

	@Override
	public void run()
	{
		try
		{
			Thread.sleep(tempo);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		for (int c = 0; c < 10; c++)
			for (int cc = 0; cc < limite; cc++)
			{
				label.setText(String.format("%d:%d", c + 1, cc + 1));
				try
				{
					Thread.sleep(tempo);
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.dispose();
	}
}
