package _01_2_contador;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App
{
	public static void main(String[] args)
	{
		FrameContador f1 = new FrameContador(100, 100);
		FrameContador f2 = new FrameContador(150, 1000);
		FrameContador f3 = new FrameContador(100, 100);
		FrameContador f4 = new FrameContador(150, 1000);
		FrameContador f5 = new FrameContador(100, 100);
		FrameContador f6 = new FrameContador(150, 1000);
		// cria novo pool de threads com duas threads
		ExecutorService application = Executors.newFixedThreadPool(5);
		try
		{
			application.execute(f1);
			application.execute(f2);
			application.execute(f3);
			application.execute(f4);
			application.execute(f5);
			application.execute(f6);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
		application.shutdown(); // termina aplicativo quando as threads terminam
	}
}
