# Faculdades Anglo-Americano de Foz do Iguaçu #
# Ciência da Computação #

## Projeto de Software II - 2015 ##

Exemplos de sala de aula para a disciplina de Projeto de Software II - Faculdade Anglo-Americano de Foz do Iguaçu, Professor Miguel Matrakas.

Grande parte dos exemplos são baseados nos códigos do livro Java como Programar, Deitel, Ed Pearson.

### Objetivo do repositório ###

* Exemplos de sala de aula para a disciplina de Projeto de Software II
* Acompanhamento do desenvolvimento de trabalhos acadêmicos
* Entrega de trabalhos práticos

### Como configurar e utilizar ###

Para o desenvolvimento dos exemplos e trabalhos está sendo utilizada a IDE Eclipse Luna.

* Instale os plug-ins de conexão com repositórios GIT no Eclipse ou sua IDE de preferencia.
* Configure o repositório remoto para apontar para este projeto no Bitbucket (https://bitbucket.org/mdmatrakas/aulaspsw2016)
* Utilize codificação UTF-16 para salvar os arquivos de código, evitando assim diferenças de codificação em caracteres acentuados entre os IDEs dos participantes do projeto.
* Ao realizar um commit no repositório, forneça uma descrição da funcionalidade implementada, informando quais as classes alteradas.
* Resolva pequenos problemas de cada vez (em cada commit), e procure não realizar commit de códigos com erros, apenas das funcionalidades prontas.
* Monte uma lista de atividades (cronograma), com todas as funcionalidades e alterações que devem ser realizadas por você no projeto.
* Utilize um sistema de gerencia de atividade, ou coloque comentários no código, como o exemplo a seguir, que IDE irá mostrar a lista de atividades em uma janela apropriada.
```
#!java
//TODO: blah blah
```

* Para cada classe do projeto, utilize o seguinte cabeçalho, assim todos podem entender facilmente o código.
```
#!java
/*
 * Descrição da classe: Blah blah
 * 
 * Autor: Fulano de Tal
 * Data: 01/01/2015
 * 
 * Modificações: (O que? Quem? Quando?)
 * 10/01/2015 - Fulano de Tal
 *                      Blah blah
 * 15/01/2015 - Fulano de Tal
 *                      Blah blah
 */
```

* Ao realizar um commit, no comentário especificar qual o número da atividade que está sendo realizada (de acordo com a lista abaixo). Realizar pelo menos um commit para cada atividade.

### Responsável ###

* Miguel Matrakas

# Descrição do projeto #