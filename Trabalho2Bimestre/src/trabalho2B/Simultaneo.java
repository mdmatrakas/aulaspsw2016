package trabalho2B;

import java.awt.BorderLayout;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class Simultaneo extends JFrame{
	
	JLabel tempo = new JLabel("0");
	JLabel idade = new JLabel();
	int segundos = 0;
	Connection con;
	Statement state;
	ResultSet result;
	perguntas pergunta = new perguntas();
	String resposta;
	String pergunta1 = "Em que ano você nasceu?";
	String pergunta2 = "Em que ano estamos?";
	String resposta1;
	String resposta2;
	
	perguntas pergunta3 = new perguntas();
	
	
//	Object monitor; usar esse monitor dentro de syncronized
	
	String url = "jdbc:postgresql://localhost/Trabalho2B";
	String user = "postgres";
	String password = "teste123";
	String driverName = "org.postgresql.Driver";
	
	
	@SuppressWarnings("deprecation")
	public Simultaneo() throws SQLException{
		try{
			Class.forName(driverName).newInstance();
			this.con = DriverManager.getConnection(url, user, password);
			System.out.println("Conexao estabelecida");
		}
		catch(SQLException e){
			System.out.println("alloo"+e);
		}
		catch(Exception e){
			System.out.println("alooo 2"+e);
		}
		editarLayout();
		contarTempo obj = new contarTempo();
		obj.start();
		mostrarMensagem();
		obj.stop();
		banco(pergunta);
		banco(pergunta3);
		
	}

	public void editarLayout() {
		// TODO Auto-generated method stub
		Font fonte = new Font("Arial", Font.BOLD, 50);
		Font fonte2 = new Font("Arial", Font.BOLD, 30);
		
		add(BorderLayout.NORTH,tempo);
		add(BorderLayout.CENTER,idade);
		
		tempo.setHorizontalAlignment(SwingConstants.CENTER);
		idade.setHorizontalAlignment(SwingConstants.CENTER);
		idade.setFont(fonte);
		tempo.setFont(fonte2);
		
		setSize(600,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public void mostrarMensagem() throws SQLException {
		// TODO Auto-generated method stub
		int anoNasc = Integer.parseInt(JOptionPane.showInputDialog("Em que ano você nasceu?"));
		int anoAtual = Integer.parseInt(JOptionPane.showInputDialog("Em que ano estamos?"));
		int resultado = anoAtual - anoNasc;
		idade.setText("Sua idade é: "+resultado);
		JOptionPane.showMessageDialog(null, "Você gastou "+segundos+" segundos para responder.");
		pergunta.setPergunta1(pergunta1);
		pergunta3.setPergunta1(pergunta2);
		pergunta3.setResposta1(Integer.toString(anoAtual));
		pergunta.setResposta1(Integer.toString(anoNasc));
		//pergunta.setResposta2(Integer.toString(anoAtual));
	}
	
	public static void main(String[] args) throws SQLException{
		new Simultaneo();
	}
	
	public class contarTempo extends Thread{
		public void run(){
			while(true){
				try{
					Thread.sleep(1000);
				}
				catch(Exception e){
					System.out.println("Cala boca!"+e);
				}
				segundos++;
				tempo.setText(segundos+"");
				}
			}
		}
	
	public class perguntas {
		String pergunta1 ;
		//String pergunta2 = "Em que ano estamos?";
		String resposta1;
		public String getPergunta1() {
			return pergunta1;
		}
		public void setPergunta1(String pergunta1) {
			this.pergunta1 = pergunta1;
		}
		public String getPergunta2() {
			return pergunta2;
		}
//		public void setPergunta2(String pergunta2) {
//			this.pergunta2 = pergunta2;
//		}
		public String getResposta1() {
			return resposta1;
		}
		public void setResposta1(String resposta1) {
			this.resposta1 = resposta1;
		}
		public String getResposta2() {
			return resposta2;
		}
//		public void setResposta2(String resposta2) {
//			this.resposta2 = resposta2;
//		}
		
		
		//String pergunta2 = "Em que ano estamos?";
		
		//String resposta2;	
	}
	
	public void banco(perguntas pergunta1) throws SQLException {
		String sql = "insert into tabela_pergunta (pergunta,resposta)" + "values(?,?)";
		PreparedStatement stat = con.prepareStatement(sql);
		stat.setString(1, pergunta1.getPergunta1());
		//stat.setString(2, pergunta1.getPergunta2());
		stat.setString(2, pergunta1.getResposta1());
	    
		stat.execute();
		stat.close();
	}
	
}//fim do programa